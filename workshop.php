<!DOCTYPE html>
<html lang="en">

<head>
    <title>Local Media Summit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  -->
    <meta name="description" content="Local Media Summit">
    <meta name="keywords" content="Suara, Local Media Summit, LMS">

    <meta property="og:title" content="locammediasummit.com" />
    <meta property="og:description"
        content="Local Media Summit - Emerging Local Media Business Viability to Represent the Unrepresented" />
    <meta property="og:url" content="https://microsite.suara.com/localmediasummit" />
    <meta property="og:image" content="./2023/assets/images/jms_thumb.jpg" />

    <link rel="Shortcut icon" href="./2022/assets/images/favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css">
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
    <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

</head>

<body>

    <div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="index.php#home"><img src="./2022/assets/images/icon-home.svg" alt="image" /></a>
                <!-- <ul class="navbar-nav mx-auto mb-2 mb-xl-0">
                    <li class="nav-item d-block d-xl-none">
                        <a class="jms-2023" href="jms.php" target="_blank">JATIM MEDIA SUMMIT 2023</a>
                    </li>
                </ul> -->
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#agenda">AGENDA</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#network">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    ARCHIEVE
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item-lms" href="2023/jms.php" target="_blank">JMS 2023</a>
                                    </li>
                                    <li><a class="dropdown-item-lms" href="2022/index.php" target="_blank">LMS 2022</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#kontak">CONTACT</a>
                            </li>
                            <!-- <li class="nav-item d-none d-xl-block">
                                <a class="jms-2023" href="jms.php" target="_blank">JATIM MEDIA SUMMIT 2023</a>
                            </li> -->
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/"
                                    target="_blank"><img src="./2022/assets/images/ig.svg" alt="image" width="25"
                                        height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img
                                        src="./2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img
                                        src="./2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/"
                                    target="_blank"><img src="./2022/assets/images/ig.svg" alt="image" width="25"
                                        height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img
                                        src="./2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img
                                        src="./2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

    <div id="agenda" class="container-fluid bg-white">
        <div class="container">
            <div class="row">
                <p class="head-det-agenda mt-4">WORKSHOP DESCRIPTION</p>

                <div class="d-flex justify-content-center row g-3">
                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda ms-3 ms-md-0">
                            <!-- <div class="judul text-center mb-1">OPENING CONFERENCE</div> -->
                            <div class="isi ms-3">
                                Kegiatan berformat workshop atau talkshow akan menghadirkan 1 atau 2 pembicara, yang
                                akan membahas topik-topik spesifik di tiap workshopnya. Rencana topik-topik workshop
                                yang akan diadakan:
                            </div><br>
                            <div class="isi ms-3">
                                <ul>
                                    <li>Fundraising Strategy for a Media Startup</li>
                                    <li>Finding a Viable Business Model for Local Media</li>
                                    <li>Video Packaging and Monetization</li>
                                    <li>Digital Security: Web, Social Media, and e-Mail Account Security</li>
                                    <li>Three Fundamental Technologies for Digital Media</li>
                                    <li>Utilizing Data for Content Production and Audience Development</li>
                                    <li>Platforms Optimization for Content Delivery and Monetisation</li>
                                    <li>Technology for Future Journalism</li>
                                    <li>Content Creator Management for Media</li>
                                </ul>
                            </div><br>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div id="kontak">
        <h1 class="header">Contact Us</h1>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h2>For further information, you can reach us via these channels.</h2>
                    <h3>
                        <a href="mailto:localmediasummit@arkadiacorp.com">
                            <img src="./2022/assets/images/mail.png" alt="image" />
                            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32"
                                height="32" />
                        </a>
                        <div class="br"></div>
                        <div class="wa">
                            <img src="./2022/assets/images/wa.png" alt="image" width="32" height="32" />
                            <a href="https://wa.me/+628119156234">0811-9156-234</a>
                        </div>
                    </h3>
                </div>

            </div>
        </div>
    </div>


    <div id="wa">
        <a href="https://wa.me/+628119156234" class=" fly-wa-bottom" target="_blank">
            <img src="./2023/assets/images/wa.svg" alt="img" />
        </a>
    </div>

    <div class="copyright">
        <p>© 2023 SUARA.COM - ALL RIGHTS RESERVED.</p>
    </div>

</body>