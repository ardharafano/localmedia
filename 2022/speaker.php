<!DOCTYPE html>
<html lang="en">
<head>
  <title>Local Media Summit</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
  <meta name="description" content="Local Media Summit">
  <meta name="keywords" content="Suara, Local Media Summit, LMS">
  <link rel="Shortcut icon" href="assets/images/favicon3.png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
  <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
  <link rel="stylesheet" href="assets/css/splide.min.css">
  <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
  <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
              
</head>

<body>

<div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="assets/images/icon-home.svg" alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#media">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#agenda">PROGRAM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#speaker">SPEAKERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#highlight">HIGHLIGHT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

<div id="speaker" class="container-fluid bg-white">
  <div class="container">
    <div class="row">
        <h1 class="header">SPEAKERS</h1>

        <!-- <div id="airlangga" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/airlangga.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Airlangga Hartanto</div>
            <div class="jabatan">Menteri Koordinator Perekonomian</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">-</div>
      </div> -->

      <!-- <div id="sandiuno" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/sandiuno.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Sandiaga Uno</div>
            <div class="jabatan">Menteri Pariwisata dan Ekonomi Kreatif</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Menteri Pariwisata dan Ekonomi Kreatif sejak tahun 2020. Pada Oktober 2017 - September 2018, Sandi merupakan Wakil Gubernur DKI Jakarta. Ketua Umum Himpunan Pengusaha Muda Indonesia (HIPMI) periode 2005–2008 ini juga merupakan Ketua Komite Tetap Bidang Usaha Mikro, Kecil, dan Menengah (UMKM) di Kamar Dagang dan Industri Indonesia (Kadin).</div>
      </div> -->

      <div id="sandiuno" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/teten.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Teten Masduki</div>
            <div class="jabatan">Menteri Koperasi dan UMKM</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
        <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Teten Masduki adalah Menteri Koperasi dan Usaha Kecil dan Menengah Indonesia</div>
      </div>

      <div id="sapto" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/sapto.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Sapto Anggoro</div>
            <div class="jabatan">Anggota Dewan Pers </div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Sapto Anggoro adalah anggota Dewan Pers. Sapto mengawali karir sebagai jurnalis sebelum kemudian mendirikan beberapa media online terkemuka di Indonesia.</div>
      </div>

      <div id="suwarjono" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/suwarjono.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Suwarjono</div>
            <div class="jabatan">CEO Suara.com</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">CEO dan Editor in Chief Suara.com ini telah berkarir di media digital lebih dari 20 tahun. Kini Suwarjono adalah Wakil Ketua Asosiasi Media Siber Indonesia (AMSI) yang pada periode 2014-2017 merupakan Ketua Umum Aliansi Jurnalis Independen (AJI).</div>
      </div>

      <div id="lars" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/lars.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Lars H. Bestle</div>
            <div class="jabatan">IMS Head of Department Asia</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Lars H. Bestle is Regional Director for Asia at International Media Support (IMS), an international media development organisation based in Copenhagen. He has been engaged with media development and access to information in Asia for over 20 years. As Asia Director Lars Bestle oversees and leads a team of managers across the region with a focus on promoting public interest journalism through support to media start-ups, safety of journalists and counter-measures on disinformation.</div>
      </div>

      <div id="yos" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/yos.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Yos Kusuma</div>
            <div class="jabatan">News Partner Manager at Google</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Yos Kusuma adalah News Partner Manager Google Indonesia. Mantan jurnalis di Grup MNC dan MetroTV ini menyatakan pekerjaannya bekerja sama dengan media untuk mendukung transformasi digital melalui program dan inovasi.</div>
      </div>

      <div id="gunawan" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/gunawan1.png" alt="image" width="125" height="125"/>
          </div>
          <div class="nama mt-3">Gunawan Susanto</div>
          <div class="jabatan">Country Manager AWS (Indonesia)</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Gunawan Susanto adalah Country Manager Amazon Web Service. Setelah lulus kuliah dari Fakultas Teknik Universitas Indonesia, Gunawan memulai karier sebagai management trainee di IBM Indonesia sampai kemudian menjadi President Director tahun 2014. Setelah empat tahun di IBM, Gunawan bergabung ke AWS dengan menjadi Country Leader AWS Indonesia yang pertama. Dengan pengalamannya, Gunawan berada di posisi unik bekerja dengan startup dan perusahaan dalam berbagai ukuran. Passionnya adalah meningkatkan kapasitas Indonesia dalam hal keterampilan Cloud melalui Pendidikan dan  pelatihan, membagikan insights dan pengalamannya ke banyak startup dan komunitas.</div>
      </div>

      <div id="eva" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/eva.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Eva Danayanti</div>
            <div class="jabatan">IMS Programme Manager Indonesia</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Program Manager Indonesia pada International Media Support (IMS), lembaga yang berbasis di Denmark. Digital Humanities Spesialist lulusan University of Colorado Boulder ini sebelumnya adalah Direktur Eksekutif Aliansi Jurnalis Independen (AJI) Indonesia.</div>
      </div>

      <div id="hendri" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/hendri.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Hendri Salim</div>
            <div class="jabatan">CEO TechInAsia Indonesia</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">CEO dari Tech In Asia Indonesia, portal media digital yang berfokus pada berita mengenai dunia start-up, teknologi yang berbasis di Singapura. </div>
      </div>

      <div id="janoe" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/janoe.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Janoe Arijanto</div>
            <div class="jabatan">Ketua Persatuan Perusahaan Periklanan Indonesia</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Ketua Persatuan Perusahaan Periklanan Indonesia (P3I) Pusat. Janoe juga merupakan Chief Executive Officer Dentsu One, salah satu agensi periklanan dalam jaringan bisnis global agensi komunikasi pemasaran Dentsu Aegis Network di Indonesia. </div>
      </div>

      <div id="devi" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/devi.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Devi Asmarani</div>
            <div class="jabatan">CO-Founder and Chief Editor Magdelene.co</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Co-Founder dan Editor in Chief Magdalene.co. Devi penerima S.K. Trimurti Award, penghargaan tahunan yang diberikan oleh Aliansi Jurnalis Independen (AJI) Indonesia untuk karya-karya tentang hak-hak perempuan dan kebebasan pers.</div>
      </div>

      <div id="yuswohady" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/yuswohady.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Yuswohady</div>
            <div class="jabatan">Managing Partner Inventure</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Yuswohady adalah Managing Partner Inventure yang telah menulis lebih dari 40 buku mengenai pemasaran. Selama 12 tahun bekerja di MarkPlus Inc dengan posisi terakhir sebagai Chief Executive, MarkPlus Institute of Marketing (MIM). Lulusan Teknik Mesin, Fakultas Teknik, Universitas Gadjah Mada dan Manajemen Keuangan, Universitas Indonesia.</div>
      </div>

      <div id="derian" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/derian_laks.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Derian Laks</div>
            <div class="jabatan">Country Director Dailymotion</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Country Director Dailymotion Indonesia, platform berbagi video secara online yang berpusat di Paris, Prancis. Saat ini Dailymotion menduduki peringkat kedua setelah Youtube. Sebelumnya, alumni Universitas Gunadarma ini merupakan Associate Director Of Sales mediasmart Mobile.</div>
      </div>

      <div id="arif" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/arif.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Arif Mujahidin</div>
            <div class="jabatan">Corporate Communication Director Danone Indonesia</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Sebagai praktisi kehumasan atau komunikasi korporat dengan pengalaman panjang, Arif Mujahidin akan berbicara mengenai peluang-peluang bagi media lokal untuk bekerjasama dengan dunia bisnis. Arif memiliki pengalaman bekerja di berbagai perusahaan FMCG multinasional termasuk Coca Cola Indonesia.</div>
      </div>

      <div id="henrik_grunnet" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/henrik.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Henrik Grunnet</div>
            <div class="jabatan">IMS Media Advisor</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Henrik Grunnet adalah jurnalis dan pembuat dokumenter. Senior media advisor untuk International Media Support (IMS) sejak 2009 ini juga seorang trainer media di berbagai belahan dunia, termasuk di daerah konflik seperti Yaman dan Suriah.</div>
      </div>

      <div id="sunarti" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/sunarti.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Sunarti Sain</div>
            <div class="jabatan">Editor in-Chief Radar Selatan, Sulawesi Selatan</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Sunarti Sain adalah Pemimpin Koran Radar Selatan yang berhasil mentransformasikan diri menjadi media digital di Sulawesi Selatan</div>
      </div>

      <div id="ahmad" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/ahmadnasir.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Ahmad Nasir</div>
            <div class="jabatan">CEO Indotnesia</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">M Nasir adalah pendiri Indotnesia, sebuah media online yang mendasarkan dirinya pada produksi video. Indotnesia sedang dalam ikhtiar mengembangkan sebuah konsep koperasi media.</div>
      </div>

      <div id="arsito" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/arsito.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Arsito Hidayatullah</div>
            <div class="jabatan">Redaktur Pelaksana Suara.com</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Arsito adalah Managing Editor Suara.com yang turut serta mengembangkan media digital ini sejak awal, berikut berbagai media dan platform Arkadia Digital Media lainnya. Sebelumnya, Arsito pernah bekerja di beberapa media online di Jakarta, setelah beberapa tahun menjadi jurnalis di Riau.</div>
      </div>

      <div id="ratna" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/catur-ratna.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Catur Ratna Wulandari</div>
            <div class="jabatan">Editor in Chief DigitalMama</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">CEO dan Editor in Chief DigitalMama ini merupakan alumni Paramadina Graduate School of Diplomacy. Sebelum mendirikan DigitalMama.ID, Ratna berkarir sebagai jurnalis di Harian Umum Pikiran Rakyat, Bandung.</div>
      </div>

      <div id="inayah" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/inayah-wahid.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Inayah Wahid</div>
            <div class="jabatan">Founder HAMburger Podcast</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
          <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Inayah Wulandari Wahid adalah founder HAMburger Podcast, sebuah podcast yang membahas masalah-masalah hak asasi manusia secara ringan dan menarik. Putri bungsu mantan Presiden Indonesia Abdurrahman Wahid ini juga dikenal sebagai aktris, stand up comedian, dan juga aktivis hak asasi manusia.</div>
      </div>

      <div id="dimas" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/dimas.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Dimas Sagita</div>
            <div class="jabatan">Head of Growth Suara.com</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Dimas adalah Head of Growth Suara.com. Traffic Growth Specialist dengan latar belakang Digital Marketing ini membawahi operasi media sosial dan SEO Suara.com serta seluruh vertikal yang berada di bawah grup Arkadia Digital Media. </div>
      </div>

      <div id="subagja" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/subagja.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Subagja Hamara</div>
            <div class="jabatan">CEO Harapan Rakyat</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Subagja Hamara adalah Pemimpin Redaksi Harapan Rakyat yang berbasis di Ciamis, Jawa Barat. Ketua Persatuan Wartawan Ciamis ini berhasil membawa medianya yang berbasis cetak, merambah ke ranah digital.</div>
      </div>

      <div id="lasma" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/lasma.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Lasma Natalia</div>
            <div class="jabatan">Direktur LBH Bandung</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Direktur Lembaga Bantuan Hukum (LBH) Bandung sejak 2020. Pada tahun 2020, Magister Hukum dari Universitas Padjadjaran ini terpilih sebagai perempuan pembela lingkungan dalam laporan khusus TEMPO.</div>
      </div>

      <div id="rendy" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/rendy.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Rendy Sadikin </div>
            <div class="jabatan">Community Development Manager Yoursay.id</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Rendy Sadikin adalah Kepala Biro Yogyakarta Suara.com. Rendy yang memiliki pengalaman panjang di industri media terutama media online, media development, Team Management, Social Media Journalism, Web Content Writing, Search Engine Optimization (SEO), and Analytic Tools (Google) ini membawahi operasi platform user-generated content milik PT Arkadia Digital Media, Yoursay.id, yang sedang berkembang pesat.</div>
      </div>

      <div id="suwarmin" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/suwarmin.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Suwarmin</div>
            <div class="jabatan">Direktur Bisnis dan Konten Solopos Group</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Suwarmin adalah Direktur Bisnis dan Konten Solopos Group. Suwarmin berpengalaman sebagai jurnalis di berbagai platform, mulai dari cetak, radio, hingga kini merambah ke digital. </div>
      </div>

      <div id="bram" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/bram2.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Bram Bravo</div>
            <div class="jabatan">Head of Digital Ad Ops & Programmatic Advertising Suara.com</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Bram Bravo, Digital Ad Ops & Programmatic Manager Suara.Com. Dengan latarbelakang pedidikan Teknologi Informasi Bram mengelola periklanan digital semua media di Grup Arkadia Digital Media</div>
      </div>

      <div id="trijoko" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/trijoko.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Tri Joko Her Riadi</div>
            <div class="jabatan">Founder BandungBergerak</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">CEO BandungBergerak ini memilih menjadi jurnalis sejak akhir 2007. Joko penerima Anugerah Adinegoro 2015 dan Elizabeth O'Neill Journalism Award 2018. Mendirikan sekaligus menjadi pemimpin redaksi BandungBergerak.id sejak Maret 2021.</div>
      </div>

      <div id="septini" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/septini1.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Saptini Darmaningrum</div>
            <div class="jabatan">Direktur Usaha BeritaJatim</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Direktur Usaha BeritaJatim ini merupakan Wakil Sekretaris Komite Komunikasi Digital (KKD) Provinsi Jawa Timur. Komite tersebut bertugas memerangi berita hoaks, misinformasi dan ujaran kebencian, di tengah disrupsi informasi sekaligus memperkuat edukasi dan literasi.</div>
      </div>
      
      <div id="prawira" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/prawira.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Prawira Maulana</div>
            <div class="jabatan">Content Manager Tribun Bengkulu</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Prawira Maulana adalah Content Manager TribunBengkulu.com yang bertanggung jawab atas produksi konten termasuk audio visual di media yang berada di bawah Grup Tribun ini. Meski TribunBengkulu.com baru berusia kurang dari dua tahun, Prawira Maulana berhasil memonetisasi konten-konten video yang diproduksi TribunBengkulu melalui berbagai sarana delivery termasuk YouTube dan Facebook.</div>
      </div>

      <div id="lerane" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/lerane.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">La Rane Hafied</div>
            <div class="jabatan">Pendiri Paberik Soeara Rakjat</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Rane seorang pencerita, seorang storyteller. Pendiri Paberik Soeara Rakjat, perusahaan dengan produk utama cerita, terutama melalui medium audio/podcast.</div>
      </div>

      <div id="aliefah" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/aliefah.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Aliefah Permata Fikri</div>
            <div class="jabatan">Sr. Account Manager MGID</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Sr. Account Manager MGID, perusahaan periklanan online yang bermarkas di Los Angeles. Sebelum berkiprah di MGID, alumni Universitas Jenderal Soedirman Purwokerto ini berkarir di Heroleads Asia dan Lamudi. </div>
      </div>

      <div id="martha" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/martha1.png" alt="image" width="125" height="125"/>
          </div>
          <div class="nama mt-3">Martha Rinna Dewi</div>
          <div class="jabatan">Territory Manager AWS (Indonesia)</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Martha Rinna Dewi adalah Territory Manager AWS Indonesia. Pemegang gelar MBA dari Coventry University ini biasa berbicara mengenai berbagai peluang inovasi melalui topik seperti Machine Learning, IoT, Komputasi Edge, dan lainnya. Martha memiliki passion membantu perusahaan-perusahaan meningkatkan kapasitas bisnis, pertumbuhan pendapatan, efisiensi, pengurangan biaya dan mendorong inovasi.</div>
      </div>

      <div id="jesse" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/jesse.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Jesse Adam Halim</div>
            <div class="jabatan">Produser HAMburger Podcast</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Sebagai salah seorang Co-founder HAMburger Podcast, Jesse percaya pada kekuatan narasi tanding. Mengawali karir sebagai jurnalis, kini HAMburger Podcast menjadi platformnya untuk mengkampanyekan isu-isu HAM.</div>
      </div>

      <div id="septiaji" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/septiaji.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Septiaji Eko Nugroho</div>
            <div class="jabatan">Ketua Presidium Mafindo</div>
        </div>
      </div>
      
      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Septiaji Eko Nugroho adalah Ketua Presidium Masyarakat Anti Fitnah Indonesia (MAFINDO) yang berdiri sejak tahun 2016. Mafindo adalah sebuah organisasi masyarakat sipil yang bergerak di bidang pencegahan misinformasi dan disinformasi. Dengan lebih dari 500 sukarelawan di 17 kota di Indonesia, Mafindo bekerjasama dengan 24 media terkemuka mendirikan Cekfakta.com yang dikembangkan di atas teknologi “Yudistira” yang dibangun tim IT Mafindo.</div>
      </div>

      <div id="carl_javier" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/carl.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Carl Javier</div>
            <div class="jabatan">CEO Puma Podcast Philippines</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Carl Javier adalah Chief Executive Officer PumaPodcast, sebuah production house yang memproduksi podcast dan audio berbasis di Filipina. Javier memiliki pengalaman sebagai jurnalis, business manager, komikus, dan penulis fiksi dan puisi. Pemegang gelar sarjana dari jurusan Bahasa Inggris dan master untuk Creative Writing dari the University of the Philippines ini juga kini mengajar di Ateneo de Manila University.</div>
      </div>

      <div id="eddy" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/eddy.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Eddy Prastyo</div>
            <div class="jabatan">Manager Produksi Suara Surabaya Media</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
            <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Eddy Prastyo, Manager Produksi Suara Surabaya Media
Eddy adalah yang mengelola operasional produksi dan keredaksian di Radio Suara Surabaya dan www.suarasurabaya.net. Sebagai manager produksi Suara Surabaya Media, Eddy bertanggung jawab atas konten dan isu untuk media sosial Suara Surabaya seperti FB e100 yang memiliki 1,5 juta follower, akun Twitter @e100ss dengan 1 juta follower, akun Instagram @suarasurabayamedia dengan 600 ribu follower. Media sosial yang dikelola SS ini menjadi semacam penghubung bagi warga Surabaya dan sekitarnya untuk menyelesaikan masalah dengan saling bantu satu sama lain.</div>
      </div>

      <div id="zuhri" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/zuhri.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Zuhri Muhammad</div>
            <div class="jabatan">Founder dan CEO BatamNews</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
          <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">-</div>
      </div>

      <div id="upi" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/upi.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Upi Asmaradhana</div>
            <div class="jabatan">Founder-CEO Kabar Group Indonesia</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
          <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">Upi Asmaradhana adalah pendiri dan Chief Executive Officer Kabar Group Indonesia. Upi merupakan jurnalis dan aktivis dari Makassar yang awalnya mendirikan Kabarmakassar.com sebelum mengembangkannya menjadi sebuah grup media yang memiliki beberapa laman lain di berbagai kota di Indonesia. Duta Literasi Sulawesi Selatan ini juga dikenal sebagai aktivis kebebasan berekspresi dan kebebasan pers.</div>
      </div>

      <div id="dwieko" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/dwieko.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Dwi Eko Lokononto</div>
            <div class="jabatan">Pemimpin Umum & Pemimpin Redaksi BeritaJatim</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
          <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">-</div>
      </div>

      <div id="heru" class="col-lg-13 col-md-6 col-12 ms-md-5">
        <div class="border-detail">
          <div class="speaker-img">
            <img src="assets/images/heru.png" alt="image" width="125" height="125"/>
          </div>
            <div class="nama mt-3">Heru Tjatur</div>
            <div class="jabatan">Co-Founder ICT Watch</div>
        </div>
      </div>

      <div class="col-md-3 col-12">
          <div class="text-speaker ms-3 ms-md-4 ms-xl-0 ms-xxl-0 mb-5 text-start">-</div>
      </div>

    </div>
  </div>

</div>

<div id="kontak" class="container-fluid">
  <h1 class="header">Contact Us</h1>
  <div class="container">
    <div class="row">

    <div class="col-md-12">
        <h2>For further information, you can reach us via these channels.</h2>
        <h3>
          <a href="mailto:localmediasummit@arkadiacorp.com">
            <img src="assets/images/mail.png" alt="image"/> 
            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
          </a>
            <div class="br"></div>
            <div class="wa">
              <img src="assets/images/wa.png" alt="image" width="32" height="32"/> 
                <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
            </div>
        </h3>
      </div>

    </div>
  </div>
</div>

<div class="copyright">
  <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
</div>

</body>