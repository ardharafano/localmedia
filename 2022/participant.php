<!doctype html>
<html lang="en">

<head>
<title>Local Media Summit - Participant</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
  <meta name="description" content="Local Media Summit">
  <meta name="keywords" content="Suara, Local Media Summit, LMS">
  <link rel="Shortcut icon" href="assets/images/favicon.ico">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
  <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
  <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>



<body>

<div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="assets/images/icon-home.svg" alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#media">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#agenda">PROGRAM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#speaker">SPEAKERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#highlight">HIGHLIGHT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

    <div id="kolab" class="container-fluid">
        <div class="container">

        <h4 class="text-center">PARTICIPANT</h4>
        <h5 class="text-center judul-participant mb-3">Pendaftaran untuk menghadiri local media summit 2022. Kami memberikan kesempatan bagi yang berminat, silahkan isi form di bawah ini.</h5>
        <ul class="nav nav-pills2 pills mb-3 justify-content-center" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link2 active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">MEDIA</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link2" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">PERSONAL</button>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                
            <form action="register_act.php" class="d-flex justify-content-center row g-3 mx-md-1 text-form needs-validation" novalidate>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Nama Lengkap" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Media" required>
                     <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Jabatan" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                     Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="email" class="form-control" id="validationCustom05" placeholder="Email" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Email Sudah Digunakan
                    </div>
                </div>

                <div class="col-md-10">
                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" 
                    minlength="10" maxlength = "13" min="100000000"class="form-control" id="validationCustom04" placeholder="No. Telepon/ WhatsApp" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / No. Telepon Sudah Digunakan / Minimal 10 Digit
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="judul-form">
                    Apakah Akan Hadir Secara
                    </div>

                    <div class="form-check mb-3">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" name="flexRadioDefault" id="flexRadioDefault1" required>
                        <label class="form-check-label" for="flexRadioDefault1">Offline</label>
                    </div>
                    <div class="form-check">
                        <!-- <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" name="flexRadioDefault" id="flexRadioDefault2" required>
                        <label class="form-check-label" for="flexRadioDefault2">Online</label> -->
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="judul-form">
                    Kegiatan Yang Akan Dihadiri
                    </div>

                    <!-- <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck1">
                        <label class="form-check-label" for="validationFormCheck1">Opening Conference: Masa Depan Media Lokal: Model Bisnis, Keberlanjutan, dan Pengembangan Audiens (Future of Local Media: Business Model, Sustainability, and Audience Development)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck2">
                        <label class="form-check-label" for="validationFormCheck2">Closing Conference: Menemukan Kembali Media Lokal: Mencari Peluang dan Mengatasi Tantangan (Reinventing Local Media: Finding opportunities and overcoming the challenges)</label>
                    </div> -->
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck3">
                        <label class="form-check-label" for="validationFormCheck3">Workshop Strategi Pendanaan untuk Startup Media (Fundraising strategy for a Media Startup)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck4">
                        <label class="form-check-label" for="validationFormCheck4">Workshop Menemukan Model Bisnis yang Sehat untuk Media Lokal (Finding a viable business model for local media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck5">
                        <label class="form-check-label" for="validationFormCheck5">Workshop Pengemasan dan monetisasi video (Video packaging and monetization)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck6">
                        <label class="form-check-label" for="validationFormCheck6">Workshop Mencari Model Bisnis Podcast (Finding Podcast business model)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck7">
                        <label class="form-check-label" for="validationFormCheck7">Workshop Tiga Teknologi Fundamental untuk Media Digital (Three Fundamental Technologies for Digital Media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck8">
                        <label class="form-check-label" for="validationFormCheck8">Workshop Mentransformasi Media Lama ke Digital Media (Transforming Legacy Media to Digital Media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck9">
                        <label class="form-check-label" for="validationFormCheck9">Workshop Optimalisasi Mesin Pencari dan Media Sosial (Search Engine & social media Optimalization)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck10">
                        <label class="form-check-label" for="validationFormCheck10">Workshop Bagaimana Menghasilkan Pemasukan dari Media Sosial (How to generate income from social media)</label>
                    </div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck41">
                        <label class="form-check-label" for="validationFormCheck41">Workshop Ide Kolaborasi untuk Media Lokal (Collaboration Ideas for Local Media)</label>
                    </div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck42">
                        <label class="form-check-label" for="validationFormCheck42">Workshop Membangun Audiens dari Komunitas Lokal (Building The Audience from The Local Community)</label>
                        <div class="valid-feedback">
                            Berhasil (Pilih Minimal Satu)
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                <div class="judul-form ps-4">
                  Apakah ada Coaching Clinic yang Anda butuhkan?
                    </div>

                    <textarea class="form-control" id="validationCustom06" placeholder="Sebutkan" minlength="10" rows="5"></textarea>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Minimal 10 Karakter
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="judul-form ps-4">
                    Jika Anda adalah pemimpin media lokal, apakah media Anda bersedia mengikuti Wall of Fame?
                    </div>

                    <div class="form-check mb-3">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" name="flexRadioDefault1" id="flexRadioDefault3" required>
                        <label class="form-check-label" for="flexRadioDefault3">Bersedia</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" name="flexRadioDefault1" id="flexRadioDefault4" required>
                        <label class="form-check-label" for="flexRadioDefault4">Tidak Bersedia</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div id="p1" class="p1" style="display:none">
                <div class="col-md-10">
                    <div class="judul-form-2 ps-4">
                    Jika bersedia, silakan isi data berikut
                    </div>

                    <label for="validationCustom01" class="form-label">Media</label>
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Media">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <label for="validationCustom01" class="form-label mt-3">Website</label>
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Website">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                <label for="validationCustom01" class="form-label mt-3">Alamat</label>
                    <textarea class="form-control" id="validationCustom06" placeholder="Sebutkan" minlength="10" rows="3"></textarea>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Minimal 10 Karakter
                    </div>
                </div>

                <div class="col-md-10">
                    <label for="validationCustom01" class="form-label mt-3">Berdiri (Tahun)</label>
                    <input type="date" class="form-control" id="validationCustom01" placeholder="Website">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                <label for="validationCustom01" class="form-label mt-3">Sejarah Pendirian</label>
                    <textarea class="form-control" id="validationCustom06" placeholder="Sebutkan" minlength="10" rows="5"></textarea>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Minimal 100 Karakter
                    </div>
                </div>

                <div class="col-md-10">
                    <label for="validationCustom01" class="form-label mt-3">Bahasa</label>
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Bahasa">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                        <div class="my-4">Fokus Utama</div>

                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="flexRadioDefault2" id="flexRadioDefault5">
                        <label class="form-check-label" for="flexRadioDefault5">General News</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault2" id="flexRadioDefault6">
                        <label class="form-check-label" for="flexRadioDefault6">Niche/ Segmented</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10 mt-3">
                    <textarea class="form-control" id="validationCustom06" placeholder="Sebutkan" minlength="10" rows="5"></textarea>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Minimal 10 Karakter
                    </div>
                </div>

                <div class="col-md-10">
                      <div class="my-2"><b>Target Pembaca</b></div>
                      <div class="my-2">Social Media</div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck12">
                        <label class="form-check-label" for="validationFormCheck12">Facebook</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck13">
                        <label class="form-check-label" for="validationFormCheck13">Instagram</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck14">
                        <label class="form-check-label" for="validationFormCheck14">Tiktok</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck15">
                        <label class="form-check-label" for="validationFormCheck15">Youtube</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck16">
                        <label class="form-check-label" for="validationFormCheck16">Twitter</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck17">
                        <label class="form-check-label" for="validationFormCheck17">Lainnya</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control mt-3" id="validationCustom01" placeholder="Lainnya">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                  <div class="my-3"><b>Jumlah Staff</b> (Redaksi dan Non Redaksi)</div>
                    <input type="number" class="form-control" id="validationCustom01" placeholder="Jumlah Staff">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                      <div class="my-3"><b>Pendapatan Utama</b> (Iklan Direct, Activation, Programmatic Ads, Sosial Media, membership etc.)</div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck18">
                        <label class="form-check-label" for="validationFormCheck18">Iklan Langsung</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck19">
                        <label class="form-check-label" for="validationFormCheck19">Programmatic Ads</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck20">
                        <label class="form-check-label" for="validationFormCheck20">Social Media Ads (YouTube) </label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck21">
                        <label class="form-check-label" for="validationFormCheck21">Berlangganan</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck22">
                        <label class="form-check-label" for="validationFormCheck22">Donasi</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck23">
                        <label class="form-check-label" for="validationFormCheck23">Lainnya</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control mt-3" id="validationCustom01" placeholder="Lainnya">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                        <div class="my-4">Pendapatan Tahunan</div>

                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="flexRadioDefault3" id="flexRadioDefault7">
                        <label class="form-check-label" for="flexRadioDefault7">Di bawah Rp100 juta per tahun</label>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="flexRadioDefault3" id="flexRadioDefault8">
                        <label class="form-check-label" for="flexRadioDefault8">Rp100 juta – Rp1 miliar per tahun</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault3" id="flexRadioDefault9">
                        <label class="form-check-label" for="flexRadioDefault9">Di atas Rp1 miliar per tahun</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                      <div class="my-3">Kebutuhan untuk Pengembangan (Boleh pilih lebih dari satu) </div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck24">
                        <label class="form-check-label" for="validationFormCheck24">Content</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck25">
                        <label class="form-check-label" for="validationFormCheck25">Business</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck26">
                        <label class="form-check-label" for="validationFormCheck26">Technology</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck27">
                        <label class="form-check-label" for="validationFormCheck27">Audience</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="validationFormCheck28">
                        <label class="form-check-label" for="validationFormCheck28">Market</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="judul-form-2 ps-4">
                    Penghargaan yang pernah diperoleh
                    </div>

                    <!-- <label for="validationCustom01" class="form-label"></label> -->
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Sebutkan">
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="judul-form-2 ps-4">
                        Logo Media
                    </div>
                    <input type="file" class="form-control" id="validationCustom20" placeholder="Dokumen Usaha">
                    <small>* Ukuran maksimum 10 MB, Tipe file jpg, jpeg, png, pdf</small>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Minimal 1 Dokumen
                    </div>
                </div>

                </div>

                <h3 class="mt-5 mb-0"><b>Untuk mengikuti semua rangkaian acara Local Media Summit 2022 peserta dikenakan biaya Admission Rp 300.000. Bagi yang mendaftar sebelum tanggal 21 Oktober 2022, kami memberikan dukungan biaya Admisi free, (kuota terbatas).</b></h3><br>
                <div class="col-md-10">
                    <div class="judul-form ps-4">
                    Apakah Anda membutuhkan dukungan pendanaan untuk mengikuti acara ini?
                    </div>

                    <div class="form-check mb-3">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" id="tab1" name="tab" required>
                        <label class="form-check-label" for="tab1">Iya</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" id="tab2" name="tab" required>
                        <label class="form-check-label" for="tab2">Tidak</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                    <article>
                        <div class="col-md-10">
                            <div class="judul-form-3 text-center">
                                Dukungan pendanaan ini memiliki kuota terbatas. Kami akan mengabari Anda jika terpilih untuk mendapatkan dukungan pendanaan. 
                                Terima kasih.
                            </div>
                        </div>
                    </article>

                    <article>
                        <div class="col-md-10">
                            <div class="judul-form-3 text-center">
                            Silakan transfer ke nomor rekening bank <b>BCA 0703038414 Cab. Kebayoran Baru Jakarta</b> atas nama <b>PT Arkadia Media Nusantara</b> sejumlah <b>Rp300,000.</b> Kemudian kirimkan bukti transfer ke admin kami melalui e-mail <a href="mailto:localmediasummit@arkadiacorp.com" class="email2">localmediasummit@arkadiacorp.com</a> atau nomor WhatsApp berikut <a href="https://wa.me/+628118258596" class="wa text-dark bg-white">+62 811-8258-596</a>. Terima kasih.
                            </div>
                        </div>
                    </article>

                    <div class="col-md-10">
                    <div class="judul-form ps-4">
                    Apakah Anda membutuhkan dukungan biaya transportasi dan akomodasi untuk mengikuti LMS 2022?
                    </div>

                    <div class="form-check mb-3">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" id="tab3" name="tab2" required>
                        <label class="form-check-label" for="tab3">Iya</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" id="tab4" name="tab2" required>
                        <label class="form-check-label" for="tab4">Tidak</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                    <article class="tab2">
                        <div class="col-md-10">
                            <div class="judul-form-3 text-center">
                                Dukungan pendanaan ini memiliki kuota terbatas. Kami akan mengabari Anda jika terpilih untuk mendapatkan dukungan pendanaan. 
                                Terima kasih.
                            </div>
                        </div>
                    </article>

                    <article class="tab2">
                        <div class="col-md-10">
                            &nbsp;
                        </div>
                    </article>

                <div class="col-md-10">
                    <div class="form-check mt-1 mb-2">
                        <input class="form-check-input mb-2" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label mb-2" for="invalidCheck">
                            Seluruh data yang didaftarkan dapat dipertanggung jawabkan oleh Peserta
                        </label>
                        <label class="form-check-label text-secondary" for="invalidCheck">
                            <small>Arkadia Digital Media tidak akan menyebarluaskan data yang telah diberikan kepada pihak lain dan hanya digunakan selama penyelenggaraan Program Local Media Summit</small>
                        </label>
                        <div class="invalid-feedback">
                            Anda harus setuju sebelum data dikirim.
                        </div>
                    </div>
                </div>

                <div class="col-10 text-center daftar">
                    <button class="mb-5 button-kirim" type="submit" onclick="setTimeout(check, 1000);">SUBMIT</button>
                    <!-- <a href="javascript:window.open('','_self').close();">
                    <button class="mb-5 button-batal" type="button">KEMBALI</button>
                    </a> -->
                </div>
            </form>
            
            </div>

            <!-- form2 -->

            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">

               <form action="register_act.php" class="d-flex justify-content-center row g-3 text-form needs-validation" novalidate>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Nama Lengkap" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Organisasi/ Perusahaan" required>
                     <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Pekerjaan/ Jabatan" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                     Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" 
                    minlength="2" maxlength = "2" min="15"class="form-control" id="validationCustom04" placeholder="Usia" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                     Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="email" class="form-control" id="validationCustom05" placeholder="Email" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Email Sudah Digunakan
                    </div>
                </div>

                <div class="col-md-10">
                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" 
                    minlength="10" maxlength = "13" min="100000000"class="form-control" id="validationCustom04" placeholder="No. Telepon/ WhatsApp" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / No. Telepon Sudah Digunakan / Minimal 10 Digit
                    </div>
                </div>

                <!-- <div class="col-md-10">
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault10" required>
                        <label class="form-check-label" for="flexRadioDefault10">Personal</label><br>
                        <small>Admission Rp 300.000 </small><br>
                        <small>Anda bisa mengikuti seluruh kegiatan LMS 2022</small>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault11" required>
                        <label class="form-check-label" for="flexRadioDefault11">Media</label><br>
                        <small>Admission Rp 300.000 </small><br>
                        <small>Anda bisa mengikuti seluruh kegiatan LMS 2022</small>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div> -->

                <div class="col-md-10">
                    <div class="judul-form ps-4">
                    Apakah Akan Hadir Secara
                    </div>

                    <div class="form-check mb-3">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" name="flexRadioDefault1" id="flexRadioDefault12" required>
                        <label class="form-check-label" for="flexRadioDefault12">Offline</label>
                    </div>
                    <div class="form-check">
                        <!-- <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" name="flexRadioDefault1" id="flexRadioDefault13" required>
                        <label class="form-check-label" for="flexRadioDefault13">Online</label> -->
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="judul-form ps-4">
                    Kegiatan Yang Akan Dihadiri
                    </div>

                    <!-- <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck29">
                        <label class="form-check-label" for="validationFormCheck29">Opening Conference: Masa Depan Media Lokal: Model Bisnis, Keberlanjutan, dan Pengembangan Audiens (Future of Local Media: Business Model, Sustainability, and Audience Development)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck30">
                        <label class="form-check-label" for="validationFormCheck30">Closing Conference: Menemukan Kembali Media Lokal: Mencari Peluang dan Mengatasi Tantangan (Reinventing Local Media: Finding opportunities and overcoming the challenges)</label>
                    </div> -->
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck31">
                        <label class="form-check-label" for="validationFormCheck31">Workshop Strategi Pendanaan untuk Startup Media (Fundraising strategy for a Media Startup)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck32">
                        <label class="form-check-label" for="validationFormCheck32">Workshop Menemukan Model Bisnis yang Sehat untuk Media Lokal (Finding a viable business model for local media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck33">
                        <label class="form-check-label" for="validationFormCheck33">Workshop Pengemasan dan monetisasi video (Video packaging and monetization)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck34">
                        <label class="form-check-label" for="validationFormCheck34">Workshop Mencari Model Bisnis Podcast (Finding Podcast business model)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck35">
                        <label class="form-check-label" for="validationFormCheck35">Workshop Tiga Teknologi Fundamental untuk Media Digital (Three Fundamental Technologies for Digital Media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck36">
                        <label class="form-check-label" for="validationFormCheck36">Workshop Mentransformasi Media Lama ke Digital Media (Transforming Legacy Media to Digital Media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck37">
                        <label class="form-check-label" for="validationFormCheck37">Workshop Optimalisasi Mesin Pencari dan Media Sosial (Search Engine & social media Optimalization)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck38">
                        <label class="form-check-label" for="validationFormCheck38">Workshop Bagaimana Menghasilkan Pemasukan dari Media Sosial (How to generate income from social media)</label>
                    </div>
                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck39">
                        <label class="form-check-label" for="validationFormCheck39">Workshop Ide Kolaborasi untuk Media Lokal (Collaboration Ideas for Local Media)</label>
                    </div>

                    <div class="form-check mb-3">
                        <input type="checkbox" class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" id="validationFormCheck40">
                        <label class="form-check-label" for="validationFormCheck40">Workshop Membangun Audiens dari Komunitas Lokal (Building The Audience from The Local Community)</label>
                        <div class="valid-feedback">
                            Berhasil (Pilih Minimal Satu)
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                <div class="col-md-10">
                <div class="judul-form ps-4">
                  Apakah ada Coaching Clinic yang Anda butuhkan?
                    </div>

                    <textarea class="form-control" id="validationCustom06" placeholder="Sebutkan" minlength="10" rows="5"></textarea>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Minimal 10 Karakter
                    </div>
                </div>

                <h3 class="mt-5 mb-0"><b>Untuk mengikuti semua rangkaian acara Local Media Summit 2022 peserta dikenakan biaya Admission Rp 300.000. Bagi yang mendaftar sebelum tanggal 21 Oktober 2022, kami memberikan dukungan biaya Admisi free, (kuota terbatas).</b> </h3><br>
                <div class="col-md-10">
                    <div class="judul-form ps-4">
                    Apakah Anda membutuhkan dukungan pendanaan untuk mengikuti acara ini?
                    </div>

                    <div class="form-check mb-3">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" id="tab5" name="tab3" required>
                        <label class="form-check-label" for="tab5">Iya</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input me-xl-1 mx-xl-0  mx-xxl-1" type="radio" id="tab6" name="tab3" required>
                        <label class="form-check-label" for="tab6">Tidak</label>
                        <div class="valid-feedback">
                            Berhasil
                        </div>
                        <div class="invalid-feedback">
                            Pilih Salah Satu
                        </div>
                    </div>
                </div>

                    <article class="tab3">
                        <div class="col-md-10">
                            <div class="judul-form-3 text-center">
                                Dukungan pendanaan ini memiliki kuota terbatas. Kami akan mengabari Anda jika terpilih untuk mendapatkan dukungan pendanaan. 
                                Terima kasih.
                            </div>
                        </div>
                    </article>

                    <article class="tab3">
                        <div class="col-md-10">
                            <div class="judul-form-3 text-center">
                            Silakan transfer ke nomor rekening bank <b>BCA 0703038414 Cab. Kebayoran Baru Jakarta</b> atas nama <b>PT Arkadia Media Nusantara</b> sejumlah <b>Rp300,000.</b> Kemudian kirimkan bukti transfer ke admin kami melalui e-mail <a href="mailto:localmediasummit@arkadiacorp.com" class="email2">localmediasummit@arkadiacorp.com</a> atau nomor WhatsApp berikut <a href="https://wa.me/+628118258596" class="wa text-dark bg-white">+62 811-8258-596</a>. Terima kasih.
                            </div>
                        </div>
                    </article>

                <div class="col-md-10">
                    <div class="form-check mt-1 mb-2">
                        <input class="form-check-input mb-2" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label mb-2" for="invalidCheck">
                            Seluruh data yang didaftarkan dapat dipertanggung jawabkan oleh Peserta
                        </label>
                        <label class="form-check-label text-secondary" for="invalidCheck">
                            <small>Arkadia Digital Media tidak akan menyebarluaskan data yang telah diberikan kepada pihak lain dan hanya digunakan selama penyelenggaraan Program Local Media Summit</small>
                        </label>
                        <div class="invalid-feedback">
                            Anda harus setuju sebelum data dikirim.
                        </div>
                    </div>
                </div>

                <div class="col-10 text-center daftar">
                    <button class="mb-5 button-kirim" type="submit" onclick="setTimeout(check, 1000);">SUBMIT</button>
                    <!-- <a href="javascript:window.open('','_self').close();">
                    <button class="mb-5 button-batal" type="button">KEMBALI</button>
                    </a> -->
                </div>
            </form>

            </div>
        </div>


        
        </div>
    </div>

<div id="kontak" class="container-fluid">
  <h1 class="header">Contact Us</h1>
  <div class="container">
    <div class="row">

    <div class="col-md-12">
        <h2>For further information, you can reach us via these channels.</h2>
        <h3>
          <a href="mailto:localmediasummit@arkadiacorp.com">
            <img src="assets/images/mail.png" alt="image"/> 
            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
          </a>
            <div class="br"></div>
            <div class="wa">
              <img src="assets/images/wa.png" alt="image" width="32" height="32"/> 
                <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
            </div>
        </h3>
      </div>

    </div>
  </div>
</div>

<div class="copyright">
  <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
</div>

    <script>
        function check() {
        swal({
                title: "Gagal!",
                text: "Data Masih Belum Lengkap",
                icon: "error",
                button: true
            });
        }
    </script>


    <script>
        (function() {
            'use strict'
            var forms = document.querySelectorAll('.needs-validation')
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()
    </script>

    <script>
        $(document).ready(function()
{        
    $("#div1").text($("#p1").css("display"));
    
    $("#flexRadioDefault4").click(function()
    {
        if ($("#p1").is(":visible"))
        {
            $("#p1").toggle();
            $("#div1").text($("#p1").css("display"));
        }
    });
    
    $("#flexRadioDefault3").click(function()
    {
        if (!$("#p1").is(":visible"))
        {
            $("#p1").toggle();
            $("#div1").text($("#p1").css("display"));
        }
    });
});
    </script>

<script>
    $('[name=tab]').each(function(i,d){
  var p = $(this).prop('checked');
//   console.log(p);
  if(p){
    $('article').eq(i)
      .addClass('on');
  }    
});  

$('[name=tab]').on('change', function(){
  var p = $(this).prop('checked');
  
  // $(type).index(this) == nth-of-type
  var i = $('[name=tab]').index(this);
  
  $('article').removeClass('on');
  $('article').eq(i).addClass('on');
});

$('[name=tab2]').each(function(i,d){
  var p = $(this).prop('checked');
//   console.log(p);
  if(p){
    $('article').eq(i)
      .addClass('on');
  }    
});  

$('[name=tab2]').on('change', function(){
  var p = $(this).prop('checked');
  
  // $(type).index(this) == nth-of-type
  var i = $('[name=tab2]').index(this);
  
  $('article.tab2').removeClass('on');
  $('article.tab2').eq(i).addClass('on');
});

$('[name=tab3]').each(function(i,d){
  var p = $(this).prop('checked');
//   console.log(p);
  if(p){
    $('article').eq(i)
      .addClass('on');
  }    
});  

$('[name=tab3]').on('change', function(){
  var p = $(this).prop('checked');
  
  // $(type).index(this) == nth-of-type
  var i = $('[name=tab3]').index(this);
  
  $('article.tab3').removeClass('on');
  $('article.tab3').eq(i).addClass('on');
});
  </script>

</body>

</html>