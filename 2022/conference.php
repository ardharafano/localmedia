<!DOCTYPE html>
<html lang="en">
<head>
  <title>Local Media Summit</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
  <meta name="description" content="Local Media Summit">
  <meta name="keywords" content="Suara, Local Media Summit, LMS">
  <link rel="Shortcut icon" href="assets/images/favicon3.png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
  <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
  <link rel="stylesheet" href="assets/css/splide.min.css">
  <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
  <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
              
</head>

<body>

<div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="assets/images/icon-home.svg" alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#media">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#agenda">PROGRAM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#speaker">SPEAKERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#highlight">HIGHLIGHT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

<div id="agenda" class="container-fluid bg-white">
    <div class="container">
        <div class="row">
            <p class="head-det-agenda mt-4">CONFERENCE DESCRIPTION</p>

            <div class="d-flex justify-content-center row g-3">
                <div class="col-md-10 col-12">
                    <div class="bg-det-agenda ms-3 ms-md-0">
                        <div class="judul text-center mb-1">OPENING CONFERENCE</div>
                        <div class="isi ms-3">
                            <B>Future of Local Media: Business Model, Sustainability, and Audience Development</B><br> Perkembangan teknologi dan lingkungan mengharuskan kalangan media bisa beradaptasi dengan cepat demi merespon perubahan yang terjadi. Para pengelola media lokal dituntut untuk terus berinovasi dan tanggap pada berbagai
                            peluang yang bisa dimasuki.</div><br>
                        <div class="isi ms-3">Pada opening conference ini akan diulas mengenai perkembangan media lokal secara global dan spesifik Indonesia, termasuk berbagai kendala dan tantangan bagi media lokal. Juga inisiatif yang telah dilakukan kalangan stakeholder
                            untuk berkolaborasi dengan media, serta inovasi yang telah dilakukan kalangan media lokal agar bisa terus berkembang dan sustain. </div><br>
                        <div class="isi ms-3">Berbagai peluang bagi media lokal dalam landscape periklanan di Indonesia juga akan dibahas dalam conference ini, termasuk menggali peluang bisnis media dalam pengembangan ekonomi kreatif di Indonesia.</div><br>
                        <div class="isi ms-3">
                            <ul>
                                <li>Keynote speaker: Sandiaga Uno, Minister of Ministry of Tourism and Creative Economy </li>
                                <li>Speaker</li>
                                <ul>
                                    <li>Lars Bestle, Asia Head Department of IMS (International Media Support) </li>
                                    <li>Suwarjono, CEO Suara.com </li>
                                    <li>Nining, Business Director Beritajatim </li>
                                    <li>Janoe Arijanto, Dentsu/Chairman of P3I (Association of advertising company Indonesia)</li>
                                </ul>
                                <li>Moderator: Eva Danayanti, IMS Indonesia Programme Manager</li>
                            </ul>
                        </div>
                        <div class="isi ms-3"><i>Tersedia fasilitas penerjemah bahasa selama acara berlangsung.</i></div>
                    </div>
                </div>

                <div class="col-md-10 col-12">
                    <div class="bg-det-agenda ms-3 ms-md-0">
                        <div class="judul text-center mb-1">CLOSING CONFERENCE</div>
                        <div class="isi ms-3">
                            <b>Reinventing Local Media: Finding opportunities and overcoming the challenges</b></div>
                        <div class="isi ms-3">Tantangan dan masalah yang dihadapi lokal media makin beragam, seiring dengan perkembangan jaman. Pertumbuhan teknologi hingga efek pandemik telah mempengaruhi perubahan kebiasaan masyarakat dalam mengonsumsi media. Sehingga untuk terus bisa tumbuh dan berkelanjutan, media harus mampu melihat peluang baru bagi perkembangan model bisnisnya.</div><br>
                        <div class="isi ms-3">Dalam konferensi ini, kami ingin mengeksplorasi berbagai ide untuk pengembangan model bisnis bagi lokal media, termasuk menempatkannya sebagai UMKM.</div><br>
                        <div class="isi ms-3">
                            <ul>
                                <li>Keynote speaker: Teten Masduki, Minister of SMEs and Cooperatives</li>
                                <li>Speakers</li>
                                <ul>
                                    <li>Sunarti Sain, Pemimpin Redaksi Radar Selatan</li>
                                    <li>Sapto Anggoro, Anggota Dewan Pers</li>
                                </ul>
                            </ul>
                        </div>
                        <div class="isi ms-3"><i>Tersedia fasilitas penerjemah bahasa selama acara berlangsung.</i></div>
                    </div>
                </div>


            </div>

        </div>
    </div>

</div>

<div id="kontak" class="container-fluid">
  <h1 class="header">Contact Us</h1>
  <div class="container">
    <div class="row">

    <div class="col-md-12">
        <h2>For further information, you can reach us via these channels.</h2>
        <h3>
          <a href="mailto:localmediasummit@arkadiacorp.com">
            <img src="assets/images/mail.png" alt="image"/> 
            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
          </a>
            <div class="br"></div>
            <div class="wa">
              <img src="assets/images/wa.png" alt="image" width="32" height="32"/> 
                <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
            </div>
        </h3>
      </div>

    </div>
  </div>
</div>

<div class="copyright">
  <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
</div>

</body>