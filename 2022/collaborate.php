<!doctype html>
<html lang="en">

<head>
<title>Local Media Summit - Collaborate</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
  <meta name="description" content="Local Media Summit">
  <meta name="keywords" content="Suara, Local Media Summit, LMS">
  <link rel="Shortcut icon" href="assets/images/favicon.ico">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
  <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
  <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>



<body>

<div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="assets/images/icon-home.svg" alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#media">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#agenda">PROGRAM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#speaker">SPEAKERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#highlight">HIGHLIGHT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

    <div id="kolab" class="container-fluid">
        <div class="container">

        <div class="d-flex justify-content-center row g-3">
            <div class="col-md-10">
                <h4 class="text-center">COLLABORATOR</h4>
                <h5 class="text-center mb-3">Anda bisa memberikan dukungan kepada media lokal untuk berpartisipasi dalam kegiatan ini sebagai individu atau lembaga. Jika ingin berkontribusi silakan isi form di bawah ini.</h5>
            </div>
        </div>

            <form action="register_act.php" class="d-flex justify-content-center row g-3 needs-validation" novalidate>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Nama Lengkap" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Organisasi/ Media/ Perusahaan" required>
                     <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Jabatan" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                     Belum Diisi
                    </div>
                </div>

                <div class="col-md-10">
                    <input type="email" class="form-control" id="validationCustom05" placeholder="Email" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / Email Sudah Digunakan
                    </div>
                </div>

                <div class="col-md-10">
                    <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" 
                    minlength="10" maxlength = "13" min="100000000"class="form-control" id="validationCustom04" placeholder="No. Telepon/ WhatsApp" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi / No. Telepon Sudah Digunakan / Minimal 10 Digit
                    </div>
                </div>

                <div class="col-10">
                    <div class="form-check mt-5">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label" for="invalidCheck">
                            Seluruh data yang didaftarkan dapat dipertanggung jawabkan oleh Peserta
                        </label>
                        <label class="form-check-label text-secondary" for="invalidCheck">
                            <small>Arkadia Digital Media tidak akan menyebarluaskan data yang telah diberikan kepada pihak lain dan hanya digunakan selama penyelenggaraan Program Local Media Summit</small>
                        </label>
                        <div class="invalid-feedback">
                            Anda harus setuju sebelum data dikirim.
                        </div>
                    </div>
                </div>

                <div class="col-10 text-center daftar">
                    <button class="mb-5 button-kirim" type="submit" onclick="setTimeout(check, 1000);">SUBMIT</button>
                    <!-- <a href="javascript:window.open('','_self').close();">
                    <button class="mb-5 button-batal" type="button">KEMBALI</button>
                    </a> -->
                </div>
            </form>
        </div>
    </div>

<div id="kontak" class="container-fluid">
  <h1 class="header">Contact Us</h1>
  <div class="container">
    <div class="row">

    <div class="col-md-12">
        <h2>For further information, you can reach us via these channels.</h2>
        <h3>
          <a href="mailto:localmediasummit@arkadiacorp.com">
            <img src="assets/images/mail.png" alt="image"/> 
            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
          </a>
            <div class="br"></div>
            <div class="wa">
              <img src="assets/images/wa.png" alt="image" width="32" height="32"/> 
                <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
            </div>
        </h3>
      </div>

    </div>
  </div>
</div>

<div class="copyright">
  <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
</div>

    <script>
        function check() {
        swal({
                title: "Gagal!",
                text: "Data Masih Belum Lengkap",
                icon: "error",
                button: true
            });
        }
    </script>


    <script>
        (function() {
            'use strict'
            var forms = document.querySelectorAll('.needs-validation')
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()
    </script>

</body>

</html>