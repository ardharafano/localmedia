<!doctype html>
<html lang="en">

<head>
  <title>Local Media Summit</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
  <meta name="description" content="Local Media Summit">
  <meta name="keywords" content="Suara, Local Media Summit, LMS">
  <link rel="Shortcut icon" href="assets/images/favicon3.png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
  <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
  <link rel="stylesheet" href="assets/css/splide.min.css">
  <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
  <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
              
</head>



<body>

<div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="assets/images/icon-home.svg" alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#media">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#agenda">PROGRAM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#speaker">SPEAKERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#highlight">HIGHLIGHT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

  <div id="det-artikel">
    <div class="container">
    <div class="row">

        <div class="col-sm-7">
            <div class="breadcrumbs">
              <div class="bc-text">ARTIKEL</div>
            </div>
            <h1>Local Media Summit 2022 menjadi alternatif untuk pangsa anak muda milenial sekarang</h1>
            <div class="author-date">
            <h5>Chandra Iswinarno | Yosea Arga Pramudita</h5><h5>Rabu, 22 Juli 2020 | 19:39 WIB</h5>
            </div>
            <img src="assets/images/det-artikel-header.png" alt="image" width="653" height="366" class="header"/>
            <div class="figcaption">Direktur Eksekutif Charta Politika Yunarto Wijaya. [Suara.com/Fakhri Fuadi]</div>
            <h2>RK, sapaan Ridwan Kamil, yang berada pada urutan pertama mendapat prosentase sebesar 15,6 persen suara responden.</h2>
            <p class="text">Suara.com - Lembaga survei Charta Politika merilis hasil survei terkait tren tiga bulan kemudian kondisi politik, ekonomi, dan hukum pada masa pandemi Covid-19. Ridwan Kamil selaku Gubernur Jawa Barat menempati urutan pertama dalam kategori kepala daerah dengan kinerja terbaik selama masa pandemi.</p>
            <p class="text">RK, sapaan Ridwan Kamil, yang berada pada urutan pertama mendapat prosentase sebesar 15,6 persen suara responden. Selanjutnya di urutan kedua ditempati Gubernur Jawa Tengah Ganjar Pranowo dengan prosentase 13,4 persen suara responden.</p>
            <p class="text">"Kepala daerah kinerja terbaik, RK sebesar 15,6 persen," kata Direktur Eksekutif Charta Politika Yunarto Wijaya dalam diskusi yang disiarkan secara virtual, Rabu (22/7/2020).</p>
            <div class="baca-juga-body">
            <p>Baca Juga:<br>
            <a href="#">Kopda Muslimin Sempat Muntah-muntah Lalu Tewas</p></a>
            </div>
            <p class="text">"Selanjutnya, Gubernur DKI Jakarta Anies Baswedan menempati urutan ketiga dengan angka 11,8 persen suara responden. Posisi keempat ditempati Gubernur Jawa Timur Khofifah Indar Parawansa dengan angka 5,1 persen suara responden.</p>
            <p class="text">"Mas Ganjar sebesar 13,4 persen, sementara Anies 11,8 persen," ucap Yunarto.</p>
            <p class="text">Untuk posisi nomor lima ditempati oleh Wali Kota Surabaya Tri Rismaharini dengan angka 2,2 persen.</p>
            <!-- <img src="assets/images/ads.png" alt="image" width="653" height="366" class="header"/> -->
            <p class="text">"Di tataran wali kota atau bupati muncul Risma (Tri Rismaharini), Bima Arya, dan Azwar Anas," papar Yunarto.</p>
            <p class="text">Survei tersebut dilakukan Charta Politika sejak 6 hingg 12 Juli 2020 dengan metode wawancara melalui telepon. Dalam hal ini, Charta Politika melibatkan 2.000 responden.</p>
            <img src="assets/images/det-artikel-header-2.png" alt="image" width="653" height="366" class="header-2"/>
            <p class="text">Metode survei adalah simple random sampling dengan margin of error 2,19 persen dan tingkat kepercayaan 95 persen. Untuk kriteria responden, minimal 17 tahun atau memenuhi syarat menjadi pemilih di pemilu.</p>
        
            <div class="col-sm-10">
            <div class="breadcrumbs">
              <div class="bc-text">BACA JUGA</div>
            </div>
            <div class="baca-juga-footer">
              <a href="#">
              <img src="assets/images/baca-juga.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>
              
              <img src="assets/images/baca-juga.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>

              <a href="#">
              <img src="assets/images/baca-juga.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>

              <a href="#">
              <img src="assets/images/baca-juga.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>
            </div>
            </div>


        </div>

        <div class="col-sm-4">
        <div class="sticky-top sticky">
          <div class="breadcrumbs">
              <div class="bc-text">TERBARU</div>
            </div>
        <div class="baca-juga-sidebar">
              <a href="#">
              <img src="assets/images/baca-juga-sidebar.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>
              
              <a href="#">
              <img src="assets/images/baca-juga-sidebar.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>

              <a href="#">
              <img src="assets/images/baca-juga-sidebar.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>

              <a href="#">
              <img src="assets/images/baca-juga-sidebar.png" alt="image" width="270" height="151" class="baca-juga-footer"/>
              </a>
              <a href="#">
              <div class="text">Survei Charta Politika: Prabowo Menteri Kinerja Terbaik di Kabinet Jokowi</div>
              <div class="tag">News</div>
              </a>
            </div>
            </div>

        </div>

    </div>
    </div>
</div>

<div id="kontak" class="container-fluid">
  <h1 class="header">Contact Us</h1>
  <div class="container">
    <div class="row">

    <div class="col-md-12">
        <h2>For further information, you can reach us via these channels.</h2>
        <h3>
          <a href="mailto:localmediasummit@arkadiacorp.com">
            <img src="assets/images/mail.png" alt="image"/> 
            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
          </a>
            <div class="br"></div>
            <div class="wa">
              <img src="assets/images/wa.png" alt="image" width="32" height="32"/> 
                <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
            </div>
        </h3>
      </div>

    </div>
  </div>
</div>


<div class="copyright">
  <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
</div>


</body>

</html>