<!DOCTYPE html>
<html lang="en">

<head>
    <title>Local Media Summit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content="Local Media Summit">
    <meta name="keywords" content="Suara, Local Media Summit, LMS">
    <link rel="Shortcut icon" href="assets/images/favicon3.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css">
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
    <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
</head>

<body>

    <div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="assets/images/icon-home.svg" alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="index.php#media">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#agenda">PROGRAM</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#speaker">SPEAKERS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#highlight">HIGHLIGHT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

    <div id="agenda" class="container-fluid bg-white">
        <div class="container">
            <div class="row">
                <p class="head-det-agenda mt-4">WORKSHOP DESCRIPTION</p>
                <p class="head-det-agenda-2">Local Media Summit 2022</p>

                <div class="d-flex justify-content-center row g-3">
                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">1. Fundraising strategy for a Media Startup</div>
                            <div class="isi mb-3 ms-3">Membangun sebuah perusahaan media startup membutuhkan waktu, dedikasi, disiplin, dan pendanaan yang memadai. Terdapat beberapa tahap pendanaan yang perlu dilewati oleh perusahaan media startup hingga kondisi finansialnya dapat
                                dikatakan stabil.</div>
                            <div class="isi mb-3 ms-3">Pada sejumlah media local, pendanaan awal pada umummnya berasal dari dana pribadi atau investasi sejumlah individu dengan dana terbatas. Dalam perkembangannya, para pengelola startup media local kesulitan mengembangkan bisnisnya
                                pada tahap pertumbuhan karena terbatasnya modal. </div>
                            <div class="isi mb-3 ms-3">Workshop ini hendak menjawab kebutuhan para pengelola startup media local dalam pengetahuan pendanaan pada tahap pertumbuhan ini. Mulai dari merancang business plan, mengemas proposal, hingga mengenal Lembaga donor/investasi.</div>
                            <div class="isi mb-3 ms-3"><b>Speakers : </b>Devi Asmarani (Magdalene), Hendri Salim (TechinAsia)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda-2 ms-3 ms-md-0">
                            <div class="judul mt-3">2. Finding a Viable Business Model for Local Media</div>
                            <div class="isi mb-3 ms-4">Bisnis media online terlebih lagi dengan karakter lokal perlu kerja keras agar sukses. Sumber utama penghasilan media lokal saat ini masih bertumpu pada iklan. Tapi, persaingan meraih iklan sangat ketat. Bukan cuma harus bersaing
                                dengan media sejenis, pengelola media local juga harus bersaing dengan influencer di media sosial untuk merebut iklan.</div>
                            <div class="isi mb-3 ms-4">Workshop ini akan membedah model-model bisnis media local yang lebih variatif dan inovatif. Mulai dari content marketing, Community engagement, Community insight, Konten Premium, hingga model bisnis inovatif lainnya. </div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b> Aliefah Permata Fikri (MGID), Lucky Lokonoto (Dentsu/Ketum P3I)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">3. Finding Podcast Business Model</div>
                            <div class="isi mb-3 ms-4">Podcast merupakan salahsatu konten media digital yang tengah naik daun. Podcast juga merupakan ruang informasi yang memikat dan interaktif. Meski demikian, masih banyak pengelola media lokal yang belum akrab dengan model bisnis
                                podcast.
                            </div>
                            <div class="isi mb-3 ms-4">Dalam workshop ini, akan dikenalkan beragam model bisnis podcast. Sehingga para pengelola media lokal dapat memonetisasi podcast sebagai upaya meningkatkan pendapatannya. </div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b>Carl Javier (CEO Puma Podcast Philippines), La Rane Hafied (Pendiri Paberik Soeara Rakjat)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda-2 ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">4. Video Packaging and Monetization </div>
                            <div class="isi mb-3 ms-4">Video menjadi salahsatu produk media lokal yang berpotensi besar meraih cuan. Dengan monetisasi, pengelola media local dapat menghasilkan revenue dari membuat dan memposting konten video di berbagai platform, seperti YouTube,
                                DailyMotion, Facebook, TikTok dan lain-lain.</div>
                            <div class="isi mb-3 ms-4">Meski peluang ini sangat besar, tapi banyak aspek yang perlu diperhatikan dalam monetisasi video. Workshop ini akan membeberkan cara mengemas video yang berpeluang besar meraih cuan lewat berbagai platform video. </div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b> Derian Laks (Dailymotion), Prawira Maulana (Content Manager Tribun Bengkulu)</div>

                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">5. Three Fundamental Technologies for Digital Media </div>
                            <div class="isi mb-3 ms-4">Jika dulu konsumen membaca media cetak untuk mendapatkan berita, sekarang mereka lebih senang membaca berita dari media online yang bisa diakses dengan mudah melalui smartphone. Sehingga pengelola media lokal harus terus beroinovasi
                                agar medianya tetap menjadi pilihan utama dan tidak ditinggalkan pembacanya termasuk pemasang iklan.</div>
                            <div class="isi mb-3 ms-4">Sebagai media berbasis teknologi, pengelola media lokal tidak hanya harus berpegang teguh pada kualitas konten, tapi bagaimana menggunakan teknologi. Workshop ini akan membedah teknologi-teknologi penting dan mendasar bagi
                                media local.
                            </div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b> AWS (tbc)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda-2 ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">6. Transforming Legacy Media to Digital Media</div>
                            <div class="isi mb-3 ms-4">Era disrupsi dan pandemi telah memicu rontoknya berbagai media cetak. Oleh karena itu, mau-tak-mau media cetak harus bertransformasi dan mengikuti tren industri saat ini agar tetap bertahan. Di antaranya, dengan menyeriusi
                                versi digital.</div>
                            <div class="isi mb-3 ms-4">DWorkshop ini akan membedah kiat transformasi digital yang dilakukan beberapa pengelola media local berbasis cetak. Bagaimana proses transformasinya, hingga cara meningkatkan revenue dari produk cetak hingga digital.</div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b>Suwarmin (Solo Pos), Subagja Hamara (Harapan Rakyat)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">7. Business Ecosystem for Local Media Publisher</div>
                            <div class="isi mb-3 ms-4">Traffic merupakan salahsatu aspek penting pada media digital. Dengan traffic yang tinggi, peluang meraih pendapatan semakin tinggi. Salahsatu upaya meningkatkan traffic adalah dengan melakukan optimalisasi search engine dan
                                sosial media. </div>
                            <div class="isi mb-3 ms-4">Pada workshop ini, akan dibahas sejumlah kiat untuk melakukan optimasi search engine dan media sosial. Sehingga bisa mendorong peningkatan traffic media-media lokal. </div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b>Yos Kusuma (Google Indonesia)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda-2 ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">8. How to Generate Income From Social Media and SEO</div>
                            <div class="isi mb-3 ms-4">Kini sosial media bukan hanya fitur pelengkap pada media digital. Selain berfungsi sebagai sarana branding dan distribusi konten, sosial media juga memiliki potensi untuk meraih pendapatan.</div>
                            <div class="isi mb-3 ms-4">Dalam workshop ini akam disampaikan berbagai potensi bisnis dalam media sosial yang dapat dilakukan oleh para pengelola media local.</div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b>Henrik Grunnet (IMS), Dimas Sagita (Suara.com)</div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">9. Collaboration Ideas for Local Media.</div>
                            <div class="isi mb-3 ms-4">Perkembangan teknologi digital menuntut pengelola media lokal untuk berani melakukan berbagai inovasi, bereksperimen dalam mencari model bisnis yang tepat. Tentu saja harus juga melakukan transformasi digital untuk menciptakan
                                sumber pendapatan demi keberlangsungan bisnis media.</div>
                            <div class="isi mb-3 ms-4">Workshop kali ini akan memperkenalkan model bisnis kolaboratif. Berkolaborasi dengan beragam media yang mempunyai ide dan semangat yang sama, akan menciptakan ekosistem yang mampu meraih revenue. Kolaborasi juga memperkecil
                                sumberdaya, karena para media kolaborator kan berbagi sumberdaya untuk meraih tujuan bersama.</div>
                            <div class="isi mb-3 ms-4"><b>Speakers : </b>Ahmad Nasir (Indotnesia), Rendy Sadikin (YourSay) </div>
                        </div>
                    </div>

                    <div class="col-md-10 col-12">
                        <div class="bg-det-agenda-2 ms-3 ms-md-0">
                            <div class="judul mt-3 mb-1">10. Building The Audience from The Local Community</div>
                            <div class="isi mb-3 ms-4">Membangun audien bagi media lokal sebuah keniscayaan. Tentu saja bukan hal mudah membangun kepercayaan pada media lokal dengan sumber daya yang terbatas. Tapi ada peluang di depan mata yang nyaris dilupakan para pengelola media
                                lokal, yakni menghadirkan informasi publik bagi masyarakat dimana media tersebut berdiri.</div>
                            <div class="isi mb-3 ms-4">Workshop kali ini akan memberikan pengalaman kolaborasi antara lembaga masyarakat sipil dengan media lokal dalam melayani informasi komunitasnya. Dengan kolaborasi ini dapat menumbuhkan audien yang loyal.</div>
                            <div class="isi mb-3 ms-4"><b>Speaker : </b>Lasma Natalia (LBH Bandung), Trijoko Her Riadi (BandungBergerak), Catur Ratna Wulandari (DigitalMama)</div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div id="kontak" class="container-fluid">
        <h1 class="header">Contact Us</h1>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h2>For further information, you can reach us via these channels.</h2>
                    <h3>
                        <a href="mailto:localmediasummit@arkadiacorp.com">
                            <img src="assets/images/mail.png" alt="image" />
                            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
                        </a>
                        <div class="br"></div>
                        <div class="wa">
                            <img src="assets/images/wa.png" alt="image" width="32" height="32" />
                            <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                            <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                            <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
                        </div>
                    </h3>
                </div>

            </div>
        </div>
    </div>

    <div class="copyright">
        <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
    </div>

</body>