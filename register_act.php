<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="Shortcut icon" href="./2022/assets/images/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Local Media Summit">
    <meta name="keywords" content="Suara, Local Media Summit, LMS">

    <meta property="og:title" content="locammediasummit.com" />
    <meta property="og:description" content="Local Media Summit - Emerging Local Media Business Viability to Represent the Unrepresented" />
    <meta property="og:url" content="https://microsite.suara.com/localmediasummit" />
    <meta property="og:image" content="assets/images/hg_lms.jpg" />

    <title>Local Media Summit - Pendaftaran Berhasil</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/checkout/">
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
    <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="assets/css/splide.min.css">


</head>



<body>

<div id="nav">
    <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
        <div class="container-xl">
            <a class="navbar-brand" href="#home"><img src="./2022/assets/images/icon-home.svg" alt="image" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

            <div class="nav-umkm">
                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="#about">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="#media">NETWORK</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#news">NEWS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#faq">FAQ</a>
                        </li>
                        <!-- <li class="nav-item">
                        <a class="nav-link" href="#agenda">PROGRAM</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#speaker">SPEAKERS</a>
                    </li>
                      <li class="nav-item">
                            <a class="nav-link" href="#highlight">HIGHLIGHT</a>
                        </li> -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              ARCHIEVE
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item-lms" href="2022/index.php" target="_blank">LMS 2022</a></li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#kontak">CONTACT</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="navbarsExample07XL">
                <div class="desktop">
                    <ul class="navbar-nav nav-umkm-right me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="./2022/assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="./2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="./2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                        </li>
                        <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                  <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                  </ul>
                </li> -->

                    </ul>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="navbarsExample07XL">
                <div class="mobile">
                    <ul class="nav-umkm-right me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.instagram.com/localmediasummit/" target="_blank"><img src="./2022/assets/images/ig.svg" alt="image" width="25" height="25" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img src="./2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img src="./2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                        </li>
                        <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    REGISTER
                  </a>
                  <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a></li>
                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a></li>
                  </ul>
                </li> -->

                    </ul>
                </div>
            </div>

        </div>
    </nav>
    </div>

    <!-- <div id="home" class="container-fluid">
        <div class="container">
            <div class="row">


                <div class="col-lg-12">
                    <img src="./2022/assets/images/header-text5.svg" alt="image" class="header-text" widht="973" height="331" />
                    <img src="./2022/assets/images/header-text5-mobile.svg" alt="image" class="header-text-mobile" widht="649" height="435" />
                    <h5 class="text-center text-white mt-2">Perpustakaan Nasional, Jalan Medan Merdeka Selatan, Jakarta Pusat</h5>
                    <h1 class="date mt-2">October 27-28, 2022</h1>
                    <div class="body-count">
                            <h1 class="sub-date mt-2">Event Countdown</h1>
                            <div class="count" id="count"></div>
                        </div>
                </div>

            </div>

        </div>
    </div> -->

    <div id="home">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <video id="background-video" autoplay loop muted poster="./2022/assets/images/about.png">
                  <source src="assets/video/header.mp4" type="video/mp4">
                </video>

                    <div class="overlay">
                        <!-- <h1>MENUJU VISI INDONESIA 2045</h1>
                    <p>KOTA DUNIA UNTUK SEMUA</p> -->
                        <!-- <div id="myBtn" onclick="myFunction()">

                            <div class="button" onclick="this.classList.toggle('active')">
                                <div class="background" x="0" y="0" width="200" height="200"></div>
                                <div class="icon" width="200" height="200">
                                    <div class="part left" x="0" y="0" width="200" height="200" fill="#fff"></div>
                                    <div class="part right" x="0" y="0" width="200" height="200" fill="#fff"></div>
                                </div>
                                <div class="pointer"></div>
                            </div>

                        </div> -->
                    </div>

                </div>

            </div>
        </div>
    </div>


    <div id="head" class="container">
        <section class="splide head" aria-label="Splide Basic HTML Example">
            <div class="splide__track">
                <ul class="splide__list">
                    <li class="splide__slide">
                        <div class="container">
                            <div class="row">
                                <div class="media">
                                    <a href="assets/images/lms-2023.svg" target="_blank">
                                        <img src="assets/images/lms-2023.svg" alt="image" class="hg mt-1" width="446" height="210" />
                                    </a>
                                </div>
                                <h3>INITIATORS</h3>
                                <div class="img-initiators">
                                    <img src="assets/images/suara-white.svg" alt="image" width="177" height="19" class="suara" />
                                    <img src="assets/images/ims-white.svg" alt="image" width="89" height="33" class="ims" />
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="splide__slide">
                        <div class="container">
                            <div class="row">
                                <div class="media">
                                    <a href="assets/images/lms-2023.svg" target="_blank">
                                        <img src="assets/images/lms-2023.svg" alt="image" class="hg mt-1" width="446" height="210" />
                                    </a>
                                </div>
                                <h3>INITIATORS</h3>
                                <div class="img-initiators">
                                    <img src="assets/images/suara-white.svg" alt="image" width="177" height="19" class="suara" />
                                    <img src="assets/images/ims-white.svg" alt="image" width="89" height="33" class="ims" />
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="splide__slide">
                        <div class="container">
                            <div class="row">
                                <div class="media">
                                    <a href="assets/images/lms-2023.svg" target="_blank">
                                        <img src="assets/images/lms-2023.svg" alt="image" class="hg mt-1" width="446" height="210" />
                                    </a>
                                </div>
                                <h3>INITIATORS</h3>
                                <div class="img-initiators">
                                    <img src="assets/images/suara-white.svg" alt="image" width="177" height="19" class="suara" />
                                    <img src="assets/images/ims-white.svg" alt="image" width="89" height="33" class="ims" />
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>


    <div id="bg">
        <div id="about">
            <div class="container">
                <div class="row">
                    <h1 class="header">ABOUT</h1>
                    <p>Local Media Summit 2022 ini merupakan pertemuan para pengelola media lokal pertama dan terbesar di Indonesia. Media lokal di sini adalah media yang memiliki lingkup di sebuah wilayah geografis tertentu atau menyasar segmen masyarakat
                        tertentu.
                    </p>
                    <p>Dalam acara ini, para pengelola media lokal akan berjumpa dengan perusahaan, agensi iklan, lembaga donor, lembaga pemerintah, platforms Internet, dan solusi teknologi. Ini adalah kesempatan media lokal memperkenalkan berbagai produk
                        dan layanannya pada berbagai kalangan yang lebih luas. Kemudian para mereka juga dapat berjumpa dengan para pakar dalam memecahkan berbagai masalah dan tantangan yang mereka hadapi dalam pengembangan newsroom dan bisnisnya.</p>
                    <p>Sementara investor media dan Lembaga donor bisa menemukan partner yang tepat di forum ini. Pengiklan, lembaga pemerintah, platform, atau penyedia solusi teknologi bisa berbicara dengan media lokal yang mereka ingin temui agar bisa
                        lebih memahami fokus isu, layanan, atau siapa audience mereka.</p>
                    <p>Kegiatan ini juga akan menghasilkan catatan dan rekomendasi yang dapat digunakan oleh media lokal maupun stakeholder nya untuk mendorong pengembangan bisnis media lokal di Indonesia.</p>
                </div>
            </div>
        </div>

        <div id="video">
            <h1>VIDEO</h1>
            <h3>Local Media Summit 2022</h3>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/deMh5ElnhoY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        </div>

        <div id="foto">
            <div class="container">
                <h1>FOTO</h1>
                <h3>Local Media Summit 2022</h3>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-lg-center align-content-lg-center flex-lg-wrap overflow-scroll">
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                            <img src="assets/images/foto.png" alt="image" width="268" height="268" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="agenda" class="container-fluid">
            <section class="splide agenda" aria-label="Basic Structure Example">
                <h1 class="header">AGENDA</h1>
                <h3>Local Media Summit 2022</h3>
                <div class="splide__track">
                    <ul class="splide__list">
                        <li class="splide__slide">
                            <!-- <h2 class="header mt-4">RUNDOWN</h2> -->
                            <div class="container">
                                <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">WORKSHOP DAY 1</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">WORKSHOP DAY 2</button>
                                    </li>
                                </ul>

                                <div class="tab-content justify-content-center" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">

                                        <div class="ps-md-5 header-workshop">
                                            <h1>WORKSHOP <b class="bold">DAY 1 | Conference</b></h1>
                                            <h2 class="pe-md-3">27 Oktober 2022</h2>
                                        </div>

                                        <div class="isi-workshop">

                                            <table class="table">
                                                <div class="distance-top"></div>
                                                <tr>
                                                    <td class="jam"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 09.00 - 09.15 WIB</td>
                                                    <td>Opening Ceremony<br>Sambutan Kepala Perpustakaan Nasional Indonesia M Syarif Bando<br> Pembukaan oleh CEO Suara.com
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="CEO Suara">Suwarjono</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 09.15 - 09.30 WIB</td>
                                                    <td>Keynote Speech<br>Future of Local Media: Business Model, Sustainability, and Audience Development<br>
                                                        <!-- <b>Speakers:</b>
                                              <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Minister of Tourism and Creative Economy">Sandiaga Uno</div> -->
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 09.30 - 12.00 WIB</td>
                                                    <td>Opening Conference<br>Future of Local Media: Business Model, Sustainability, and Audience Development<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="IMS Head of Department Asia">Lars Bestle</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Direktur Bisnis BeritaJatim">Saptini Darmaningrum</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Ketua Persatuan Perusahaan Periklanan Indonesia">Janoe Arijanto</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Country Manager AWS Indonesia">Gunawan Susanto</div>,<br>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="IMS Programme Manager Indonesia">Eva Danayanti</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam" rowspan="3"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 13.00 - 14.30 WIB</td>
                                                    <td>Workshop 1<br>Fundraising strategy for a Media Startup<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="CO-Founder and Chief Editor Magdelene.co">Devi Asmarani</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="CEO TechInAsia Indonesia">Hendri Salim</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Workshop 2<br>Finding a viable business model for local media <br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Sr. Account Manager MGID">Aliefah Permata Fikri</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Pemimpin Umum & Pemimpin Redaksi BeritaJatim">Dwi Eko Lokononto</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Workshop 3<br>Finding Podcast Business Model<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="CEO Puma Podcast Philippines">Carl Javier</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Pendiri Paberik Soeara Rakjat">La Rane Hafied</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam" rowspan="3"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 15.00 - 16.30 WIB</td>
                                                    <td>Workshop 4<br>Video packaging and monetization<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Country Director Dailymotion">Derian Laks</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Content Manager Tribun Bengkulu">Prawira Maulana</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Workshop 5<br>How to generate income from social media & SEO <br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="IMS Media Advisor">Henrik Grunnet</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Head of Growth Suara.com">Dimas Sagita</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td">Sharing Ideas 1<br>Building The Audience from The Local Community<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Director LBH Bandung">Lasma Natalia</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Founder & Pemimpin Redaksi BandungBergerak">Tri Joko Heriadi</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Editor in Chief DigitalMama">Catur Ratna Wulandari</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="jam"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 19.00 - 22.00 WIB</td>
                                                    <td class="td">Networking Night<br><b>Host:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Founder HAMburger Podcast">Inayah Wahid</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Produser HAMburger Podcast">Jesse Adam Halim</div>
                                                    </td>
                                                </tr>

                                            </table>

                                        </div>

                                    </div>

                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">

                                        <div class="ps-md-5 header-workshop">
                                            <h1>WORKSHOP <b class="bold">DAY 2 | Workshop</b></h1>
                                            <h2 class="pe-md-3">28 Oktober 2022</h2>
                                        </div>

                                        <div class="isi-workshop">

                                            <table class="table">
                                                <div class="distance-top"></div>
                                                <tr>
                                                    <td class="jam" rowspan="3"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 09.00 - 10.30 WIB</td>
                                                    <td>Coaching Clinic 1<br>Bagaimana kolaborasi dengan Suara.com</td>
                                                </tr>
                                                <tr>
                                                    <td>Coaching Clinic 2<br>3 Fundamental Technologies for Digital Media</td>
                                                </tr>
                                                <tr>
                                                    <td>Coaching Clinic 3<br>Mengenali 3 besar hoax di Indonesia dan tips menghadapi nya</td>
                                                </tr>

                                                <tr>
                                                    <td class="jam"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 10.00 - 11.30 WIB</td>
                                                    <td>Workshop 6<br>Hyperlocal Strategy for Local Media<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Manager Produksi Suara Surabaya Media">Eddy Prastyo</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Founder dan CEO BatamNews">Zuhri Muhammad</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 10.30 - 12.00 WIB</td>
                                                    <td>Workshop 7<br>Transforming Legacy Media to Digital Media<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Direktur Bisnis dan Konten Solopos Group">Suwarmin</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="CEO Harapan Rakyat">Subagja Hamara</div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam" rowspan="3"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 13.30 - 15.00 WIB</td>
                                                    <td>Workshop 8<br>Business Ecosystem for Local Media Publisher<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="News Partner manager at Google">Yos Kusuma</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Redaktur Pelaksana Suara.com">Arsito Hidayatullah</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Workshop 9<br>Digital Security for Media Publishers <br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Co-founder ICT Watch">Heru Tjatur</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Sharing Ideas 2<br>Koperasi Media: Kolaborasi Media dan Konten Kreator<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="CEO Indotnesia">Ahmad Nasir</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Community Development Manager Yoursay.id">Rendy Sadikin</div>
                                                        <!-- <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Corporate Communication Director Danone Indonesia">arif Mujahidin</div> -->
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="jam" rowspan="2"><img src="./2022/assets/images/bullet.png" alt="image" class="bullet" /> 15.00 - 17.00 WIB</td>
                                                    <td>Keynote Speech<br>Reinventing Local Media: Finding opportunities and overcoming the challenges<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Menteri Koperasi dan UMKM">Teten Masduki</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td">Closing Conference<br>Reinventing Local Media: Finding opportunities and overcoming the challenges<br><b>Speakers:</b>
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Managing Partner Inventure">Yuswohadi</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Editor in-Chief Radar Selatan, Sulawesi Selatan">Sunarti Sain</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Anggota Dewan Pers">Sapto Anggoro</div>,
                                                        <div class="btn-tooltip text-primary" data-toggle="tooltip" data-placement="top" title="Corporate Communication Director Danone Indonesia">Arif Mujahidin</div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div id="part">
                                <h1 class="header mt-4 mb-md-3">PESERTA</h2>
                                    <div class="container">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <img src="./2022/assets/images/part.png" alt="image" class="part" width="409" height="408" />
                                            </div>

                                            <div class="col-md-8">
                                                <div class="about">
                                                    <!-- <div class="distance-top"></div> -->
                                                    <h2>Peserta Lokal Media Summit 2022 secara umum terbagi dua kelompok, yakni pengelola media lokal dan stakeholder bisnis media.</h2>
                                                    <h1><b>Media Lokal</b></h1>
                                                    <h2>300 pengelola media local setingkat CEO dan Pemimpin Redaksi.</h2>
                                                    <h1><b>Stakeholder Bisnis Media</b></h1>
                                                    <h2>100 peserta dari kalangan media investors, big advertisers, donors, government agencies, platforms, Akademisi, NGO dan kalangan umum lainnya yang tertarik dengan perkembangan bisnis media.</h2>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div id="event">
                                <a href="agenda.php" aria-label="agenda">
                                    <h1 class="header">CONTENT EVENT</h1>
                                </a>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <a href="./2022/conference.php" aria-label="agenda">
                                                <img src="./2022/assets/images/e1.svg" alt="image" class="event-left" width="428" height="194" />
                                            </a>
                                            <a href="agenda.php" aria-label="agenda">
                                                <img src="./2022/assets/images/e2.svg" alt="image" class="event-left" width="428" height="194" />
                                            </a>
                                        </div>

                                        <div class="col-md-4">
                                            <a href="./2022/coaching.php" aria-label="agenda">
                                                <img src="./2022/assets/images/e3.svg" alt="image" class="event2" width="200" height="408" />
                                                <img src="./2022/assets/images/e6.svg" alt="image" class="event3" width="428" height="194" />
                                            </a>
                                        </div>

                                        <div class="col-md-4">
                                            <a href="./2022/wof.php" aria-label="agenda">
                                                <img src="./2022/assets/images/e4.svg" alt="image" class="event-right" width="428" height="194" />
                                            </a>
                                            <a href="./2022/networking-night.php" aria-label="agenda">
                                                <img src="./2022/assets/images/e5.svg" alt="image" class="event-right" width="428" height="194" />
                                            </a>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide">
                            <div id="venue">
                                <h1 class="header">WAKTU DAN TEMPAT</h1>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <img src="./2022/assets/images/venue.png" alt="image" class="venue" width="409" height="408" />
                                        </div>

                                        <div class="col-md-8">
                                            <div class="about">
                                                <h1><b>Waktu</b></h1>
                                                <h2>Kamis - Jumat<br> 27-28 Oktober 2022</h2>
                                                <hr>
                                                <h1><b>Tempat</b></h1>
                                                <h2>Gedung Perpustakaan Nasional,<br> Jl. Medan Merdeka Selatan No.11<br> Jakarta 10110</h2>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </section>

        </div>

        <div id="speaker" class="container-fluid">
            <div class="container">
                <div class="row">
                    <a href="speaker.php">
                        <h1 class="header mb-3">SPEAKERS</h1>
                        <h3>Local Media Summit 2022</h3>
                    </a>
                    <!-- <div class="col-lg-13 col-6">
        <div class="border">
          <div class="speaker-img">
            <img src="./2022/assets/images/airlangga.png" alt="image" width="125" height="125"/>
          </div>
          <a href="./2022/speaker.php#airlangga" target="_blank">
            <div class="nama mt-3">Airlangga Hartanto</div>
            <div class="jabatan">Menteri Koordinator Perekonomian</div>
          </a>
        </div>
      </div> -->

                    <!-- <div class="col-lg-13 col-6">
        <div class="border">
          <div class="speaker-img">
            <img src="./2022/assets/images/sandiuno.png" alt="image" width="125" height="125"/>
          </div>
          <a href="./2022/speaker.php#sandiuno" target="_blank">
            <div class="nama mt-3">Sandiaga Uno</div>
            <div class="jabatan">Menteri Pariwisata dan Ekonomi Kreatif</div>
          </a>
        </div>
      </div> -->

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/teten.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/./2022/speaker.php#teten" target="_blank">
                                <div class="nama mt-3">Teten Masduki</div>
                                <div class="jabatan">Menteri Koperasi dan UMKM</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/sapto.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#sapto" target="_blank">
                                <div class="nama mt-3">Sapto Anggoro</div>
                                <div class="jabatan">Anggota Dewan Pers </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/suwarjono.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#suwarjono" target="_blank">
                                <div class="nama mt-3">Suwarjono</div>
                                <div class="jabatan">CEO Suara.com</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/lars.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#lars" target="_blank">
                                <div class="nama mt-3">Lars H. Bestle</div>
                                <div class="jabatan">IMS Head of Department Asia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/yos.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#yos" target="_blank">
                                <div class="nama mt-3">Yos Kusuma</div>
                                <div class="jabatan">News Partner Manager at Google</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/gunawan1.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#gunawan" target="_blank">
                                <div class="nama mt-3">Gunawan Susanto</div>
                                <div class="jabatan">Country Manager AWS (Indonesia)</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/eva.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#eva" target="_blank">
                                <div class="nama mt-3">Eva Danayanti</div>
                                <div class="jabatan">IMS Programme Manager Indonesia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/hendri.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#hendri" target="_blank">
                                <div class="nama mt-3">Hendri Salim</div>
                                <div class="jabatan">CEO TechInAsia Indonesia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/janoe.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#janoe" target="_blank">
                                <div class="nama mt-3">Janoe Arijanto</div>
                                <div class="jabatan">Ketua Persatuan Perusahaan Periklanan Indonesia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/devi.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#devi" target="_blank">
                                <div class="nama mt-3">Devi Asmarani</div>
                                <div class="jabatan">CO-Founder and Chief Editor Magdelene.co</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/yuswohady.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#yuswohady" target="_blank">
                                <div class="nama mt-3">Yuswohady</div>
                                <div class="jabatan">Managing Partner Inventure</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/derian_laks.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#derian" target="_blank">
                                <div class="nama mt-3">Derian Laks</div>
                                <div class="jabatan">Country Director Dailymotion</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/arief.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#arif" target="_blank">
                                <div class="nama mt-3">Arif Mujahidin</div>
                                <div class="jabatan">Corporate Communication Director Danone Indonesia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/henrik.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#henrik_grunnet" target="_blank">
                                <div class="nama mt-3">Henrik Grunnet</div>
                                <div class="jabatan"> IMS Media Advisor</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/sunarti.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#sunarti" target="_blank">
                                <div class="nama mt-3">Sunarti Sain</div>
                                <div class="jabatan">Editor in-Chief Radar Selatan, Sulawesi Selatan</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/ahmadnasir.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#ahmad" target="_blank">
                                <div class="nama mt-3">Ahmad Nasir</div>
                                <div class="jabatan">CEO Indotnesia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/arsito.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#arsito" target="_blank">
                                <div class="nama mt-3">Arsito Hidayatullah</div>
                                <div class="jabatan">Redaktur Pelaksana Suara.com</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/catur-ratna.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#ratna" target="_blank">
                                <div class="nama mt-3">Catur Ratna Wulandari</div>
                                <div class="jabatan">Editor in Chief DigitalMama</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/inayah-wahid.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#inayah" target="_blank">
                                <div class="nama mt-3">Inayah Wahid</div>
                                <div class="jabatan">Founder HAMburger Podcast</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/dimas.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#dimas" target="_blank">
                                <div class="nama mt-3">Dimas Sagita</div>
                                <div class="jabatan">Head of Growth Suara.com</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/subagja.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#subagja" target="_blank">
                                <div class="nama mt-3">Subagja Hamara</div>
                                <div class="jabatan">CEO Harapan Rakyat</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/lasma.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#lasma" target="_blank">
                                <div class="nama mt-3">Lasma Natalia</div>
                                <div class="jabatan">Direktur LBH Bandung</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/rendy.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#rendy" target="_blank">
                                <div class="nama mt-3">Rendy Sadikin </div>
                                <div class="jabatan">Community Development Manager Yoursay.id</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/suwarmin.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#suwarmin" target="_blank">
                                <div class="nama mt-3">Suwarmin</div>
                                <div class="jabatan">Direktur Bisnis dan Konten Solopos Group</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/bram2.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#bram" target="_blank">
                                <div class="nama mt-3">Bram Bravo</div>
                                <div class="jabatan">Head of Digital Ad Ops & Programmatic Advertising Suara.com</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/trijoko.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#trijoko" target="_blank">
                                <div class="nama mt-3">Tri Joko Her Riadi</div>
                                <div class="jabatan">Founder BandungBergerak</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/septini1.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#septini" target="_blank">
                                <div class="nama mt-3">Saptini Darmaningrum</div>
                                <div class="jabatan">Direktur Usaha BeritaJatim</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/prawira.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#prawira" target="_blank">
                                <div class="nama mt-3">Prawira Maulana</div>
                                <div class="jabatan">Content Manager Tribun Bengkulu</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/lerane.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#lerane" target="_blank">
                                <div class="nama mt-3">La Rane Hafied</div>
                                <div class="jabatan">Pendiri Paberik Soeara Rakjat</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/aliefah.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#aliefah" target="_blank">
                                <div class="nama mt-3">Aliefah Permata Fikri</div>
                                <div class="jabatan">Sr. Account Manager MGID</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/martha1.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#martha" target="_blank">
                                <div class="nama mt-3">Martha Rinna Dewi</div>
                                <div class="jabatan">Territory Manager AWS (Indonesia)</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/jesse.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#jesse" target="_blank">
                                <div class="nama mt-3">Jesse Adam Halim</div>
                                <div class="jabatan">Produser HAMburger Podcast</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/septiaji.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#septiaji" target="_blank">
                                <div class="nama mt-3">Septiaji Eko Nugroho</div>
                                <div class="jabatan">Ketua Presidium Mafindo</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/carl.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#carl_javier" target="_blank">
                                <div class="nama mt-3">Carl Javier</div>
                                <div class="jabatan">CEO Puma Podcast Philippines</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/eddy.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#eddy" target="_blank">
                                <div class="nama mt-3">Eddy Prastyo</div>
                                <div class="jabatan">Manager Produksi Suara Surabaya Media</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/zuhri.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#zuhri" target="_blank">
                                <div class="nama mt-3">Zuhri Muhammad</div>
                                <div class="jabatan">Founder dan CEO BatamNews</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/upi.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#upi" target="_blank">
                                <div class="nama mt-3">Upi Asmaradhana</div>
                                <div class="jabatan">Founder-CEO Kabar Group Indonesia</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/dwieko.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#dwieko" target="_blank">
                                <div class="nama mt-3">Dwi Eko Lokononto</div>
                                <div class="jabatan">Pemimpin Umum & Pemimpin Redaksi BeritaJatim</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-13 col-6">
                        <div class="border">
                            <div class="speaker-img">
                                <img src="./2022/assets/images/heru.png" alt="image" width="125" height="125" />
                            </div>
                            <a href="./2022/speaker.php#heru" target="_blank">
                                <div class="nama mt-3">Heru Tjatur</div>
                                <div class="jabatan">Co-Founder ICT Watch</div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div id="sponsor" class="container-fluid">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <h1 class="header mt-3">INITIATORS</h1>
                        <div class="sponsor">
                            <a href="https://suara.com/" target="_blank">
                                <img src="./2022/assets/images/suara.png" alt="image" width="269" height="38" />
                            </a>
                            <a href="https://www.mediasupport.org/" target="_blank">
                                <img src="./2022/assets/images/lms.png" alt="image" width="175" height="74" />
                            </a>
                        </div>

                        <!-- opsi kolaborator baru, minta dibawah IMS dan suara -->
                        <!-- <div class="media">
                    <a href="https://www.mediasupport.org/" target="_blank">
                        <img src="./2022/assets/images/ims.svg" alt="image" width="175" height="74" />
                    </a>
                    <a href="https://suara.com/" target="_blank">
                        <img src="./2022/assets/images/suara.svg" alt="image" width="269" height="38" />
                    </a>
          </div> -->
                    </div>

                </div>
            </div>
        </div>

        <hr>

        <div id="media" class="container-fluid">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <h1 class="header">NETWORK</h1>
                        <h3>Local Media Summit 2022</h3>
                        <div class="media">
                            <a href="https://www.youtube.com/channel/UCIPBADJECZeHH_eYCWu-jnQ?app=desktop" target="_blank">
                                <img src="./2022/assets/images/hamburger.png" alt="image" width="72" height="72" />
                            </a>
                            <a href="https://www.babad.id/" target="_blank">
                                <img src="./2022/assets/images/babad.png" alt="image" width="105" height="35" />
                            </a>
                            <a href="https://bincangperempuan.com/" target="_blank">
                                <img src="./2022/assets/images/bincang-perempuan.png" alt="image" width="70" height="70" />
                            </a>
                            <a href="https://digitalmama.id/" target="_blank">
                                <img src="./2022/assets/images/digitalmama.png" alt="image" width="84" height="40" />
                            </a>
                            <a href="https://indotnesia.suara.com/" target="_blank">
                                <img src="./2022/assets/images/indotnesia.png" alt="image" width="100" height="66" />
                            </a>
                            <a href="https://www.inidata.id/" target="_blank">
                                <img src="./2022/assets/images/inidata.png" alt="image" width="110" height="20" />
                            </a>
                            <a href="https://kaltimkece.id/" target="_blank">
                                <img src="./2022/assets/images/kaltimkece.png" alt="image" width="40" height="40" />
                            </a>
                            <a href="https://katongntt.com/" target="_blank">
                                <img src="./2022/assets/images/katong-ntt.png" alt="image" width="121" height="32" />
                            </a>
                            <a href="https://kutub.id/kemana-seharusnya-pemuda-bergerak/" target="_blank">
                                <img src="./2022/assets/images/kutub.png" alt="image" width="50" height="52" />
                            </a>
                            <a href="https://www.magdalene.co/" target="_blank">
                                <img src="./2022/assets/images/magdalene.png" alt="image" width="115" height="21" />
                            </a>
                            <a href="https://purwokertokita.com/" target="_blank">
                                <img src="./2022/assets/images/purwokertokita.png" alt="image" width="40" height="40" />
                            </a>
                            <a href="https://beritajatim.com/" target="_blank">
                                <img src="./2022/assets/images/beritajatim.png" alt="image" width="170" height="23" />
                            </a>
                            <a href="https://harapanrakyat.com/" target="_blank">
                                <img src="./2022/assets/images/harapanrakyat.png" alt="image" width="170" height="56" />
                            </a>
                            <a href="https://www.kabarmakassar.com/" target="_blank">
                                <img src="./2022/assets/images/kabarmakassar.png" alt="image" width="141" height="35" />
                            </a>
                            <a href="https://www.sukabumiupdate.com/" target="_blank">
                                <img src="./2022/assets/images/sukabumi.png" alt="image" width="100" height="27" />
                            </a>
                            <a href="https://www.telisik.id/" target="_blank">
                                <img src="./2022/assets/images/telisik.png" alt="image" width="100" height="40" />
                            </a>
                            <a href="https://www.beritabali.com/" target="_blank">
                                <img src="./2022/assets/images/beritabali.png" alt="image" width="150" height="30" />
                            </a>
                            <a href="https://www.jabarnews.com/" target="_blank">
                                <img src="./2022/assets/images/jabarnews.png" alt="image" width="150" height="23" />
                            </a>
                            <a href="https://www.riauonline.co.id/" target="_blank">
                                <img src="./2022/assets/images/riauonline.png" alt="image" width="150" height="35" />
                            </a>
                            <a href="https://zonautara.com/" target="_blank">
                                <img src="./2022/assets/images/zonautara.png" alt="image" width="50" height="66" />
                            </a>
                            <a href="https://terakota.id/" target="_blank">
                                <img src="./2022/assets/images/terakota.png" alt="image" width="150" height="42" />
                            </a>
                            <a href="https://kieraha.com/" target="_blank">
                                <img src="./2022/assets/images/kieraha.png" alt="image" width="100" height="23" />
                            </a>
                            <a href="https://kabarpapua.co/" target="_blank">
                                <img src="./2022/assets/images/kabarpapua.png" alt="image" width="150" height="21" />
                            </a>
                            <a href="https://garak.id/" target="_blank">
                                <img src="./2022/assets/images/garak.png" alt="image" width="100" height="23" />
                            </a>
                            <a href="https://ciayumajakuning.id/" target="_blank">
                                <img src="./2022/assets/images/ciayumajakuning.png" alt="image" width="60" height="61" />
                            </a>
                            <a href="https://kitapunya.id/" target="_blank">
                                <img src="./2022/assets/images/kitapunya.png" alt="image" width="120" height="30" />
                            </a>
                            <a href="https://wongkito.co/" target="_blank">
                                <img src="./2022/assets/images/wongkito.png" alt="image" width="140" height="24" />
                            </a>
                            <a href="https://ekuatorial.com/" target="_blank">
                                <img src="./2022/assets/images/ekuatorial.png" alt="image" width="140" height="52" />
                            </a>
                            <a href="https://kaidah.id/" target="_blank">
                                <img src="./2022/assets/images/kaidah.png" alt="image" width="110" height="33" />
                            </a>
                            <a href="https://lensaku.id/" target="_blank">
                                <img src="./2022/assets/images/lensaku.png" alt="image" width="150" height="38" />
                            </a>
                            <a href="https://www.sudutpayakumbuh.com/" target="_blank">
                                <img src="./2022/assets/images/sudut.png" alt="image" width="70" height="70" />
                            </a>
                            <a href="https://www.terasmanado.com/" target="_blank">
                                <img src="./2022/assets/images/terasmanado.png" alt="image" width="200" height="51" />
                            </a>
                            <a href="https://www.kanalmetro.com/" target="_blank">
                                <img src="./2022/assets/images/kanalmetro.png" alt="image" width="130" height="27" />
                            </a>
                            <a href="https://www.pojoksatu.id/" target="_blank">
                                <img src="./2022/assets/images/pojoksatu.png" alt="image" width="140" height="22" />
                            </a>
                            <a href="https://www.referensia.id/" target="_blank">
                                <img src="./2022/assets/images/referensia.png" alt="image" width="140" height="31" />
                            </a>
                            <a href="https://www.acehinfo.id/" target="_blank">
                                <img src="./2022/assets/images/acehinfo.png" alt="image" width="120" height="38" />
                            </a>
                            <a href="https://radarselatan.fajar.co.id/" target="_blank">
                                <img src="./2022/assets/images/radarselatan.png" alt="image" width="70" height="52" />
                            </a>
                            <a href="https://dikita.id/" target="_blank">
                                <img src="./2022/assets/images/dikita.png" alt="image" width="80" height="25" />
                            </a>
                            <a href="https://www.goriau.com/home.html" target="_blank">
                                <img src="./2022/assets/images/goriau.png" alt="image" width="100" height="33" />
                            </a>
                            <a href="https://www.kapol.id" target="_blank">
                                <img src="./2022/assets/images/kapol.png" alt="image" width="110" height="21" />
                            </a>
                            <a href="https://madurapost.net/" target="_blank">
                                <img src="./2022/assets/images/madurapost.png" alt="image" width="120" height="37" />
                            </a>
                            <a href="https://batamnews.co.id/" target="_blank">
                                <img src="./2022/assets/images/batamnews.png" alt="image" width="120" height="37" />
                            </a>
                            <a href="https://matamaros.com/" target="_blank">
                                <img src="./2022/assets/images/matamaros.png" alt="image" width="100" height="37" />
                            </a>
                            <a href="https://maduraindepth.com/" target="_blank">
                                <img src="./2022/assets/images/maduraindepth.png" alt="image" width="120" height="23" />
                            </a>
                            <a href="https://katinting.com/" target="_blank">
                                <img src="./2022/assets/images/katinting.png" alt="image" width="120" height="38" />
                            </a>
                            <a href="https://kabarmedan.com/" target="_blank">
                                <img src="./2022/assets/images/kabarmedan.png" alt="image" width="120" height="25" />
                            </a>
                            <a href="https://kabargupas.com/" target="_blank">
                                <img src="./2022/assets/images/kabargupas.png" alt="image" width="140" height="31" />
                            </a>
                            <a href="https://konde.co/" target="_blank">
                                <img src="./2022/assets/images/konde.png" alt="image" width="120" height="36" />
                            </a>
                            <a href="https://beritaminang.com/" target="_blank">
                                <img src="./2022/assets/images/beritaminang.png" alt="image" width="110" height="37" />
                            </a>
                            <a href="https://kediripedia.com/" target="_blank">
                                <img src="./2022/assets/images/kediripedia.png" alt="image" width="85" height="50" />
                            </a>
                            <a href="https://bantennews.co.id/" target="_blank">
                                <img src="./2022/assets/images/banten-news.jpeg" alt="image" width="150" height="47" />
                            </a>
                            <a href="https://jubi.id/" target="_blank">
                                <img src="./2022/assets/images/jubi.jpeg" alt="image" width="87" height="25" />
                            </a>
                            <a href="https://interes.id/" target="_blank">
                                <img src="./2022/assets/images/interes.png" alt="image" width="42" height="45" />
                            </a>
                            <a href="https://kitamediamuda.com/" target="_blank">
                                <img src="./2022/assets/images/kmm.png" alt="image" width="53" height="50" />
                            </a>
                            <a href="https://trusttv.id/" target="_blank">
                                <img src="./2022/assets/images/trusttv.png" alt="image" width="131" height="50" />
                            </a>
                            <a href="https://intuisi.co/" target="_blank">
                                <img src="./2022/assets/images/intuisi.jpeg" alt="image" width="118" height="40" />
                            </a>
                            <a href="https://detikmanado.com/" target="_blank">
                                <img src="./2022/assets/images/detikmanado.png" alt="image" width="150" height="22" />
                            </a>
                            <a href="https://kilasjambi.com/" target="_blank">
                                <img src="./2022/assets/images/kilasjambi.png" alt="image" width="150" height="26" />
                            </a>
                            <a href="https://ninna.id/" target="_blank">
                                <img src="./2022/assets/images/ninna.jpeg" alt="image" width="100" height="34" />
                            </a>
                            <a href="https://www.roemahkata.com/" target="_blank">
                                <img src="./2022/assets/images/roemahkata.png" alt="image" width="90" height="52" />
                            </a>
                            <a href="https://www.britabrita.com/" target="_blank">
                                <img src="./2022/assets/images/brita.png" alt="image" width="150" height="25" />
                            </a>
                            <a href="https://www.indodaily.co/" target="_blank">
                                <img src="./2022/assets/images/indodaily.png" alt="image" width="150" height="34" />
                            </a>
                            <a href="https://www.acehkita.com/" target="_blank">
                                <img src="./2022/assets/images/acehkita.jpeg" alt="image" width="130" height="30" />
                            </a>
                            <a href="https://www.wowbabel.com/" target="_blank">
                                <img src="./2022/assets/images/wowbabel.png" alt="image" width="100" height="45" />
                            </a>
                            <a href="https://www.lingkarkita.com/" target="_blank">
                                <img src="./2022/assets/images/lingkarkita.png" alt="image" width="120" height="26" />
                            </a>
                            <a href="https://www.jurnalif.com/" target="_blank">
                                <img src="./2022/assets/images/jurnalif.png" alt="image" width="50" height="62" />
                            </a>
                            <a href="https://www.langgam.id/" target="_blank">
                                <img src="./2022/assets/images/langgam.png" alt="image" width="80" height="80" />
                            </a>
                            <a href="https://www.layarkita.com/" target="_blank">
                                <img src="./2022/assets/images/layarkita.jpeg" alt="image" width="100" height="45" />
                            </a>
                            <a href="https://www.kaltimtoday.co/" target="_blank">
                                <img src="./2022/assets/images/kaltimtoday.jpeg" alt="image" width="150" height="33" />
                            </a>
                            <a href="https://www.alagraph.com/" target="_blank">
                                <img src="./2022/assets/images/alagraph.png" alt="image" width="110" height="24" />
                            </a>
                            <a href="https://www.alagraphbontangpost.id/" target="_blank">
                                <img src="./2022/assets/images/bontangpost.png" alt="image" width="120" height="24" />
                            </a>
                            <a href="https://www.sultrakini.com/" target="_blank">
                                <img src="./2022/assets/images/sultrakini.png" alt="image" width="130" height="40" />
                            </a>
                            <a href="https://www.idenera.com/" target="_blank">
                                <img src="./2022/assets/images/idenera.png" alt="image" width="120" height="29" />
                            </a>
                            <a href="https://www.projectmultatuli.org/" target="_blank">
                                <img src="./2022/assets/images/multatuli.png" alt="image" width="120" height="30" />
                            </a>
                            <a href="https://www.starbanjar.com/" target="_blank">
                                <img src="./2022/assets/images/starbanjar.jpeg" alt="image" width="180" height="26" />
                            </a>
                            <a href="https://www.suarakendari.com/" target="_blank">
                                <img src="./2022/assets/images/suarakendari.png" alt="image" width="90" height="43" />
                            </a>
                            <a href="https://www.amirariau.com/" target="_blank">
                                <img src="./2022/assets/images/amira.png" alt="image" width="120" height="32" />
                            </a>
                            <a href="https://bandungbergerak.id/" target="_blank">
                                <img src="./2022/assets/images/bandungbergerak.jpeg" alt="image" width="160" height="42" />
                            </a>
                            <a href="https://www.kolase.id/" target="_blank">
                                <img src="./2022/assets/images/kolase.png" alt="image" width="110" height="47" />
                            </a>
                            <a href="https://www.insidepontianak.com/" target="_blank">
                                <img src="./2022/assets/images/insidepontianak.png" alt="image" width="130" height="31" />
                            </a>
                            <a href="https://www.bentaratimur.id/" target="_blank">
                                <img src="./2022/assets/images/bentaratimur.jpeg" alt="image" width="130" height="35" />
                            </a>
                            <a href="https://www.seputarpapua.com/" target="_blank">
                                <img src="./2022/assets/images/seputarpapua.png" alt="image" width="120" height="25" />
                            </a>
                            <a href="https://www.barta1.com/" target="_blank">
                                <img src="./2022/assets/images/barta1.png" alt="image" width="110" height="47" />
                            </a>
                            <a href="https://www.independen.id/" target="_blank">
                                <img src="./2022/assets/images/independen.png" alt="image" width="120" height="39" />
                            </a>
                            <a href="https://www.suaragayo.com/" target="_blank">
                                <img src="./2022/assets/images/suaragayo.png" alt="image" width="120" height="30" />
                            </a>
                            <a href="https://www.radarsulbar.fajar.co.id/" target="_blank">
                                <img src="./2022/assets/images/radarsulbar.png" alt="image" width="120" height="31" />
                            </a>
                            <a href="https://www.kosakata.org/" target="_blank">
                                <img src="./2022/assets/images/kosakata.png" alt="image" width="100" height="66" />
                            </a>
                            <a href="https://www.bataknature.com/" target="_blank">
                                <img src="./2022/assets/images/bataknature.png" alt="image" width="140" height="25" />
                            </a>
                            <a href="https://www.wartabromo.com/" target="_blank">
                                <img src="./2022/assets/images/wartabromo.png" alt="image" width="150" height="43" />
                            </a>
                            <a href="https://www.volkpop.co/" target="_blank">
                                <img src="./2022/assets/images/volkpop.png" alt="image" width="90" height="61" />
                            </a>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <hr>

        <div id="sponsor" class="container-fluid">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <h1 class="header mt-3">COLLABORATORS</h1>
                        <h3>Local Media Summit 2022</h3>
                        <div class="sponsor">
                            <a href="https://www.norway.no/en/indonesia/" target="_blank">
                                <img src="./2022/assets/images/norway.png" alt="image" width="280" height="62" />
                            </a>
                            <a href="https://www.netherlandsandyou.nl" target="_blank">
                                <img src="./2022/assets/images/netherlands.png" alt="image" width="220" height="92" />
                            </a>
                            <a href="https://www.kurawalfoundation.org/" target="_blank">
                                <img src="./2022/assets/images/kurawal.png" alt="image" width="180" height="60" />
                            </a>
                            <a href="https://bri.co.id/" target="_blank">
                                <img src="./2022/assets/images/bri.png" alt="image" width="91" height="35" />
                            </a>
                            <a href="https://bni.co.id/" target="_blank">
                                <img src="./2022/assets/images/bni.png" alt="image" width="107" height="35" />
                            </a>
                            <a href="https://www.btn.co.id/" target="_blank">
                                <img src="./2022/assets/images/btn.png" alt="image" width="150" height="34" />
                            </a>
                            <a href="https://www.btpn.com/" target="_blank">
                                <img src="./2022/assets/images/btpn1.png" alt="image" width="100" height="96" />
                            </a>
                            <a href="https://aws.amazon.com/" target="_blank">
                                <img src="./2022/assets/images/aws.png" alt="image" width="88" height="52" />
                            </a>
                            <a href="https://vibicloud.com/" target="_blank">
                                <img src="./2022/assets/images/vibicloud.png" alt="image" width="120" height="34" />
                            </a>
                            <a href="https://www.dailymotion.com/id" target="_blank">
                                <img src="./2022/assets/images/dailymotion.png" alt="image" width="150" height="26" />
                            </a>
                            <a href="https://www.notix.co/" target="_blank">
                                <img src="./2022/assets/images/notix.png" alt="image" width="95" height="31" />
                            </a>
                            <!-- <a href="https://pinhome.id/" target="_blank">
                        <img src="./2022/assets/images/pinhome.png" alt="image" width="162" height="45" />
                    </a> -->
                            <a href="https://www.mgid.com" target="_blank">
                                <img src="./2022/assets/images/mgid.png" alt="image" width="80" height="39" />
                            </a>
                            <a href="https://www.danone.co.id/" target="_blank">
                                <img src="./2022/assets/images/danone.png" alt="image" width="80" height="107" />
                            </a>
                            <a href="https://www.telkom.co.id/sites" target="_blank">
                                <img src="./2022/assets/images/telkom1.png" alt="image" width="130" height="71" class="mb-md-3" />
                            </a>
                            <a href="https://www.pertamina.com/" target="_blank">
                                <img src="./2022/assets/images/pertamina.png" alt="image" width="180" height="44" />
                            </a>
                            <a href="https://www.chandra-asri.com/" target="_blank">
                                <img src="./2022/assets/images/chandraasri.png" alt="image" width="177" height="50" />
                            </a>
                            <a href="https://www.adaro.com/" target="_blank">
                                <img src="./2022/assets/images/adaro.png" alt="image" width="120" height="43" class="mb-md-3" />
                            </a>
                            <a href="https://www.djarumfoundation.org/" target="_blank">
                                <img src="./2022/assets/images/djarum.png" alt="image" width="200" height="35" />
                            </a>
                            <a href="https://www.sinarmas.com/" target="_blank">
                                <img src="./2022/assets/images/sinarmas2.png" alt="image" width="150" height="33" />
                            </a>
                            <a href="https://merdekacoppergold.com/" target="_blank">
                                <img src="./2022/assets/images/mcg.png" alt="image" width="130" height="56" class="mb-md-3" />
                            </a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div id="news" class="container-fluid">
            <div class="container">
                <div class="row">
                    <h1 class="text-center">NEWS</h1>
                    <h3>Local Media Summit 2022</h3>

                    <div class="col-sm-4 col-12">
                        <a href="#news" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                            <img src="./2022/assets/images/news.jpg" alt="Artikel" width="336" height="216" />
                            <p class="isi">Local Media Summit 2022: Kolaborasi untuk Pengembangan Bisnis, Teknologi dan Konten </p>
                        </a>
                    </div>

                </div>
            </div>

            <!-- modal news1 -->
            <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content modal-xl">
                        <div class="modal-header modal-xl">
                            <h5 class="modal-title" id="exampleModalLabel">Local Media Summit 2022</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body mx-5">
                            <img src="./2022/assets/images/news.jpg" alt="Artikel" width="336" height="216" class="mb-4" />
                            <h2>Kolaborasi untuk Pengembangan Bisnis, Teknologi dan Konten </h2>
                            <p class="isi-popup">JAKARTA, 21 September 2022 - Media siber lokal di Indonesia menghadapi berbagai tantantangan pada berbagai aspek. Mulai dari kapasitas bisnis, teknologi, ekosistem media, dan peningkatan kualitas konten. Modal mandiri yang
                                terbatas, teknologi yang sederhana, serta model bisnis yang juga terbatas adalah kondisi kondisi yang dihadapi oleh pengelola media lokal di Indonesia.</p>
                            <p class="isi-popup">Singkatnya, tantangan yang dihapi media lokal di Indonesia terdiri dari teknologi, model bisnis, distribusi, monetisasi, investasi, dan konten. ”Untuk menghadapi berbagai aspek tantangan tersebut, kami bekerjasama dengan International
                                Media Support (IMS) menyelenggarakan Local Media Summit 2022,” kata Pemimpin Redaksi Suara.Com, Suwarjono. </p>
                            <p class="isi-popup">Menurutnya, Local Media Summit 2022 yang akan diselenggarakan pada 27-28 Oktober 2022 di Jakarta itu merupakan pertemuan para pengelola media lokal dari lintas sektor, lintas lembaga, organisasi media yang pertama di Indonesia.
                            </p>
                            <p class="isi-popup">”Dalam helatan ini, para pengelola media lokal akan berjumpa dengan media investors, big advertisers, donors, government agencies, platforms. Termasuk berbagai perusahaan yang peduli akan keberlangsungan media yang melayani
                                arus informasi lokal,” katanya.</p>
                            <p class="isi-popup">Peserta Local Media Summit 2022 secara umum terbagi dua kelompok, yakni pengelola media lokal dan stakeholder bisnis media. Setidaknya akan diikuti sekitar 300 pengelola media lokal setingkat CEO dan Pemimpin Redaksi. Dan 100
                                peserta dari kalangan investor media, big advertisers, donor, lembaga pemerintah, swasta, serta platform.</p>
                            <p class="isi-popup">Inisiatif Suara.Com dalam mendukung pengembangan media local diapresiasi oleh International Media Support (IMS) sebuah Lembaga yang berbasis di Denmark. Program Manager IMS Indonesia, Eva Danayanti mengungkapkan Local Media
                                Summit 2022 dapat dikatakan sebagai puncak dari berbagai inisiatif dan kolaborasi Suara.com dan berbagai media local dalam beberapa tahun ini. </p>
                            <p class="isi-popup">“Puncak pertemuan ini menjadi ruang belajar dan kolaborasi untuk mengembangkan kapasitas bisnis media lokal. Sehingga dapat terus mengemas dan mendistribusikan beragam informasi pada berbagai isu, utamanya kepentingan publik
                                atau audiennya,” papar Eva.</p>
                            <p class="isi-popup">Local Media Summit 2022 dikemas untuk menjawab kebutuhan media lokal dan stakeholdernya. Bagi kalangan media lokal dapat membuka peluang dan membangun jejaring dengan media investors, big advertisers, donors, government agencies,
                                serta platforms. Kemudian memperkenalkan berbagai produk dan layanannya pada berbagai kalangan yang lebih luas. Menurut Suwarjono, dalam Local Media Summit 2002 para media lokal juga dapat berjumpa dengan para ahli dalam
                                membahas berbagai isu dan tantangan yang mereka hadapi dalam pengembangan newsroom dan bisnisnya.</p>
                            <p class="isi-popup">“Tak hanya bagi media, Local Media Summit 2022 juga dapat member banyak manfaat bagi kalangan advertisers, donor, lembaga pemerintah, lembaga swasta dan Platforms. Antara lain membangun hubungan dengan berbagai media lokal
                                di seluruh Indonesia. Mendapatkan pemahaman dan karakter dari berbagai media lokal. Dan tentunya dapat memahami fokus isu, produk, layanan serta audien dari berbagai media local,” paparnya.</p>
                            <p class="isi-popup">Agenda utama Local Media Summit 2022 meliputi Knowledge & Experience Sharing, Tech, Platform & Distribution, serta Connecting, Collaboration & Networking. Yang dikemas dalam bentuk konferensi, workshop, coaching clinic, gala
                                dinner, hall of fame, dan networking session.</p>
                            <p class="isi-popup">Pada Local Media Summit 2022 akan terdapat dua konferensi sebagai pembuka dan penutup acara. Pembuka bertajuk, Future of Local Media: Business, Viability, and Audience. Mengeksplorasi lanskap media lokal berdasarkan situasi
                                terakhir, disrupsi teknologi, dan bagaimana masa depannya. Sedangkan konferensi penutup bertajuk, Reinventing Local Media: Finding opportunities and overcoming the challenges. Mengeksplorasi ide-ide model bisnis media lokal
                                dan mencari peluang media sebagai bagian dari usaha kecil dan menengah (UKM).</p>
                            <p class="isi-popup">Sementara itu, untuk topik-topik workshop meliputi; Strategi Pendanaan bagi Startup Media, Mencari Model Bisnis untuk Media Lokal. Mengemas dan monetisasi konten video, dan Keamanan Digital bagi Media Lokal. Lalu Teknologi
                                mendasar dan penting bagi media digital. </p>
                            <p class="isi-popup">Topik workshop lainnya adalah Transformasi Digital pada Media Berbasis Cetak, Optimasi Media Sosial dan SEO untuk distribusi dan monetisasi konten. Kemudian terdapat dua sesi kolaborasi ide antara media lokal dan stakeholdernya.
                                Pertama pengembangan model bisnis berbasis koperasi kolaboratif. Kedua adalah kolaborasi dalam informasi publik untuk audiennya</p>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end modal news 1 -->

        </div>

        <div id="kolab" class="container-fluid">
        <div class="container">

        <div class="d-flex justify-content-center row g-3">
            <div class="col-md-10">
                <h4 class="text-center">NEWSLATTER</h4>
                <h5 class="text-center mb-3">Download materi Local Media Summit 2022</h5>
            </div>
        </div>

            <form action="register_act.php" class="d-flex justify-content-center row g-3 needs-validation" novalidate>

                <div class="col-md-10">
                    <input type="email" class="form-control" id="validationCustom01" placeholder="Masukkan Email Anda" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>


                <!-- <div class="col-10">
                    <div class="form-check mt-5">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                        <label class="form-check-label" for="invalidCheck">
                            Seluruh data yang didaftarkan dapat dipertanggung jawabkan oleh Peserta
                        </label>
                        <label class="form-check-label text-secondary" for="invalidCheck">
                            <small>Arkadia Digital Media tidak akan menyebarluaskan data yang telah diberikan kepada pihak lain dan hanya digunakan selama penyelenggaraan Program Local Media Summit</small>
                        </label>
                        <div class="invalid-feedback">
                            Anda harus setuju sebelum data dikirim.
                        </div>
                    </div>
                </div> -->

                <div class="col-10 text-center daftar">
                    <button class="mb-5 button-kirim" type="submit" onclick="setTimeout(check, 1000);">SUBMIT</button>
                    <!-- <a href="javascript:window.open('','_self').close();">
                    <button class="mb-5 button-batal" type="button">KEMBALI</button>
                    </a> -->
                </div>
            </form>
        </div>
    </div>

        <div id="faq" class="container-fluid">
            <div class="container">
                <div class="row g-4">
                    <h1 class="text-center">FAQ</h1>
                    <div class="col-sm-12 col-12">

                        <div>
                            <details open>
                                <summary>
                                    Siapakah yang boleh mengikuti kegiatan Local Media Summit?
                                </summary>
                                <div>
                                    Kegiatan ini terbuka tidak hanya bagi para pengiat media lokal tapi juga bagi siapa saja yang tertarik dengan isu dan perkembangan media lokal di Indonesia.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Bagaimana cara mengikuti local media summit?
                                </summary>
                                <div>
                                    Silahkan mendaftar melalui menu registrasi sesuai dengan latar belakang Anda. Kami akan mengirimkan pemberitahuan lebih lanjut atas detail yang Anda pilih.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Apakah ada dukungan pembiayaan yang disediakan bagi peserta?
                                </summary>
                                <div>
                                    Kami menyediakan bantuan pembiayaan untuk admission fee dan logistik bagi kalangan media lokal dan admission fee untuk kalangan non media dalam jumlah terbatas. Bagi Anda yang membutuhkan silahkan pilih pada bagian registrasi. Kami akan menginfokan lebih
                                    lanjut apakah permintaan Anda bisa kami akomodir.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Apakah bisa mengikuti seluruh rangkaian kegiatan local media summit?
                                </summary>
                                <div>
                                    Semua peserta berhak mengikuti keseluruhan rangkaian acara. Namun, dikarenakan ada beberapa workshop yang berjalan bersamaan sehingga Anda diminta untuk memilih sesuai dengan prioritas masing-masing.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Apa saja yang akan disediakan untuk peserta Local Media Summit?
                                </summary>
                                <div>
                                    Kesempatan mengikuti seluruh rangkaian kegiatan, seminar kit, makanan, dan minuman selama kegiatan berlangsung.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Bagaimana menentukan pilihan dari dua kategori yang ada dalam menu registrasi?
                                </summary>
                                <div>
                                    Kami membedakan keikutsertaan dalam dua kategori yang bisa Anda pilih, yaitu participants dan collaborators. Participants adalah Anda yang mewakili media atau individu. Sedangkan collaborators adalah Anda yang mewakili lembaga atau institusi yang ingin
                                    memberikan dukungan atas kegiatan ini selain menjadi peserta, seperti dukungan pendanaan, peralatan, dll.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Apakah perbedaan participants media dan individu?
                                </summary>
                                <div>
                                    Participants media adalah Anda para pengelola media lokal atau Anda yang ditunjuk untuk mewakili media lokal Anda. Participants individu adalah Anda yang berasal dari media namun tidak ditunjuk sebagai perwakilan media sehingga hadir mewakili pribadi,
                                    atau Anda yang berasal dari kalangan NGO, akademisi, mahasiswa, serta kalangan umum lainnya yang tertarik dengan isu perkembangan media lokal di Indonesia.
                                </div>
                            </details>

                            <details>
                                <summary>
                                    Mengapa collaborators?
                                </summary>
                                <div>
                                    Kami menyebut dukungan yang diberikan adalah bagian dari kontribusi lembaga atau institusi Anda dalam berkolaborasi dengan media lokal di Indonesia. Kami mengajak segenap kalangan yang memiliki sumber daya untuk ikut berpartisipasi sebagai collaborators
                                    dalam kegiatan ini, mendukung perkembangan dan pengembangan media lokal di Indonesia.
                                </div>
                            </details>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div id="kontak" class="container-fluid">
            <h1 class="header">Contact Us</h1>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h2>For further information, you can reach us via these channels.</h2>
                        <h3>
                            <a href="mailto:localmediasummit@arkadiacorp.com">
                                <img src="./2022/assets/images/mail.png" alt="image" />
                                <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32" height="32" />
                            </a>
                            <div class="br"></div>
                            <div class="wa">
                                <img src="./2022/assets/images/wa.png" alt="image" width="32" height="32" />
                                <a href="https://wa.me/+628129990968">+62 812-9990-968</a>
                                <a href="https://wa.me/+628118258595">+62 811-8258-595</a>
                                <a href="https://wa.me/+628118258596">+62 811-8258-596</a>
                            </div>
                        </h3>
                    </div>

                </div>
            </div>
        </div>


        <div class="copyright">
            <p>© 2022 SUARA.COM - ALL RIGHTS RESERVED.</p>
        </div>
    </div>

    <script>
        (function contoh() {
            'use strict'
            var forms = document.querySelectorAll('.needs-validation')
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)

                    swal({
                title: "Berhasil!",
                text: "Terima Kasih, Materi Akan Kami Kirim ke Email Anda",
                icon: "success",
                button: true
            });

                        setTimeout(function () {
                window.location.href = "index.php";
                }, 3000);

                })
        })()
    </script>

    <script>
        var splide = new Splide('.splide.head', {
            perPage: 1,
            rewind: true,
            autoplay: true,
            breakpoints: {
                991.98: {
                    perPage: 1,
                    gap: "1rem",
                    // destroy: true,
                },
                768: {
                    perPage: 1,
                    gap: "1rem",
                    // destroy: true,
                },
            },
        });

        splide.mount();

        var splide = new Splide('.splide.agenda', {
            perPage: 1,
            rewind: true,
            autoplay: true,
            drag: false,
            breakpoints: {
                2000: {
                    destroy: true,
                },
                991.98: {
                    perPage: 1,
                    gap: "1rem",
                    destroy: true,
                },
                768: {
                    perPage: 1,
                    gap: "1rem",
                    destroy: true,
                },
            },
        });

        splide.mount();

    </script>

    <script>
        $(document).ready(function() {
            $('body').scrollspy({
                target: ".navbar",
                offset: 50
            });
            $("#myNavbar a").on('click', function(event) {
                if (this.hash !== "") {
                    event.preventDefault();
                    var hash = this.hash;
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function() {
                        window.location.hash = hash;
                    });
                }
            });
        });
    </script>

    <!-- <script>
        var countDownDate = new Date("Oct 27, 2022 09:00:00").getTime();

        var x = setInterval(function() {

            var now = new Date().getTime();

            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);


            document.getElementById("count").innerHTML = "<div class=\"days\"> \
  <div class=\"numbers\">" + days + "</div>Days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + hours + "</div>Hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + minutes + "</div>Minutes</div> \
</div>";

            if (distance < 0) {
                clearInterval(x);
                document.getElementById("count").innerHTML = "<div class=\"days\"> \
  <div class=\"numbers\">" + "00" + "</div>Days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + "00" + "</div>Hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + "00" + "</div>Minutes</div> \
</div>";
            }
        }, 1000);
    </script> -->

    <!-- tooltip -->
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <!-- end tooltip -->


    <!-- pause video -->
    <script>
        var video = document.getElementById("background-video");
        var btn = document.getElementById("myBtn");

        function myFunction() {
            if (video.paused) {
                video.play();
                // btn.innerHTML = "Pause";
            } else {
                video.pause();
                // btn.innerHTML = "Play";
            }
        }
    </script>

    <!-- end pause video -->

    <script>
        $(document).ready(function() {
            jQuery('img').each(function() {
                jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
            });
        });
    </script>

</body>

</html>