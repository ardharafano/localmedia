<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Form Participant Personal - Jatim Media Summit 2023</title>

    <!-- Fonts -->
    <link
        href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;1,200;1,300;1,400;1,500;1,600&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/flowbite@1.4.7/dist/flowbite.min.css" />

    <!-- Styles -->
    <style>
    * {
        font-family: 'Nunito', sans-serif !important;
    }

    body {
        font-family: 'Nunito', sans-serif;
        min-height: 100vh;
        background: #fff;
        display: flex;
        justify-content: center;
    }

    .main-logo {
        display: flex;
        justify-content: center;
    }

    .form-container {
        width: 70%;
        margin: 0;
        border-radius: 10px;
    }

    .form-description {
        font-size: 16px;
        font-weight: 300;
    }

    .form-title {
        font-weight: 700;
        font-size: 20px;
    }

    .form-wrap {
        padding: 5%;
    }

    #login-info p {
        font-size: 14px;
        font-weight: 300;
    }

    #container-login {
        display: flex;
        align-items: center;
        justify-content: center;
        min-height: 100vh;
    }

    #container-login .login-info {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }

    #container-login .login-info p {
        font-size: 14px;
        font-weight: 300;
    }

    .form-wrap h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        width: 100% !important;
    }

    @media only screen and (max-width: 768px) {
        .form-wrap {
            width: 100%;
            padding: 5%;
        }
    }

    @media only screen and (max-width: 500px) {
        .form-wrap {
            width: 100%;
            padding: 5%;
        }

        .form-container {
            width: 100%;
            margin: 1%;
        }
    }
    </style>
</head>

<body class="antialiased" style="background-color: #ffffff !important;">
    <div class="justify-center form-container">
        <div class="main-logo">
            <img width="200" height="200" alt="Form Participant Personal - Jatim Media Summit 2023"
                src="https://form.arkadia.me/forms/adf7825f-b1c2-4052-9035-a6dd0c9723a4.png" />
        </div>
        <div class="form-wrap" id="form">
            <div class="mb-5">
                <h2 class="form-title">Form Participant Personal - Jatim Media Summit 2023</h2>
                <p class="form-description"></p>
                <div id="login-info">

                </div>
            </div>
            <div>
                <form action="https://form.arkadia.me/105eb1e8-d29e-4e4e-8be7-7e6f331b8b2e"
                    enctype="multipart/form-data" method="post" id="form-input" name="form">
                    <input type="hidden" name="_token" value="Nd7M3GLpimjoOIkXieHEL8FAR3PLgJkkJYJyMkoe">
                    <div>
                        <div class="mb-6">
                            <label for="f709ed39818e4a2d9de0a281ca69c1b1"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nama Lengkap
                                <span class="text-red-500">*</span></label>
                            <input type="text" id="f709ed39818e4a2d9de0a281ca69c1b1"
                                name="f709ed39818e4a2d9de0a281ca69c1b1" value=""
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>

                            <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400"></p>
                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <label for="b5206b18c3424dc99cd8107853b78a47"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Organisasi/
                                Perusahaan <span class="text-red-500">*</span></label>
                            <input type="text" id="b5206b18c3424dc99cd8107853b78a47"
                                name="b5206b18c3424dc99cd8107853b78a47" value=""
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>

                            <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400"></p>
                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <label for="9fbcb26f81394c7bae8a6a988add57a0"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Pekerjaan/
                                Jabatan <span class="text-red-500">*</span></label>
                            <input type="text" id="9fbcb26f81394c7bae8a6a988add57a0"
                                name="9fbcb26f81394c7bae8a6a988add57a0" value=""
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>

                            <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400"></p>
                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <label for="454793a5369e406fa829bf12d787fd1b"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Usia <span
                                    class="text-red-500">*</span></label>
                            <input type="text" id="454793a5369e406fa829bf12d787fd1b"
                                name="454793a5369e406fa829bf12d787fd1b" value=""
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>

                            <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400"></p>
                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <label for="8c88bc34db1c488280172d7fd019fc35"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Email <span
                                    class="text-red-500">*</span></label>
                            <input type="email" id="8c88bc34db1c488280172d7fd019fc35"
                                name="8c88bc34db1c488280172d7fd019fc35" value=""
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>

                            <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400"></p>
                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <label for="dc6a417f00044e5e8de9bcbcb9135908"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">No. Telepon/
                                WhatsApp <span class="text-red-500">*</span></label>
                            <input type="text" id="dc6a417f00044e5e8de9bcbcb9135908"
                                name="dc6a417f00044e5e8de9bcbcb9135908" value=""
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="" required>

                            <p id="helper-text-explanation" class="mt-2 text-sm text-gray-500 dark:text-gray-400"></p>
                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <div>
                                <h3 style="text-align: center;"><strong><span style="font-size: 18pt;">Untuk mengikuti
                                            semua rangkaian acara Jatim Media Summit 2023</span></strong></h3>
                                <h3 style="text-align: center;"><strong><span style="font-size: 18pt;">Peserta dikenakan
                                            biaya Admission Rp 300.000</span></strong></h3>
                            </div>

                        </div>
                    </div>

                    <div>
                        <div class="mb-6">
                            <input
                                class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
                                id="7acfab84998e4e5d82ed52c528190832" name="7acfab84998e4e5d82ed52c528190832[]" multiple
                                type="file">

                            <label for="7acfab84998e4e5d82ed52c528190832"
                                class="block mt-2 mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Upload bukti
                                transfer <span class="text-red-500">*</span></label>

                            <div style="margin:20px 0">
                                <h4>Pembayaran dapat ditransfer melalui :</h4>
                                <span>- Bank BNI 0364935906 PT. Beritajatim Cyber Media</span><br>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="mb-6">
                            <div class="flex items-start mb-6">
                                <div class="flex items-center h-5">
                                    <input id="checkbox-578b27813b604429b85d0512971ea609"
                                        name="578b27813b604429b85d0512971ea609" placeholder="" required type="checkbox"
                                        value="true"
                                        class="w-4 h-4 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800">
                                </div>
                                <div class="ml-2">
                                    <label for="checkbox-578b27813b604429b85d0512971ea609"
                                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Seluruh
                                        data yang didaftarkan dapat dipertanggung jawabkan oleh Peserta</label>
                                    <label class="text-sm font-medium text-gray-500 dark:text-gray-300"></label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div>
                        <div class="mb-6">
                            <div>
                                <p>Arkadia Digital Media tidak akan menyebarluaskan data yang telah diberikan kepada
                                    pihak lain dan hanya digunakan selama penyelenggaraan Program Local Media Summit</p>
                            </div>

                        </div>
                    </div>
                    <button type="submit" style="background-color: #f9441b !important;"
                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Kirim</button>
                </form>
            </div>
        </div>
    </div>

    <div id="container-login" style="display: none">
        <div class="login-info">
            <p>Kamu harus login untuk mengisi form ini.</p>
            <button
                class="block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                type="button" onclick="handleModal()">
                Login
            </button>
        </div>
    </div>

    <!-- Main modal -->
    <div id="loginModal" tabindex="-1" aria-hidden="true"
        class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-modal md:h-full">
        <div class="relative p-4 w-full max-w-lg h-full md:h-auto">
            <!-- Modal content -->
            <div class="relative bg-white shadow dark:bg-gray-700">
                <!-- Modal header -->
                <div class="absolute right-0 top-0">
                    <button type="button"
                        class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                        onclick="handleModal()">
                        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="space-y-6">
                    <div id="login">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="https://unpkg.com/flowbite@1.4.7/dist/flowbite.js"></script>

    <script defer
        src="https://static.cloudflareinsights.com/beacon.min.js/v52afc6f149f6479b8c77fa569edb01181681764108816"
        integrity="sha512-jGCTpDpBAYDGNYR5ztKt4BQPGef1P0giN6ZGVUi835kFF88FOmmn8jBQWNgrNd8g/Yu421NdgWhwQoaOPFflDw=="
        data-cf-beacon='{"rayId":"7cb1e6a22d2791a7","version":"2023.4.0","r":1,"token":"2d23201592f44e07b54a6610e6eb7d4a","si":100}'
        crossorigin="anonymous"></script>
</body>

</html>