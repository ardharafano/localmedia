<div id="acara">
    <div class="container">
        <h1>ACARA</h1>
        <ul class="nav nav-pills justify-content-center mb-3 gap-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="pills-conference-tab" data-bs-toggle="pill"
                    data-bs-target="#pills-conference" type="button" role="tab" aria-controls="pills-conference"
                    aria-selected="true">Conference</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-talkshow-tab" data-bs-toggle="pill" data-bs-target="#pills-talkshow"
                    type="button" role="tab" aria-controls="pills-talkshow" aria-selected="false">Talkshow</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-workshop-tab" data-bs-toggle="pill" data-bs-target="#pills-workshop"
                    type="button" role="tab" aria-controls="pills-workshop" aria-selected="false">Workshop / Coaching
                    Clinic</button>
            </li>
            <!-- <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-coaching-tab" data-bs-toggle="pill" data-bs-target="#pills-coaching"
                    type="button" role="tab" aria-controls="pills-coaching" aria-selected="false">Coaching
                    Clinic</button>
            </li> -->
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-networking"
                    type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Networking
                    Night</button>
            </li>
        </ul>

        <div class="bg-header">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-conference" role="tabpanel"
                    aria-labelledby="pills-conference-tab">
                    <div>
                        <h2>CONFERENCE I</h2>
                        <div class="bg-img">
                            <img src="assets/images/acara/conference-1.png" alt="image" />
                            <img src="assets/images/acara/conference-2.png" alt="image" />
                        </div>
                        <p><b>“Peluang dan Tantangan Bisnis Media di Jawa Timur: Kini dan Masa Depan”
                                (Manajemen - Bisnis - Konten - Teknologi)</b>
                        </p>
                        <p>Sebagai pembuka rangkaian JMS 2023, sesi ini akan mengetengahkan paparan dan diskusi mengenai
                            apa
                            dan bagaimana bisnis media di Jawa Timur, baik terkait tantangan maupun peluangnya di masa
                            depan, mulai dari sisi pengelolaan, konten, hingga teknologi dan bisnis.
                        </p>
                        <p>Pembicara:
                            Yos Kusuma (Google Indonesia)
                            Francisco Tinangon (Group M - Advertising Agency)
                            Yuswohady (Inventure)
                        </p>
                    </div>

                    <div>
                        <h2>CONFERENCE II</h2>
                        <div class="bg-img">
                            <img src="assets/images/acara/conference-3.png" alt="image" />
                            <img src="assets/images/acara/conference-4.png" alt="image" />
                        </div>
                        <p><b>“Membangun Ekosistem Media Digital di Jawa Timur”</b>
                        </p>
                        <p>Sesi ini akan menjadi sesi paparan dan diskusi penutup yang pada intinya akan mengarah pada
                            harapan sekaligus keinginan bersama untuk membangun ekosistem media digital Jawa Timur Plus
                            yang
                            lebih baik dan lebih maju ke depannya, lengkap dengan masukan dan usulan rekomendasi yang
                            bermanfaat.

                        </p>
                        <p>Pembicara:
                            Arief Rahman (Ketua AMSI Jatim)
                            Sherlita Agustin (Kadis Kominfo Jatim)
                            Mohammad Nasih (Rektor Unair)
                            Arya Dwi Paramita (Pertamina / Kalangan Industri)

                        </p>
                    </div>
                </div>


                <div class="tab-pane fade" id="pills-talkshow" role="tabpanel" aria-labelledby="pills-talkshow-tab">
                    <div>
                        <div class="bg-img">
                            <img src="assets/images/acara/talkshow.png" alt="image" />
                        </div>
                        <ol>
                            <li>Pendanaan untuk Startup Media Lokal</li>
                            <li>Technology for Local Media</li>
                        </ol>
                    </div>
                </div>

                <div class="tab-pane fade" id="pills-workshop" role="tabpanel" aria-labelledby="pills-workshop-tab">
                    <div>
                        <div class="bg-img">
                            <img src="assets/images/acara/workshop.png" alt="image" />
                        </div>
                        <ul>
                            <li>Optimasi Sosial Media dan Distribusi Konten untuk Media Lokal</li>
                            <li>Menggali Digital Revenue dari Ad Native Platform</li>
                            <li>Creative Video Production for Social Media</li>
                            <li>Boost Traffic dengan SEO untuk Media Lokal</li>
                            <li>Business Model Canvas for Local Media</li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="tab-pane fade" id="pills-coaching" role="tabpanel" aria-labelledby="pills-coaching-tab">...
                </div> -->
                <div class="tab-pane fade" id="pills-networking" role="tabpanel" aria-labelledby="pills-networking-tab">
                    <div>
                        <div class="bg-img">
                            <img src="assets/images/acara/networking-1.png" alt="image" />
                            <img src="assets/images/acara/networking-2.png" alt="image" />
                        </div>
                        <ul>
                            <li>Jamuan makan malam dan sesi ramah-tamah yang dihadiri para pengelola media lokal bersama
                                pemangku kepentingan media, seperti perusahaan, agensi iklan, lembaga donor, lembaga
                                pemerintah, platform internet, dan perwakilan lembaga solusi teknologi.
                            </li>
                            <li>Dalam sesi acara semi-formal ini, rekan-rekan pengelola media lokal juga dapat saling
                                bercerita atau diberi kesempatan menyampaikan keunggulan media yang dimilikinya,
                                tantangan, hingga concern-nya terhadap keberlangsungan media lokal, dsb.
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>