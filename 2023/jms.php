<!DOCTYPE html>
<html lang="en">

<head>
    <title>Local Media Summit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  -->
    <meta name="description" content="Jatim Media Summit">
    <meta name="keywords" content="Suara, Local Media Summit, LMS">

    <meta property="og:title" content="locammediasummit.com" />
    <meta property="og:description"
        content="Jatim Media Summit - Emerging Local Media Business Viability to Represent the Unrepresented" />
    <meta property="og:url" content="https://microsite.suara.com/localmediasummit" />
    <meta property="og:image" content="assets/images/jms_thumb.jpg" />

    <link rel="Shortcut icon" href="../2022/assets/images/favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css">
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
    <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

</head>

<body>

    <div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="index.php#home"><img src="../2022/assets/images/icon-home.svg"
                        alt="image" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#targeting">TARGETING</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#acara">ACARA</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#workshop">WORKSHOP</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#network">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#kontak">CONTACT</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <!-- <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0"> -->
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/"
                                    target="_blank"><img src="../2022/assets/images/ig.svg" alt="image" width="25"
                                        height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img
                                        src="../2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img
                                        src="../2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    REGISTER
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a>
                                    </li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a>
                                    </li>
                                </ul>
                            </li> -->

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/"
                                    target="_blank"><img src="../2022/assets/images/ig.svg" alt="image" width="25"
                                        height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img
                                        src="../2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img
                                        src="../2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    REGISTER
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a>
                                    </li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a>
                                    </li>
                                </ul>
                            </li> -->

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

    <div id="jms">
        <div class="container">
            <div class="row">

                <section class="splide head" aria-label="Splide Basic HTML Example">
                    <div class="splide__track">
                        <ul class="splide__list">

                            <li class="splide__slide">
                                <div class="container">
                                    <div class="row">
                                        <div class="media">
                                            <img src="assets/images/lms-jatim.svg" alt="image" class="hg" width="446"
                                                height="210" />
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </section>

                <div style="z-index:999999">
                    <h4><b>Kolaborasi</b> Media Lokal Jatim Menembus Batas untuk Naik Kelas</h4>
                    <div class="date">Surabaya, 24-25  Mei 2023</div>
                    <div class="dropdown">
                        <div class="daftar  dropdown-toggle" type="button" data-bs-toggle="dropdown">
                            DAFTAR
                        </div>
                        <ul class="dropdown-menu">
                            <!-- <li><a class="dropdown-item" target="_blank"
                                    href="https://form.arkadia.me/105eb1e8-d29e-4e4e-8be7-7e6f331b8b2e">JMS Personal</a>
                            </li> -->
                            <li><a class="dropdown-item" target="_blank" href="register_personal.php">JMS Personal</a>
                            </li>
                            <li><a class="dropdown-item" target="_blank"
                                    href="https://form.arkadia.me/9e442390-ac86-4ada-a771-ca4bb6bb214e">JMS Media</a>
                            </li>
                            <li><a class="dropdown-item" target="_blank" href="register_collaborators.php">JMS
                                    Collaborator</a>
                            </li>
                        </ul>
                    </div>
                    <h3>INITIATORS</h3>
                    <img src="assets/images/jms_new.svg" alt="image" width="369" height="43" class="d-block m-auto" />
                </div>

                <div class="col-lg-12">
                    <video id="background-video" autoplay loop muted poster="assets/images/foto/6.jpeg">
                        <source src="assets/video/h4.mp4" type="video/mp4">
                    </video>

                    <div class="overlay">
                    </div>

                </div>

            </div>
        </div>
    </div>


    <div id="about">
        <div class="container">
            <div class="row">
                <h1>ABOUT</h1>
                <h2>Jatim Media Summit 2023</h2>
                <p>Local Media Summit 2023 ini merupakan pertemuan para pengelola media lokal kedua dan terbesar di
                    Indonesia. Media lokal di sini adalah media yang memiliki lingkup di sebuah wilayah geografis
                    tertentu atau menyasar segmen masyarakat tertentu.
                </p>
                <p>Dalam acara ini, para pengelola media lokal akan berjumpa dengan perusahaan, agensi iklan, lembaga
                    donor, lembaga pemerintah, platforms Internet, dan solusi teknologi. Ini adalah kesempatan media
                    lokal memperkenalkan berbagai produk dan
                    layanannya pada berbagai kalangan yang lebih luas. Kemudian para mereka juga dapat berjumpa dengan
                    para pakar dalam memecahkan berbagai masalah dan tantangan yang mereka hadapi dalam pengembangan
                    newsroom dan bisnisnya.</p>
                <p>Sementara investor media dan Lembaga donor bisa menemukan partner yang tepat di forum ini. Pengiklan,
                    lembaga pemerintah, platform, atau penyedia solusi teknologi bisa berbicara dengan media lokal yang
                    mereka ingin temui agar bisa lebih
                    memahami fokus isu, layanan, atau siapa audience mereka.</p>
                <p>Kegiatan ini juga akan menghasilkan catatan dan rekomendasi yang dapat digunakan oleh media lokal
                    maupun stakeholder nya untuk mendorong pengembangan bisnis media lokal di Indonesia.</p>
            </div>
        </div>
    </div>

    <div id="targeting">
        <div class="container">
            <h1>TARGETING</h1>
            <div class="bg">
                <h2>Objektif</h2>
                <ol>
                    <li>Mendampingi dan memfasilitasi para pengelola media lokal untuk berkembang melalui pertemuan
                        dan
                        pelatihan yang tepat.</li>
                    <li>Membuat kegiatan untuk peningkatan kapasitas bisnis, teknologi, konten dan distribusi untuk
                        setiap media lokal yang ada di Jatim.</li>
                    <li>Menjadi jembatan bagi para pengelola media lokal dengan para pemangku kepentingan media,
                        seperti
                        perusahaan, agensi iklan, lembaga donor, lembaga pemerintah, platform internet, dan solusi
                        teknologi.</li>
                </ol>
            </div>
            <div class="bg">
                <h2>Goals</h2>
                <ol>
                    <li>Menjadikan Jawa Timur sebagai poros baru media digital di Indonesia setelah Jakarta. </li>
                    <li>Membangun jejaring media berbasis di Jawa Timur Plus (Jawa Tengah, Bali dan Nusa Tenggara).
                    </li>
                    <li>Peningkatan kapasitas media berbasis di Jawa Timur Plus dalam hal bisnis, teknologi, konten,
                        distribusi, dan marketing.</li>
                    <li>Peningkatan pemahaman pemangku kepentingan media terhadap seluk-beluk bisnis media dan
                        pengembangan media lokal.</li>
                    <li>Memberikan rekomendasi kepada Pemerintah Provinsi Jawa Timur dalam mendukung kolaborasi
                        media berbasis di Jawa Timur untuk naik kelas.</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- acara -->
    <?php include('acara.php'); ?>
    <!-- end acara -->

    <!-- agenda -->
    <?php include('workshop.php'); ?>
    <!-- end agenda -->


    <!-- news -->
    <?php include('news.php'); ?>
    <!-- end news -->

    <div id="speaker">
        <div class="container">
            <div class="row">
                <!-- <a href="speaker.php"> -->
                <h1 class="header mb-3">SPEAKERS</h1>
                <h3>Jatim Media Summit 2023</h3>
                <!-- </a> -->
                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/khofifah.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Khofifah Indar Parawansa</div>
                        <div class="jabatan">Gubernur Jawa Timur</div>
                    </div>
                </div>

                <!-- <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/nasih.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Mohammad Nasih</div>
                        <div class="jabatan">Rektor Universitas Airlangga</div>
                    </div>
                </div> -->

                <!-- <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/jonathan_alan.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Jonathan Alan*</div>
                        <div class="jabatan">Konsul Jenderal AS <br>di Surabaya</div>
                    </div>
                </div> -->

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/ika.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Ika Francisca</div>
                        <div class="jabatan">Social Behavior Change Lead USAID IUWASH Tangguh</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/sapto_anggoro.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Sapto Anggoro</div>
                        <div class="jabatan">Anggota Dewan Pers </div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/eva_danayanti.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Eva Danayanti</div>
                        <div class="jabatan">Country Manager International Media Support</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/amir_suherlan.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Amir Suherlan</div>
                        <div class="jabatan">Managing Director Wavemaker</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/suwarjono.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Suwarjono</div>
                        <div class="jabatan">CEO Suara.com</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/yuswohady.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Yuswohady</div>
                        <div class="jabatan">Managing Partner Inventure</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/aliefah_new.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Aliefah Permata Fikri</div>
                        <div class="jabatan">Senior Success Manager MGID Indonesia</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/ajar_edi.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Ajar Edi</div>
                        <div class="jabatan">Director Corporate Affairs Microsoft Indonesia</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/eko_lokonoto.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Dwi Eko Lokononto</div>
                        <div class="jabatan">CEO Beritajatim.com</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/dwi_setyawan.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Dwi Setyawan</div>
                        <div class="jabatan"
                            style="width:105%; margin:auto;max-width:250px;position:relative;left:-5px">Dekan FTMM
                            <br>Universitas
                            Airlangga
                        </div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/iramdani.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Ira Ramdani</div>
                        <div class="jabatan">Produser Arkadia Production</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/heru.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Heru Tjatur</div>
                        <div class="jabatan">Advisor ICT Watch</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/arief_rahman.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Arief Rahman</div>
                        <div class="jabatan">AMSI Jatim</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/dimas.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Dimas Sagita</div>
                        <div class="jabatan">Head of Growth Suara.com</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/arya_dwi.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Arya Dwi Paramita</div>
                        <div class="jabatan">Corsec PT Pertamina Hulu Energi</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/subagja.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Subagja Hamara</div>
                        <div class="jabatan">CEO Harapan Rakyat</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/sherlita_agustin.png" alt="image" width="125"
                                height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Sherlita Agustin</div>
                        <div class="jabatan">Diskominfo Jatim</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/asep.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Asep Saefullah</div>
                        <div class="jabatan">Project Coordinator Suara.com</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/then_triyanto_new.png" alt="image" width="125"
                                height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Then Triyanto</div>
                        <div class="jabatan">Product Manager Huawei Cloud Indonesia</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/ari_suprayogi.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Ari Suprayogi</div>
                        <div class="jabatan">CEO Warta Bromo</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/ellexis_gurrola.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Ellexis Gurrola</div>
                        <div class="jabatan">Deputy Director <br>of DRG Office USAID Indonesia</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/putri_kenasti.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Kenasti Arninta Putri</div>
                        <div class="jabatan">Digital Creator</div>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="assets/images/speaker/triatma_monica.png" alt="image" width="125" height="125" />
                        </div>
                        <div class="nama mt-3 mb-2">Triatma Monica Sari</div>
                        <div class="jabatan">MC</div>
                    </div>
                </div>

            </div>
        </div>

    </div>


    <div id="network">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="header mt-3">INITIATORS</h1>
                    <div class="media">
                        <img src="assets/images/network/logo_initiators.svg" alt="image" width="751" height="54"
                            class="w-100 h-auto object-cover" style="max-width:600px;height:auto;margin:10px auto" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <hr>

    <div id="collaborators">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="header">COLLABORATORS</h1>
                    <div class="collaborators">
                        <!-- <a href="https://www.norway.no/en/indonesia/" target="_blank"> -->
                        <img src="assets/images/network/logo_collaborators_new.svg" alt="image" width="738" height="307"
                            class="w-100 h-auto object-cover" style="max-width:738px;height:auto;margin:10px auto" />

                        <!-- </a> -->
                    </div>

                </div>

            </div>
        </div>
    </div>

    <hr>

    <div id="network">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="header">MEDIA NETWORK</h1>
                    <div class="media mb-3">
                        <img src="assets/images/network/network_new.svg" alt="image" width="723" height="176"
                            class="w-100 h-auto object-cover" style="max-width:900px;height:auto;margin:10px auto" />
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div id="newslatter">
        <div class="container">

            <div class="d-flex justify-content-center row g-3">
                <div class="col-md-10">
                    <!-- <h4 class="text-center">NEWSLATTER</h4> -->
                    <h4 class="mb-3">Download materi Local Media Summit 2022</h4>
                </div>
            </div>

            <form action="register_act.php" class="d-flex justify-content-center row g-3 needs-validation" novalidate>

                <div class="col-md-10">
                    <input type="email" class="form-control" id="validationCustom01" placeholder="Masukkan Email Anda"
                        autocomplete="off" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>



                <div class="col-10 text-center daftar">
                    <button class="mb-5 button-kirim" type="submit" onclick="setTimeout(check, 1000);">KIRIM</button>
                </div>
            </form>
        </div>
    </div>

    <div id="faq">
        <div class="container">
            <div class="row g-4">
                <h1 class="text-center">FAQ</h1>
                <div class="col-sm-12 col-12">

                    <div>
                        <details open>
                            <summary>
                                Siapakah yang boleh mengikuti kegiatan Local Media Summit?
                            </summary>
                            <div>
                                Kegiatan ini terbuka tidak hanya bagi para pengiat media lokal tapi juga bagi siapa saja
                                yang tertarik dengan isu dan perkembangan media lokal di Indonesia.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Bagaimana cara mengikuti local media summit?
                            </summary>
                            <div>
                                Silahkan mendaftar melalui menu registrasi sesuai dengan latar belakang Anda. Kami akan
                                mengirimkan pemberitahuan lebih lanjut atas detail yang Anda pilih.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apakah ada dukungan pembiayaan yang disediakan bagi peserta?
                            </summary>
                            <div>
                                Kami menyediakan bantuan pembiayaan untuk admission fee dan logistik bagi kalangan media
                                lokal dan admission fee untuk kalangan non media dalam jumlah terbatas. Bagi Anda yang
                                membutuhkan silahkan pilih pada bagian registrasi. Kami akan menginfokan lebih
                                lanjut apakah permintaan Anda bisa kami akomodir.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apakah bisa mengikuti seluruh rangkaian kegiatan local media summit?
                            </summary>
                            <div>
                                Semua peserta berhak mengikuti keseluruhan rangkaian acara. Namun, dikarenakan ada
                                beberapa workshop yang berjalan bersamaan sehingga Anda diminta untuk memilih sesuai
                                dengan prioritas masing-masing.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apa saja yang akan disediakan untuk peserta Local Media Summit?
                            </summary>
                            <div>
                                Kesempatan mengikuti seluruh rangkaian kegiatan, seminar kit, makanan, dan minuman
                                selama kegiatan berlangsung.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Bagaimana menentukan pilihan dari dua kategori yang ada dalam menu registrasi?
                            </summary>
                            <div>
                                Kami membedakan keikutsertaan dalam dua kategori yang bisa Anda pilih, yaitu
                                participants dan collaborators. Participants adalah Anda yang mewakili media atau
                                individu. Sedangkan collaborators adalah Anda yang mewakili lembaga atau institusi yang
                                ingin
                                memberikan dukungan atas kegiatan ini selain menjadi peserta, seperti dukungan
                                pendanaan, peralatan, dll.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apakah perbedaan participants media dan individu?
                            </summary>
                            <div>
                                Participants media adalah Anda para pengelola media lokal atau Anda yang ditunjuk untuk
                                mewakili media lokal Anda. Participants individu adalah Anda yang berasal dari media
                                namun tidak ditunjuk sebagai perwakilan media sehingga hadir mewakili pribadi,
                                atau Anda yang berasal dari kalangan NGO, akademisi, mahasiswa, serta kalangan umum
                                lainnya yang tertarik dengan isu perkembangan media lokal di Indonesia.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Mengapa collaborators?
                            </summary>
                            <div>
                                Kami menyebut dukungan yang diberikan adalah bagian dari kontribusi lembaga atau
                                institusi Anda dalam berkolaborasi dengan media lokal di Indonesia. Kami mengajak
                                segenap kalangan yang memiliki sumber daya untuk ikut berpartisipasi sebagai
                                collaborators
                                dalam kegiatan ini, mendukung perkembangan dan pengembangan media lokal di Indonesia.
                            </div>
                        </details>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="kontak">
        <h1 class="header">Contact Us</h1>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h2>For further information, you can reach us via these channels.</h2>
                    <h3>
                        <a href="mailto:localmediasummit@arkadiacorp.com">
                            <img src="../2022/assets/images/mail.png" alt="image" />
                            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32"
                                height="32" />
                        </a>
                        <div class="br"></div>
                        <div class="wa">
                            <img src="../2022/assets/images/wa.png" alt="image" width="32" height="32" />
                            <a href="https://wa.me/+628119156234">0811-9156-234</a>
                        </div>
                    </h3>
                </div>

            </div>
        </div>
    </div>

    <div id="wa">
        <a href="https://wa.me/+628119156234" class=" fly-wa-bottom" target="_blank">
            <img src="assets/images/wa.svg" alt="img" />
        </a>
    </div>


    <div class="copyright">
        <p>© 2023 SUARA.COM - ALL RIGHTS RESERVED.</p>
    </div>

    <script>
    var splide = new Splide('.splide.head', {
        perPage: 1,
        rewind: true,
        autoplay: true,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                // destroy: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                // destroy: true,
            },
        },
    });

    splide.mount();
    </script>

    <script>
    $(document).ready(function() {
        $('body').scrollspy({
            target: ".navbar",
            offset: 50
        });
        $("#myNavbar a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function() {
                    window.location.hash = hash;
                });
            }
        });
    });
    </script>

    <!-- <script>
        var countDownDate = new Date("Oct 27, 2022 09:00:00").getTime();

        var x = setInterval(function() {

            var now = new Date().getTime();

            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);


            document.getElementById("count").innerHTML = "<div class=\"days\"> \
  <div class=\"numbers\">" + days + "</div>Days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + hours + "</div>Hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + minutes + "</div>Minutes</div> \
</div>";

            if (distance < 0) {
                clearInterval(x);
                document.getElementById("count").innerHTML = "<div class=\"days\"> \
  <div class=\"numbers\">" + "00" + "</div>Days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + "00" + "</div>Hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + "00" + "</div>Minutes</div> \
</div>";
            }
        }, 1000);
    </script> -->

    <!-- tooltip -->
    <script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
    <!-- end tooltip -->


    <!-- pause video -->
    <!-- <script>
        var video = document.getElementById("background-video");
        var btn = document.getElementById("myBtn");

        function myFunction() {
            if (video.paused) {
                video.play();
            } else {
                video.pause();
            }
        }
    </script> -->

    <!-- end pause video -->

    <!-- <script>
    $(document).ready(function() {
        jQuery('img').each(function() {
            jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
        });
    });
    </script> -->

    <script>
    function check() {
        swal({
            title: "Gagal!",
            text: "Data Masih Belum Lengkap",
            icon: "error",
            button: true
        });
    }
    </script>


    <script>
    (function() {
        'use strict'
        var forms = document.querySelectorAll('.needs-validation')
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()
    </script>

    <script>
    $(document).ready(function() {
        $(".content").slice(0, 8).show();
        $("#loadMore").on("click", function(e) {
            e.preventDefault();
            $(".content:hidden").slice(0, 4).slideDown();
            if ($(".content:hidden").length == 0) {
                $("#loadMore").text("Tidak ada konten lagi").addClass("noContent");
            }
        });

    })
    </script>

    <script>
    $(document).ready(function() {
        $(".content-foto").slice(0, 4).show();
        $("#loadMorefoto").on("click", function(e) {
            e.preventDefault();
            $(".content-foto:hidden").slice(0, 4).slideDown();
            if ($(".content-foto:hidden").length == 0) {
                $("#loadMorefoto").text("Tidak ada foto lagi").addClass("noContentfoto");
            }
        });

    })
    </script>

</body>

</html>