<div id="news">
    <div class="container">
        <div class="row">
            <h1 class="text-center mb-3">NEWS</h1>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal12">
                    <img src="assets/images/news/news12.jpeg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Fitur dan Fungsi Google Analytics 4</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal11">
                    <img src="assets/images/news/news11.jpeg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Batamnews Peduli Bagi-bagi Ratusan Nasi Kotak Gratis Setiap Jumat</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal10">
                    <img src="assets/images/news/news10.jpeg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Tantangan Dan Potensi Bisnis Media Lokal Kekinian</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal9">
                    <img src="assets/images/news/news9.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">BandungBergerak Membangun Kolaborasi dengan PembacaPada akhir 2022</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal8">
                    <img src="assets/images/news/news8.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Sampai jumpa di KTT Media Lokal 2023!</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal7">
                    <img src="assets/images/news/news7.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Acara Pamungkas Local Media Summit 2022: Bicara Peluang dan Tantangan Media Lokal
                    </p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal6">
                    <img src="assets/images/news/news6.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Koperasi Media Sebagai Solusi Alternatif Kolaborasi Konten Kreator dengan Media
                        di Masa Depan</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal5">
                    <img src="assets/images/news/news5.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Media Lokal Wajib Miliki Kepercayaan Diri dari Publik</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal4">
                    <img src="assets/images/news/news4.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Media Lokal di LMS 2022 Diajak Dapatkan Cuan Menggiurkan Melalui Monetisasi Video
                    </p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal3">
                    <img src="assets/images/news/news3.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Media Lokal Dituntut Mencari Model Bisnis yang Layak Sesuai dengan Platformnya
                    </p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                    <img src="assets/images/news/news2.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">IMS Sebut Tiga Hal Ini Jadi Tantangan Media Lokal di Indonesia</p>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6 col-6 content">
                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal1">
                    <img src="./2022/assets/images/news.jpg" alt="Artikel" width="281" height="176" class="home" />
                    <p class="isi">Local Media Summit 2022: Kolaborasi untuk Pengembangan Bisnis, Teknologi dan
                        Konten </p>
                </a>
            </div>

            <a href="#" id="loadMore">Lebih Banyak</a>


        </div>
    </div>

    <!-- modal news 1 -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel1">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="./2022/assets/images/news.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Kolaborasi untuk Pengembangan Bisnis, Teknologi dan Konten </h2>
                    <p class="isi-popup">JAKARTA, 21 September 2022 - Media siber lokal di Indonesia menghadapi
                        berbagai tantantangan pada berbagai aspek. Mulai dari kapasitas bisnis, teknologi, ekosistem
                        media, dan peningkatan kualitas konten. Modal mandiri yang terbatas,
                        teknologi yang sederhana, serta model bisnis yang juga terbatas adalah kondisi kondisi yang
                        dihadapi oleh pengelola media lokal di Indonesia.</p>
                    <p class="isi-popup">Singkatnya, tantangan yang dihapi media lokal di Indonesia terdiri dari
                        teknologi, model bisnis, distribusi, monetisasi, investasi, dan konten. ”Untuk menghadapi
                        berbagai aspek tantangan tersebut, kami bekerjasama dengan International
                        Media Support (IMS) menyelenggarakan Local Media Summit 2022,” kata Pemimpin Redaksi
                        Suara.Com, Suwarjono. </p>
                    <p class="isi-popup">Menurutnya, Local Media Summit 2022 yang akan diselenggarakan pada 27-28
                        Oktober 2022 di Jakarta itu merupakan pertemuan para pengelola media lokal dari lintas
                        sektor, lintas lembaga, organisasi media yang pertama di Indonesia.
                    </p>
                    <p class="isi-popup">”Dalam helatan ini, para pengelola media lokal akan berjumpa dengan media
                        investors, big advertisers, donors, government agencies, platforms. Termasuk berbagai
                        perusahaan yang peduli akan keberlangsungan media yang melayani arus
                        informasi lokal,” katanya.</p>
                    <p class="isi-popup">Peserta Local Media Summit 2022 secara umum terbagi dua kelompok, yakni
                        pengelola media lokal dan stakeholder bisnis media. Setidaknya akan diikuti sekitar 300
                        pengelola media lokal setingkat CEO dan Pemimpin Redaksi. Dan 100 peserta
                        dari kalangan investor media, big advertisers, donor, lembaga pemerintah, swasta, serta
                        platform.</p>
                    <p class="isi-popup">Inisiatif Suara.Com dalam mendukung pengembangan media local diapresiasi
                        oleh International Media Support (IMS) sebuah Lembaga yang berbasis di Denmark. Program
                        Manager IMS Indonesia, Eva Danayanti mengungkapkan Local Media Summit
                        2022 dapat dikatakan sebagai puncak dari berbagai inisiatif dan kolaborasi Suara.com dan
                        berbagai media local dalam beberapa tahun ini. </p>
                    <p class="isi-popup">“Puncak pertemuan ini menjadi ruang belajar dan kolaborasi untuk
                        mengembangkan kapasitas bisnis media lokal. Sehingga dapat terus mengemas dan
                        mendistribusikan beragam informasi pada berbagai isu, utamanya kepentingan publik atau
                        audiennya,” papar Eva.</p>
                    <p class="isi-popup">Local Media Summit 2022 dikemas untuk menjawab kebutuhan media lokal dan
                        stakeholdernya. Bagi kalangan media lokal dapat membuka peluang dan membangun jejaring
                        dengan media investors, big advertisers, donors, government agencies,
                        serta platforms. Kemudian memperkenalkan berbagai produk dan layanannya pada berbagai
                        kalangan yang lebih luas. Menurut Suwarjono, dalam Local Media Summit 2002 para media lokal
                        juga dapat berjumpa dengan para ahli dalam membahas
                        berbagai isu dan tantangan yang mereka hadapi dalam pengembangan newsroom dan bisnisnya.</p>
                    <p class="isi-popup">“Tak hanya bagi media, Local Media Summit 2022 juga dapat member banyak
                        manfaat bagi kalangan advertisers, donor, lembaga pemerintah, lembaga swasta dan Platforms.
                        Antara lain membangun hubungan dengan berbagai media lokal di seluruh
                        Indonesia. Mendapatkan pemahaman dan karakter dari berbagai media lokal. Dan tentunya dapat
                        memahami fokus isu, produk, layanan serta audien dari berbagai media local,” paparnya.</p>
                    <p class="isi-popup">Agenda utama Local Media Summit 2022 meliputi Knowledge & Experience
                        Sharing, Tech, Platform & Distribution, serta Connecting, Collaboration & Networking. Yang
                        dikemas dalam bentuk konferensi, workshop, coaching clinic, gala dinner,
                        hall of fame, dan networking session.</p>
                    <p class="isi-popup">Pada Local Media Summit 2022 akan terdapat dua konferensi sebagai pembuka
                        dan penutup acara. Pembuka bertajuk, Future of Local Media: Business, Viability, and
                        Audience. Mengeksplorasi lanskap media lokal berdasarkan situasi terakhir,
                        disrupsi teknologi, dan bagaimana masa depannya. Sedangkan konferensi penutup bertajuk,
                        Reinventing Local Media: Finding opportunities and overcoming the challenges. Mengeksplorasi
                        ide-ide model bisnis media lokal dan mencari
                        peluang media sebagai bagian dari usaha kecil dan menengah (UKM).</p>
                    <p class="isi-popup">Sementara itu, untuk topik-topik workshop meliputi; Strategi Pendanaan bagi
                        Startup Media, Mencari Model Bisnis untuk Media Lokal. Mengemas dan monetisasi konten video,
                        dan Keamanan Digital bagi Media Lokal. Lalu Teknologi mendasar
                        dan penting bagi media digital. </p>
                    <p class="isi-popup">Topik workshop lainnya adalah Transformasi Digital pada Media Berbasis
                        Cetak, Optimasi Media Sosial dan SEO untuk distribusi dan monetisasi konten. Kemudian
                        terdapat dua sesi kolaborasi ide antara media lokal dan stakeholdernya.
                        Pertama pengembangan model bisnis berbasis koperasi kolaboratif. Kedua adalah kolaborasi
                        dalam informasi publik untuk audiennya</p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 1 -->

    <!-- modal news2 -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel2">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news2.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">IMS Sebut Tiga Hal Ini Jadi Tantangan Media Lokal di Indonesia </h2>
                    <p class="isi-popup">Ada tiga tantangan besar yang dihadapi media lokal di Indonesia. Ketiga
                        tantangan tersebut yakni keuangan, missinformasi dan disinformasi.</p>
                    <p class="isi-popup">Dalam kesempatan itu, Lars Bestle memaparkan perkembangan global di media
                        dibandingkan dengan luar negeri.</p>
                    <p class="isi-popup">Ia memaparkan keuangan menjadi tantangan untuk lantaran adanya krisis
                        keuangan yang mengancam keselamatan jurnalis.
                    </p>
                    <p class="isi-popup">Selain itu, masalah disinformasi dan missinformasi merupakan hal yang harus
                        ditanggulangi IMS.</p>
                    <p class="isi-popup">Kata Lars Bestle, Local Media Summit ini ternyata sudah dilakukan beberapa
                        tahun lalu.</p>
                    <p class="isi-popup">Ia mengaku sangat senang bisa bertemu dengan media lokal di indonesia, di
                        sini ada perwakilan dari berbasis dari Jakarta.</p>
                    <p class="isi-popup">"Saya terimakasih kepada Suara.com yang menjadi ujung tombak dalam acara
                        ini dan perpus, dan pemerintahan Norwegia yang telah mensuport kita," katanya, Kamis
                        (27/10/2022).</p>
                    <p class="isi-popup">Menurutnya, kelanjutan media lokal sangat penting. Ia menyebut IMS yang
                        berbasis di Denmark mendukung perkembangan media di berbagai negara.</p>
                    <p class="isi-popup">"IMS mendukung jurnalis berkualitas, kita juga mendukung mengenai
                        keselamatan jurnalis, kami bekerjasama dengan aliansi, untuk keselamatan jurnalis,"
                        ungkapnya di hadapan ratusan peserta Local Media Summit 2022.</p>
                    <p class="isi-popup">"Di Asia dan 7 negara lain kita kerjasama yang menghubungkan berbagai
                        organisasi keselamatan jurnalis dan aliansi media, dan kita di Indonesia ini ada fokus
                        terhadap keselamatan Jurnalis," paparnya.</p>
                    <p class="isi-popup">Lebih lanjut, ia menyebut keberlanjutan media merupakan hal yang penting.
                        Terkait keberlangsungan finansial, ia menyebut hal tersebut mempengaruhi peran dalam
                        demokrasi, namun kini media dihadapkan dengan krisis global yang mengancam
                        jurnalis di dunia.</p>
                    <p class="isi-popup">Sementara itu, Saptini Darmaningrum yang merupakan perwakilan
                        Beritajatim.com dalam kesempatan itu berbagi kisah medianya yang hingga belasan tahun mampu
                        bertahan.</p>
                    <p class="isi-popup">"Di Jatim kami mungkin dianggap besar karena sudah 16 tahun. Tetapi dengan
                        susah payah kami menjadi besar, kami berusaha menggaji tidak telat, kami mulai dari modal
                        Rp250 juta dengan 26 karyawan. Modal yang kami siapkan kami peruntukan
                        untuk kantor, dan segala peralatan penunjang kerja," katanya.</p>
                    <p class="isi-popup">Ia juga menyinggung Pandemi Covid-19 yang menjadi kendala bagi banyak media
                        malah menjadi tantangan tersendiri bagi BeritaJatim.com. Ia bahkan menyebut saat pandemi
                        dari 40 pegawai, Berita Jatim menambah pekerja menjadi 60 orang.</p>
                    <p class="isi-popu[">Sumber :<a
                            href="https://suara.com/news/2022/10/27/151938/ims-sebut-tiga-hal-ini-jadi-tantangan-media-lokal-di-indonesia"
                            target="_blank"> Suara.com</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 2 -->

    <!-- modal news 3 -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel3">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news3.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Media Lokal Dituntut Mencari Model Bisnis yang Layak Sesuai dengan
                        Platformnya</h2>
                    <p class="isi-popup">Media lokal dituntut bisa memahami kebutuhan pasar serta memproduksi berita
                        yang bisa sesuai dengan platform medianya. Sehingga media perlu mencari model bisnis yang
                        sesuai dalam proses melakukan transformasi dalam dunia teknologi
                        digital.
                    </p>
                    <p class="isi-popup">Sebagai platform pionir native advertising, MGID membahas hal tersebut
                        dalam salah satu workshop Local Media Summit 2022 dalam sesi Finding a Viable Business Model
                        for Local Media di Gedung Perpustakaan Nasional, Jakarta, Kamis
                        (27/10/2022). Untuk diketahui, MGID membantu penerbit agar bisa memonetisasi konten, lalu
                        pada saat yang sama memelihara pemirsa dengan mendorong performa dan brand awareness. Senior
                        Account Manager MGID Indonesia Aliefah Permata
                        Fikri mengemukakan, perlunya pendekatan berbasis data. Melihat kondisi saat ini, hal yang
                        paling penting, yakni saling menolong dan saling berkolaborasi antara media lokal dan media
                        nasional.</p>
                    <p class="isi-popup">"Sebagai platform beriklanan native, kami ingin memberikan dukungan kepada
                        media lokal karena media media adalah partner (MGID)," katanya. Dia menuturkan, MGID
                        mengutamakan transparansi 100 persen sehingga kontrol penuh untuk peletakan
                        iklan dan konten juga dilakukan.</p>
                    <p class="isi-popup">RPMS terkemuka di industri yang digerakkan oleh pengiklan merek lokal dan
                        global juga dirangkul. Tujuannya, untuk meningkatkan pengalaman pengguna.</p>
                    <p class="isi-popup">“Format iklan yang dapat disesuaikan untuk menghubungkan minat pemirsa yang
                        sebenarnya. (MGID) ketentuan pembayaran fleksibel,” sebutnya. Dia melanjutkan, untuk
                        monetisasi dan retensi, pencocokkan algoritme berbasis Artificial
                        Intelligence (Al) perlu dilakukan. Rasio pengisian 100 persen dengan permintaan terprogram
                        serta terarah, baik dalam native, display, maupun video.</p>
                    <p class="isi-popup">Dia menjelaskan, smart widget merupakan solusi dalam satu hal yang mampu
                        memberikan penerbit kuasa penuh atas pengalaman pengguna. Seperti monetisasi, retensi
                        pengguna, dan pengunjung baru.</p>
                    <p class="isi-popup">“Didukung oleh mesin rekomendasi MGID yang sedang dalam proses paten. Smart
                        widget membuat kombinasi dari berbagai macam format secara otomatis untuk mencapai misi
                        penerbit,” tuturnya. Lebih lanjut, ia juga menyampaikan, peningkatan
                        kinerja terasa signifikan seperti, peningkatan CTR hingga 40 persen dibandingkan penempatan
                        native tradisional. Selain itu juga, meningkatkan on-site engagement, pengalaman pengguna
                        dan menghasilkan CPM 20 persen lebih tinggi
                        dibandingkan dengan iklan native klasik. Kemudian, under-article widget, dia menyampaikan
                        unit iklan ini berfungsi untuk responsif dan bisa menyesuaikan. Hal ini menjadi sisi klasik
                        dalam periklanan native.</p>
                    <p class="isi-popup">“Tata letak native yang dapat disesuaikan dengan mudah. Kompatibel dengan
                        mesin situs, ukuran layer atau peramban apapun, serta dapat dikombonasikan dengan bursa
                        internal,” lanjutnya. Dia juga menuturkan ada header widget dan sidebar
                        widget. Seperti header widget, unit iklan tersebut sangat nampak dan memberikan engagement
                        pemirsa secara maksimal. Sedangkan, sidebar widget wajib dimiliki untuk mendapatkan aliran
                        penghasilan tambahan di desktop.</p>
                    <p class="isi-popup">Untuk hal yang pas sebagai bagian besar server iklan dia membeberkan ada
                        IAB banners. Yakni, unit iklan yang membawa kinerja native ke baner display.</p>
                    <p class="isi-popup">"Cocok untuk sebagian besar server (iklan), kompatibel dengan Google Ad
                        Manager, sesuai dengan standar IAB display," katanya. Untuk bisa memaksimalkan CPM, terdapat
                        video recommendation player. Hal tersebut mampu meningkatkan waktu
                        yang dihabiskan pemirsa di situs setidaknya 15 persen.</p>
                    <p class="isi-popup">Peningkatan hasil rata-rata situs juga terjadi. Dengan iklan video MGID,
                        mampu mendongkrak CPM 30-100 persen.</p>
                    <p class="isi-popup">"MGID menyediakan pustaka yang berisi lebih dari 10.000 video dari banyak
                        brand unggulan di dunia," katanya. Untuk diketahui, Local Media Summit 2022 merupakan agenda
                        yang dilakukan mempertemukan media lokal untuk pengembangan
                        bisnis yang kali pertama dilakukan di Indonesia dan berlangsung selama dua hari, mulai
                        Kamis-Jumat (27-28/10/2022).</p>
                    <p class="isi-popup">Sumber : <a
                            href="https://suara.com/news/2022/10/27/170352/media-lokal-dituntut-mencari-model-bisnis-yang-layak-sesuai-dengan-platformnya?page=3"
                            target="_blank">Suara.com</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 3 -->

    <!-- modal news 4-->
    <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel4" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel4">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news4.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Media Lokal di LMS 2022 Diajak Dapatkan Cuan Menggiurkan Melalui
                        Monetisasi Video</h2>
                    <p class="isi-popup">Visibilitas menjadi poin penting dalam meraup cuan melalui video. Sehingga
                        video bisa terlihat oleh pembaca untuk menarik perhatian pengiklan agar mau memasang iklan
                        di media. Hal tersebut diungkapkan Indonesia Country Director
                        DailyMotion Derian Laks dalam sesi Video packaging and monetization dalam agenda Local Media
                        Summit (LMS) 2022 di Gedung Perpustakaan Nasional pada Kamis (27/10/2022) Derian
                        mengungkapkan, visibiltas tinggi tergantung pada
                        penempatan video di dalam satu domain. Langkah ini yang kemudian perlu dilakukan bagi
                        media-media lokal. "Video yang sudah diproduksi itu bagaimana caranya terlihat di domain,"
                        kata dia.</p>
                    <p class="isi-popup">Menurutnya, visibiltas sangat penting bagi pengiklan karena dari situlah
                        ukuran mereka dalam memasang iklan di sebuah konten video. Salah satu cara yang dilakukan
                        DailyMotion dalam meningkatkan visibilitas video adalah dengan menempatkan
                        video di tengah artikel berita. Ini dilakukan DailyMotion bersama media Suara.com.</p>
                    <p class="isi-popup">"Setelah ini dilakukan adalah hasil perubahan signifikan. Pertama view
                        naik, revenue naik," ucap dia.</p>
                    <p class="isi-popup">Selain Derian, Konten Manajer Tribun Bengkulu Prawira Maulana berbagi kisah
                        sukses dalam bermain di konten video yang tayang di salah satu platform media sosial.
                        Diakuinya, jika mengacu pada syarat monetisasi sangat berat. "Monetisasi
                        Facebook luar biasa beratnya. 600 ribu menit tayangan dalam 60 hari dibanding Youtube 4.000
                        jam per 12 bulan. Facebook sangat berat. Banyak yang menyerah sebelum mencoba," ujarnya.</p>
                    <p class="isi-popup">Namun jika terus konsisten memproduksi konten akan bisa meraup cuan ratusan
                        juta. "Teori dasarnya buatlah konten yang disuka pengguna dan disukai platform. Kenapa
                        disukai platorm? Karena bisa ditempel iklan," ujar Wira. Wira mencontohkan,
                        ketika membangun konten video, Tribun Bengkulu di Februari 2022 masih mengejar monetisasi
                        namun belum layak.</p>
                    <p class="isi-popup">"Tapi September kita di angka bulanan 70 juta sebulan. Kok bisa? Percayalah
                        mereka mendistribusikannya. Saya percaya platform ini mendistribusikannya," ujar dia.
                        Menurut Wira, platform juga berperan mendistribusikan konten. Karena
                        itu, menurutnya semua dimulai dari konsistensi membuat konten lalu menjadi kualitas baru
                        platform akan mendistribusikan konten yang dibuat publisher. "Semangat saja tidak cukup.
                        Perlu pengetahuan taktikal dan teknikal. Secara
                        taktikal semua sudah oke lah A sampai Z tapi kita lupa hal-hal teknikal. Misal kita tidak
                        mengerti fitur-fitur metrik dari platform," ujar Wira. Untuk diketahui, Local Media Summit
                        (LMS) 2022 merupakan even media lokal kali
                        pertama dan terbesar di Indonesia yang diikuti ratusan media lokal dari seluruh kawasan
                        mulai Aceh hingga Papua. Agenda ini digelar Suara.com bekerja sama dengan International
                        Media Support (IMS) selama dau hari, mulai Kamis-Jumat
                        (27-28/10/2022).
                    </p>
                    <p class="isi-popup">Sumber : <a
                            href="https://suara.com/news/2022/10/28/101703/media-lokal-di-lms-2022-diajak-dapatkan-cuan-menggiurkan-melalui-monetisasi-video?page=all"
                            target="_blank">Suara.com</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 4 -->

    <!-- modal news 5-->
    <div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel5" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel5">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news5.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Media Lokal Wajib Miliki Kepercayaan Diri dari Publik</h2>
                    <p class="isi-popup">Media di tingkat lokal harus memahami kearifan lokal (local wisdom) untuk
                        mendapatkan kepercayaan dari publik. Pernyataan tersebut disampaikan Pemred Suara Surabaya
                        Edy Prasetyo dalam diskusi Hyperlocal Strategy for Local Media
                        dalam agenda Local Media Summit 2022, Jumat (28/10/2022). Edy Prastyo juga menyatakan
                        pentingnya kedekatan tersebut dengan audiens. "Membantu audience atau pendengar itu sangat
                        penting. Suara Surabaya berangkat dari radio di
                        tingkat lokal. Kami mempunyai tagline, news, interaktif, solutif. Artinya di Suara Surabaya
                        kami membangun kedekatan dengan masyarakat lokal dan terus mencari inovasi dan kreatifitas,"
                        ujarnya. Lebih lanjut, ia mengemukakan
                        media lokal juga harus memperhatikan local wisdom yang ada diubah menjadi kekuatan.</p>
                    <p class="isi-popup">Pada titik tersebut, media lokal harus memiliki penyelesaian masalah di
                        tingkatan lokal. Masih menurut Edy, menjadi penting untuk media lokal menumbuhkan benih baik
                        di local wisdom. “Ada empat poin penting yang harus dilihat media
                        lokal, pertama soal local wisdom, melihat budaya lokal (di Surabaya, soal Kampung Arek dan
                        Santri), ketiga harus dilihat seperti apa kekerabatan sosial yang rekat dan terkahir
                        mengenai stakholders yang peduli dan bergerak,”
                        jelas Edy.</p>
                    <p class="isi-popup">Empat poin penting yang dijalankan media lokal itu akan membangun trust
                        atau kepercayaan di publik lokal. Trust ini yang menjadi penting untuk media lokal bisa
                        bertahan di tengah kondisi saat ini.</p>
                    <p class="isi-popup">“Media di tingkatan lokal juga harus punya leadership yang kuat untuk
                        membuat stakeholder bisa bergerak. Trust harus mampu dijaga oleh media lokal. Menjaga trust
                        itu ialah bisa menghasilkan produk jurnalistik, artinya kode jurnalistik
                        harus dipegang,” jelasnya. Pada tingkatan konten yang diproduksi, hal yang juga wajib
                        menjadi perhatian media lokal ialah konten tersebut harus komunikatif, menarik dan powerfull
                        secara impact di tingkatan masyarakat dan pengambil
                        kebijakan.
                    </p>
                    <p class="isi-popup">“Caranya bagaimana kepemimpinan media lebih aktif dalam pemecahan masalah
                        lokal. Berpikir menyelesaikan masalah di tingkat lokal, mengajak seluruh stakeholder untuk
                        sama-sama mencari solusi dari masalah yang ada,” Sementara itu,
                        Pemred Batamnews Muhammad Zuhri mengemukakan, untuk membangun media lokal, yang paling utama
                        ialah mempunyai visi dan misi. Dari visi dan misi ini pemimpin media bisa membangun mimpi,
                        apa yang mau dicapai. “Visi dan misi ini
                        yang menjalankan semuanya. Harus dituangkan ke karyawan yang ada di media lokal. Disampaikan
                        di ruang kerja redaksi, ruang marketing ke tataran paling bawah seperti cleaning service,”
                        jelas Zuhri.</p>
                    <p class="isi-popup">Tidak kalah penting untuk membangun media lokal juga harus tertib
                        administrasi. Keuangan harus jelas dalam pengeluaran dan pemasukan. Batamnews hadir karena
                        melihat habit pembaca yang lebih kekinian, konten lokal dan netizen journalism.
                        Batamnews saat ini branding ke pembaca lebih muda dengan mengembangkan E-Sports dan memiliki
                        tim bernama BEST (Batamnews Esports). E-sports dibangun Batamnews untuk branding kepada
                        pembaca. Selain itu, Batamnews juga mengembangkan
                        sosial media kekinian. Sosial media yang dibangun dan dijaga dengan baik oleh media lokal
                        juga bisa menjadi salah satu pemasukan tersendiri. Media lokal juga harus memperhatikan
                        bagaiamana mencari investor yang mengetahui dan
                        mengerti bagaimana bisnis media itu berjalan. Mendatangkan investor juga jadi persoalan
                        untuk media lokal, caranya ialah membangun kepercayaan. “Di Batam, tidak semua orang yang
                        punya uang itu tertarik pada media. Yang pertama
                        harus kita lakukan adalah membuat branding di media kita. Produk harus bagus, artinya konten
                        yang dihasilkan harus berkualitas,” jelas Zuhri. Untuk diketahui, Local Media Summit (LMS)
                        2022 digelar Suara.com bersama dengan International
                        Media Support (IMS) di Perpustakaan Nasional (Perpusnas) Jakarta pada 27-28 Oktober 2022.
                    </p>
                    <p class="isi-popup">Sumber : <a
                            href="https://suara.com/news/2022/10/28/155650/media-lokal-wajib-miliki-kepercayaan-diri-dari-publik?page=all"
                            target="_blank">Suara.com</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 5 -->

    <!-- modal news 6-->
    <div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel6" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel6">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news6.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Koperasi Media Sebagai Solusi Alternatif Kolaborasi Konten Kreator dengan
                        Media di Masa Depan</h2>
                    <p class="isi-popup">Persoalan banyaknya konten yang dibuat kreator tidak semua bisa
                        terdistribusi dengan baik. Lantaran itu, maka dibutuhkan kolaborasi antara media dengan
                        konten kreator untuk bersama-sama bisa menghasilkan bagi konten kreator. Hal
                        tersebut yang kemudian diangkat dalam sesi Koperasi Media: Kolaborasi Media dan Konten
                        Kreator pada Local Media Summit (LMS) 2022 yang digelar di Perpusatakaan Nasional
                        (Perpusnas) Jakarta pada Jumat (28/10/2022). Founder dan
                        CEO Indotnesia Ahmad Nasir menyampaikan, pembuat konten diibaratkan dengan buruh tani yang
                        tidak memiliki lahan. Dalam pemaparannya bertajuk Koperasi (Petani) Konten Digital
                        Indonesia, Nasir membeberkan konsep terkait Koperasi
                        Multi Pihak (KMP) yang bisa menjadi solusi alternatif konten kreator dalam
                        mendistribusikannya karyanya. "Industri media digital sangat pesat berkembang dan salah satu
                        kebutuhan yang berkembang audio visual. Sebagian media
                        online memproduksi sendiri audio visual. Sebagian lain menggunakan platform penyedia
                        konten," katanya.</p>
                    <p class="isi-popup">"Pembuat konten di Indonesia itu tumbuh pesat. Namun hanya sebagian kecil
                        yang mampu menghasilkan karyanya menjadi pendapatan yang layak," katanya. Ia juga
                        menjelaskan terkait gap antara demand dan supply dalam pelaksanaan koperasi
                        media yang dijalankan di Indotnesia. Kata dia, Konten audio visual yang disediakan oleh
                        platform umumnya berupa talent dan suasana luar negeri (warga kulit putih, suasana Eropa
                        atau Amerika.</p>
                    <p class="isi-popup">Pun ia mengemukakan, sebagian pembuat konten tidak mau atau bisa
                        menggunakan platform penyedia konten untuk menjual karya. Lebih lanjut, gagasan yang ia buat
                        selama ini yakni dengan mencari telent dengan karakter lokal, mengelola
                        platform dengan prinsip-prinsip koopratif.</p>
                    <p class="isi-popup">"Perlu juga membat prosentase pendapatan dan anggaran. untuk supply konten
                        dari videografer, jurnalis, film maker, konten kreator, dan talent. Kita juga mendapatkan
                        konten dari media, humas, admin media sosial, lembaga riset atau
                        pendidikan," katanya. Sementara itu, Community Development Manager Yoursay.id Randy Sadikin
                        memaparkan, platform Yoursay yang menjadi user generated content (UGC) sebagai ruang konten
                        kreator mengkreasikan karyanya.</p>
                    <p class="isi-popup">"Bukan cuma pembaca, mereka bisa mengkritik dan memberi masukan bagi kita
                        juga agar tidak ada barier antara media dan pengguna," kata Rendy. Rendy mengungkapkan
                        beberapa alasan Suara.com membuat UGC atau Yoursay.id. Beberapa diantaranya
                        yakni, membangun loyal audience, membangun engagmet komunitas serta pengembangan trafict dan
                        revenue. "Yoursay ada 80.000 member, ada sekitar 4.500 konten. 37 persen laki-laki, 63
                        persen perempuan dengan usia 17-30 tahun,"
                        beber Rendy.</p>
                    <p class="isi-popup">"Mereka bersal dari yogyakarta 35 persen, jateng 20, jakarta dan beberapa
                        daerah lainnya," imbuhnya. Dengan dibuatnya yoursay.id, Rendy menyebut berbagai even telah
                        dilakukannya serta menghasilkan belasan kolaborasi. "Kita membuat
                        23 even, 14 kolaborasi partner, 1.200 lebih partisipasi, 1.500 lebih komunitas," ungkapnya.
                        Lebih lanjut, Rendy juga menyebutkan sejumlah tantangan yang dilaluinya selama membangun
                        Yoursay.id. Menurutnya, tantangan yoursay
                        yakni bagaimana caranya semua member berpartisipasi. "Karenanya treatment kepada member
                        Yoursay jadi hal yang sangat penting kita jaga. Dengan berusaha memberikan feedback, setiap
                        artikel yang diriject diberikan alasan, dan
                        beberapa hal lainnya. Meski begitu, dalam konteks koperasi media, ruang UGC seperti Yoursay
                        menurut Nasir bisa saja menjadi wadah koperasi bagi konten kreator dan penghubung dengan
                        pihak yang membutuhkan konten, baik tulisan,
                        video hingga visual foto maupun infografis untuk mendapatkan pemasukan bagi pembuat konten.
                    </p>
                    <p class="isi-popup">Untuk diketahui, Local Media Summit 2022 digelar oleh Suara.com bekerja
                        sama dengan International Media Support (IMS) yang dihadiri ratusan media berbasis lokal
                        dari berbagai wilayah di Indonesia.</p>
                    <p class="isi-popup">Sumber : <a
                            href="https://suara.com/news/2022/10/28/163000/koperasi-media-sebagai-solusi-alternatif-kolaborasi-konten-kreator-dengan-media-di-masa-depan?page=all"
                            target="_blank">Suara.com</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 6 -->

    <!-- modal news 7-->
    <div class="modal fade" id="exampleModal7" tabindex="-1" aria-labelledby="exampleModalLabel7" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel7">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news7.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Acara Pamungkas Local Media Summit 2022: Bicara Peluang dan Tantangan
                        Media Lokal</h2>
                    <p class="isi-popup">Di hari terakhir pelaksanaan Local Media Summit (LMS) 2022 pada Jumat
                        (28/10/2022) di sesi penutupan, digelar diskusi bertema “Reinventing Local Media: Finding
                        Opportunities and overcoming the Challenges”. Diskusi ini diisi empat
                        narasumber yaitu Pemimpin Redaksi Radar Selatan Sunarti Sain, Anggota Dewan Pers Sapto
                        Anggoro, Corporate Communications Director of Danone Arief Mujahidin dan Managing Partner
                        Inventure Yuswohady. Pimred Radar Selatan Sunarti
                        berbicara mengenai perjuangannya membangun media Radar Selatan di Bulukumba, Sulawesi
                        Selatan. Waktu didirikan tahun 2008, masih bernama Radar Bulukumba dengan format cetak.
                        Ketika itu, menurut Sunarti, kondisi koran Radar
                        Bulukumba hampir mati karena banyak persoalan. Di tahun pertama, Sunarti membenahi internal
                        terlebih dahulu. Setelah semua beres, baru di tahun kedua, Sunarti melakukan pengembangan
                        bisnis.</p>
                    <p class="isi-popup">“Radar Selatan lahir dengan semangat lokalitas. Lokalitas itu kekuatan
                        bukan kekurangan. Kita jangan merasa kurang, minder, tidak sebanding karena kita lokal.
                        Padahal itu kekuatan kita. Media nasional tidak bisa seperti kita,”
                        ujarnya. Dalam membangun Radar Selatan, Sunarti tidak hanya mengandalkan dari media cetak.
                        Produk layanan di Radar Selatan masih mempertahankan cetak ada media online, e-paper, event,
                        kelas jurnalistik, penerbitan buku dan
                        podcast.
                    </p>
                    <p class="isi-popup">“Kami membukukan profil anggota DPRD. jadi sumber baru pemasukan. Kelas
                        jurnalistik juga menghasilkan,” ujar dia.</p>
                    <p class="isi-popup">Sementara itu Anggota Dewan Pers Sapto Anggoro lebih menitikberatkan pada
                        bisnis media yang beralih ke digital. Menurutnya, dengan digitalisasi maka yang terjadi
                        media online sama dengan media televisi. “Kita kelasnya sama. Jangan
                        berpikir lokal sama nasional beda, nggak. Cara mendapatkan informasi semua sama. Lokal itu
                        sama. Justru anda di daerah anda harus menunjukkan,” kata dia.</p>
                    <p class="isi-popup">"Sapto mengatakan, pada akhirnya semua media massa akan menjadi online.
                        Bahkan menurutnya, media online bisa menjadi sebuah unicorn. “Di jepang ada Smartnews jadi
                        unicorn. (penghasilannya) 1,1 miliar dolar. dan itu investornya
                        banyak sekali. Yang dilakukan fokus sofware development. artinya teknologi, teknologi,
                        teknologi,” papar Sapto. Menurut Sapto model bisnis online tidak lagi seperti model bisnis
                        konvensional yang pembiayaanya dari kerja sama
                        dengan pemda dan instansi, sponsorship, kepanjangan tangan dari funding, LSM, bagian dari PR
                        dan EO, developing komunitas, dll.</p>
                    <p class="isi-popup">“Sementara online memiliki standar advertising ada direct, agency, bill
                        comm, programatic, block chain , konten premium, google news show case, platform medsos,
                        community engagement, community insight dan apps game,” ujar dia.
                        Di akhir presentasinya, Sapto mengingatkan bahwa peran pers selain sebagai media informasi,
                        pendidikan dan hiburan juga berperan sebagai kontrol sosial. “Jangan sampai terjual
                        idealisnya. Karena kita sudah sebagai pilar keempat
                        demokrasi. Pers yang kuat dan independen secara finansial semestinya menjalankan amanah UU
                        Pers,” tutupnya. Sementara itu Corporate Communications Director of Danone Arief Mujahidin,
                        mengatakan, model kerja sama antara perusahaan
                        dengan media massa kini beragam. Menurutnya dalam menjalin sebuah hubungan kerja sama harus
                        mutual dan tidak bisa dipaksakan. “Jadi jangan bilang hei kamu masang kerja sama sama aku
                        viewnya cuma 100, 200. Kami lakukan media
                        mapping tapi anda juga harus melakukan mapping aqua ini iklannya mau seperti apa. Kerja sama
                        bisa dengan konten, amplifikasi, pelatihan,” kata dia.</p>
                    <p class="isi-popup">Sementara itu Managing Partner Inventure Yuswohady berbicara mengenai
                        landscape media saat ini. Menurut dia, landscape media awalnya vertikal seperti billboard
                        dan televisi di mana hanya komunikasi satu arah. “Setelah internet
                        dan sosial media, pola bergeser menjadi dua arah dan sifatnya many to many. Itulah yang ada
                        di facebook, Tiktok dan IG,” kata dia. Saat ini menurut Yuswohady yang mempengaruhi media
                        itu Fomo (fear of missing out). Kata Yuswohady,
                        saat ini semua orang bisa menjadi publisher dan Ini kekuatan luar biasa. “Ketika semua orang
                        publisher yang terjadi adalah overloaded information. Informasi meluap di satu sisi. Sisi
                        demand, semua orang punya HP. Di sisi suplay
                        informasi meluap kita punya tools yang namamya HP untuk menangkap semua informasi itu,”
                        katanya. Lalu yang terjadi, lanjut dia, setiap saat orang ingin menangkap informasi itu.
                        “Survei di seluruh dunia, 13 persen milenial sehari
                        bisa 10 jam. Untuk apa? untuk nyari berita. Fomo itu artinya fear of missing out kita ga mau
                        ketinggalan informasi,” paparnya.</p>
                    <p class="isi-popup">Menurut Yuswohady, ada tiga hal yang membuat media dilirik milenial yaitu
                        immediacy, intimacy, spontanity. “Makanya Lambe Turah dan TIkTok lebih menarik. Itu
                        Immediacy, berita langsung bisa nongol. Kenyataannya konten itu semakin
                        penting Immediacy, intimacy, Spontanity. TikTok itu intimacy dan spontanity.. At the end of
                        the day if you not digital you die. Teman-teman kalau berbisnis di media ga digital ga akan
                        survive,” kata Yuswohady.</p>
                    <p class="isi-popup">Sumber : <a
                            href="https://suara.com/news/2022/10/29/094225/acara-pamungkas-local-media-summit-2022-bicara-peluang-dan-tantangan-media-lokal"
                            target="_blank">Suara.com</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 7 -->

    <!-- modal news 8-->
    <div class="modal fade" id="exampleModal8" tabindex="-1" aria-labelledby="exampleModalLabel8" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel8">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news8.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Sampai jumpa di KTT Media Lokal 2023!</h2>
                    <p class="isi-popup">Ajang terbesar Indonesia untuk media lokal, Local Media Summit 2022 yang
                        diselenggarakan oleh Suara.com bekerja sama dengan International Media Support (IMS) di
                        Perpustakaan Nasional Jakarta pada 27-28 Oktober 2022 baru saja berakhir.
                        Acara ini dihadiri oleh 300 media lokal dari ujung barat Nanggroe Aceh Darussalam hingga ke
                        tanah Papua. Acara ini membahas bagaimana media lokal bisa menjadi lebih cerdas dan cerdas
                        dalam konten dan bisnis. Ada kesamaan yang
                        dimiliki oleh seluruh peserta yang hadir dalam acara ini yaitu media harus saling merangkul
                        untuk tumbuh dan berkembang. Pertukaran informasi dan sharing kondisi terjadi tidak hanya di
                        ruang workshop tetapi juga di sesi istirahat.
                        CEO dan Pemimpin Redaksi Suara.com, Suwarjono mengatakan, di era digital saat ini, media
                        lokal juga dituntut untuk memikirkan infrastruktur agar konten yang dihasilkan bisa
                        menjangkau khalayak yang masif. Di tingkat lokal,
                        media tidak hanya fokus pada pembuatan konten, karena konten hanyalah sebagian kecil dari
                        bisnis media di era digital saat ini. “Untuk teman-teman sekarang pemilik media, yang harus
                        kita pelajari bukan hanya konten. Konten
                        hanya sebagian kecil (dari bisnis),” kata Suwarjono saat membuka Local Media Summit 2022.
                    </p>
                    <p class="isi-popup">Pada hari pertama, sejumlah workshop menarik berlangsung. Dimulai dengan
                        konferensi tentang masa depan media lokal dan bagaimana model bisnis dan perkembangan
                        audiens. Lars Bestle yang merupakan ketua IMS di Asia menjadi trigger
                        forum tersebut kemudian disusul oleh Saptini Darmaningrum selaku business director
                        BeritaJatim; Janoe Arijanto, Ketua Asosiasi Perusahaan Periklanan Indonesia; dan Gunawan
                        Susanto, Country Manager AWS Indonesia. Lars Bestle
                        menjelaskan bahwa salah satu masalah terbesar jurnalisme adalah krisis keuangan. Semua
                        pemangku kepentingan harus mencari solusi untuk masalah ini. Yang terpenting, kata Lars
                        Bestle, adalah bagaimana kualitas jurnalistik tetap
                        dihormati dan dilindungi. Menjaga kualitas untuk menjaga martabat jurnalistik juga menjadi
                        salah satu pembahasan dalam workshop media startup di Local Media Summit 2022. Dalam
                        workshop ini, Devi Asmarani selaku Co-Founder Magdalene
                        dan Hendri Salim selaku Head of TechInAsia Indonesia berbicara tentang menjaga kepercayaan.
                        Dalam paparannya, kedua wartawan tersebut mengatakan bahwa menjadi berbeda sangat penting
                        bagi media lokal. Perbedaan dengan media
                        besar merupakan salah satu daya tarik yang memikat investor. Hal lain yang menarik adalah
                        bagaimana media lokal dapat membangun dan memelihara komunitas seperti yang dilakukan oleh
                        Devi di Magdalena.
                    </p>
                    <p class="isi-popup">Pada hari pertama, sejumlah workshop menarik berlangsung. Dimulai dengan
                        konferensi tentang masa depan media lokal dan bagaimana model bisnis dan perkembangan
                        audiens. Lars Bestle yang merupakan ketua IMS di Asia menjadi trigger
                        forum tersebut kemudian disusul oleh Saptini Darmaningrum selaku business director
                        BeritaJatim; Janoe Arijanto, Ketua Asosiasi Perusahaan Periklanan Indonesia; dan Gunawan
                        Susanto, Country Manager AWS Indonesia. Lars Bestle
                        menjelaskan bahwa salah satu masalah terbesar jurnalisme adalah krisis keuangan. Semua
                        pemangku kepentingan harus mencari solusi untuk masalah ini. Yang terpenting, kata Lars
                        Bestle, adalah bagaimana kualitas jurnalistik tetap
                        dihormati dan dilindungi. Menjaga kualitas untuk menjaga martabat jurnalistik juga menjadi
                        salah satu pembahasan dalam workshop media startup di Local Media Summit 2022. Dalam
                        workshop ini, Devi Asmarani selaku Co-Founder Magdalene
                        dan Hendri Salim selaku Head of TechInAsia Indonesia berbicara tentang menjaga kepercayaan.
                        Dalam paparannya, kedua wartawan tersebut mengatakan bahwa menjadi berbeda sangat penting
                        bagi media lokal. Perbedaan dengan media
                        besar merupakan salah satu daya tarik yang memikat investor. Hal lain yang menarik adalah
                        bagaimana media lokal dapat membangun dan memelihara komunitas seperti yang dilakukan oleh
                        Devi di Magdalena. Baca selengkapnya: Kepulauan
                        Riau Indonesia Bersiap untuk Lonjakan Kasus COVID Singapura “Komunitas adalah bagian
                        terpenting. Awalnya sulit, Magdalena muncul di awal dengan penampilan yang berbeda. Kami
                        mulai berkonsultasi dengan komunitas. Kami merangkul
                        komunitas perempuan dan feminis. Kami membuka rubrik komunitas. Kami bisa memberikan promo
                        untuk acara mereka," jelas Devi. Masih di hari pertama Local Media Summit 2022, ada dua
                        workshop lainnya, yaitu tentang menemukan model
                        bisnis untuk media lokal dan bisnis podcast untuk media lokal. Carl Javier, CEO Puma Podcat
                        asal Filipina menjelaskan bahwa podcast menjadi salah satu alternatif media untuk
                        mendapatkan penghasilan. Kuncinya, kata Carl Javier,
                        untuk membangun dan mengembangkan podcast, adalah konsistensi. Javier mengatakan ada tiga
                        tantangan berat dalam bisnis podcast, yaitu pasar, keuangan, dan masalah asosiasi. “Ini
                        pengalaman kami di tahun 2008 ketika kami mulai
                        terbentuk. Sangat sulit bagi kami, tidak ada pasar, tentu saja kesiapan kami untuk
                        menggenjot pasar di lapangan. Sedih orang tidak mau membayar wartawan karena ruang publik,”
                        ujarnya.</p>
                    <p class="isi-popup">Sementara itu, Aliefah Permata Fikri, Senior Account Manager MGID
                        Indonesia, menyatakan kolaborasi penting bagi media untuk menemukan dan mengembangkan model
                        bisnis yang tepat. “Sebagai platform native advertising, kami ingin memberikan
                        dukungan kepada media lokal karena media adalah mitra (MGID),” ujarnya. Sesi hari pertama
                        ditutup dengan malam berjejaring yang dipandu oleh Inayah Wahid, putri bungsu Presiden
                        Abdurrahman Wahid. Inayah Wahid juga pendiri HAMburger
                        Podcast, sebuah startup podcast. KTT Media Lokal Hari Kedua 2022 Di hari kedua, sesi pertama
                        membahas bagaimana cara berkolaborasi dengan Suara.com. Hadir pula dalam sesi Yoursay.id
                        Community Development Manager, Randy Sadikin.
                        Yoursay adalah platform konten buatan pengguna di bawah Suara.com. Sadikin mengatakan, yang
                        terpenting dari kolaborasi adalah mendobrak sekat antara pembaca dan penulis. Suara.com
                        membangun Yoursay.id untuk menciptakan peluang
                        berkolaborasi dengan jurnalis dan penulis di tingkat lokal. “Yoursay memiliki 80.000 anggota
                        dan telah menghasilkan sekitar 4.500 konten. (Demografi pengguna) 37 persen laki-laki, 63
                        persen perempuan, berusia 17-30 tahun. Mereka
                        berasal dari Yogyakarta (35 persen), Jawa Tengah (20 persen), dan sisanya dari Jakarta dan
                        beberapa daerah lainnya," jelas Rendy.</p>
                    <p class="isi-popup">"Dalam workshop keenam bertema hyperlocal strategy for local media,
                        Muhammad Zuhri, Founder dan Pemimpin Redaksi Batamnews menjelaskan hal yang paling krusial
                        dalam mengembangkan media lokal adalah memiliki visi dan misi. Dari
                        visi dan misi tersebut, pimpinan media dapat menetapkan target atas apa yang ingin dicapai.
                        “Visi dan misi ini adalah segalanya. Harus dituangkan ke karyawan yang ada di media lokal.
                        Disampaikan di redaksi, ruang marketing,
                        hingga pekerjaan level paling bawah seperti cleaning service di kantor,” jelas Zuhri. Dengan
                        modalitas dan pengembangan kepercayaan itu, kata Zuhri, media lokal bisa mendapatkan
                        investor. Penting untuk tidak berbelit-belit
                        dengan investor. “Di Batam, tidak semua orang yang punya uang tertarik dengan media. Hal
                        pertama yang harus kita lakukan adalah membuat branding untuk media kita. Produknya harus
                        bagus, artinya konten yang dihasilkan harus
                        berkualitas,” jelas Zuhri. Di hari terakhir Local Media Summit 2022, juga diadakan workshop
                        yang membahas transformasi legacy media menjadi digital media. Sesi penutup Local Media
                        Summit 2022 diisi dengan Konferensi bertajuk
                        “Reinventing Local Media: Finding Opportunities and Mengatasi Tantangan". Anggota Dewan Pers
                        Sapto Anggoro menegaskan, media juga memiliki peran penting dalam kontrol sosial. “Jangan
                        menjual cita-citamu. Karena kita sudah menjadi
                        pilar keempat demokrasi. Pers yang kuat dan mandiri secara finansial harus menjalankan
                        amanat UU Pers,” jelasnya. Penutupan Local Media Summit 2022 diakhiri dengan sesi foto
                        bersama dan PR besar bagi para peserta yang hadir
                        agar pandai membangun dan mengembangkan media lokalnya. Sampai jumpa di Local Media Summit
                        berikutnya!</p>

                    <p class="isi-popup">Sumber : <a
                            href="https://www.theindonesia.id/news/2022/11/02/143200/see-you-at-the-2023-local-media-summit"
                            target="_blank">theindonesia.id</a></p>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 8 -->

    <!-- modal news 9-->
    <div class="modal fade" id="exampleModal9" tabindex="-1" aria-labelledby="exampleModalLabel9" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel9">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news9.jpg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">BandungBergerak Membangun Kolaborasi dengan Pembaca Pada akhir 2022</h2>
                    <p class="isi-popup">Pada akhir 2022, BandungBergerak.id merangkul berbagai komunitas yang
                        sejatinya pembaca setianya untuk berkolaborasi. Upaya yang dilakukan adalah menggelar kelas
                        menulis luar ruangan (offline). Pertama, kelas menulis esai kritis
                        di rumah Eva Eryani di RW 11 Tamansari pada 28 November 2022. Yang kedua, kelas menulis esai
                        kreatif di Kampoeng Tjibarani pada 18 Desember 2022. Keduanya menjadi tonggak penting dalam
                        perjalanan kami yang baru seumur jagung.
                        Rumah Eva Eryani, yang dijadikan lokasi kelas menulis pertama, adalah gubuk semipermanen
                        yang terjepit di kompleks rumah deret dengan bangunan menara yang menjulang tinggi. Eva
                        menjadi satu-satunya warga yang bertahan pascapenggusuran.
                        Beberapa hari sebelum kelas menulis itu, si empunya gubuk menerima surat peringatan dari
                        pemerintah Kota Bandung untuk segera mengosongkan tempat tinggal itu. Kelas menulis esai
                        kritis di Tamansari itu diselenggarakan secara
                        gratis bagi 25 pendaftar pertama. Donasi dibuka, dan sepenuhnya diserahkan ke tuan rumah.
                        Begitulah anak-anak muda, kebanyakan mahasiswa, datang ke gubuk yang juga dijadikan Bagi
                        BandungBergerak.id, kelas menulis di Tamansari
                        ini adalah sebuah pernyataan sikap. Kami, seperti diniatkan sejak awal, akan berpihak kepada
                        suara minoritas, kepada kelompok-kelompok marginal.</p>
                    <p class="isi-popup">Kelas menulis kedua digelar dengan menggandeng komunitas Masyarakat Kreatif
                        Kampoeng Tjibarani. Lokasinya di saung bambu di bantaran Sungai Cikapundung. Diskusi yang
                        hangat, dengan belasan anak muda yang datang, ditingkahi gemericik
                        air sungai yang mengalir deras ke selatan. Berbeda dengan di Tamansari, kelas menulis kali
                        ini berbayar. Ini menjadi yang bertama bagi BandungBergerak.id, dan sangat mungkin akan
                        semakin sering terjadi di tahun-tahun mendatang.
                        Demi keberlanjutan, kami harus mulai belajar melakukan program-program serupa ini. Mau tidak
                        mau. Dana yang diperoleh dari peserta pelatihan dibagi dua. Seperti itu memang konsepnya.
                        Sebagian diberikan ke komunitas Masyarakat
                        Kreatif Kampoeng Tjibarani selaku tuan rumah untuk mencukupi kebutuhan konsumsi peserta.
                        Sepertinya, akan seperti inilah nanti kerja-kerja BandungBergerak.id ke depan. Kami ingin
                        selalu melibatkan komunitas dan tumbuh bersama-sama.
                        Selalu menyenangkan mendapati kawan-kawan muda yang secara proaktif menyalurkan minat
                        menulis mereka. Kelas menulis pada akhirnya menjadi wadah untuk saling belajar, dan persis
                        itulah yang hendak kami bangun.
                    </p>
                    <p class="isi-popup">Lewat kelas menulis, BandungBergerak.id ingin menularkan kesenangan pada
                        kata, pada kalimat. Selama cerita masih memesona, bolehlah kami berharap situs web
                        BandungBergerak.id bakal tidak pernah kering dari esai-esai bermutu, yang
                        juga kritis menanggapi isu sosial, yang dikirimkan oleh pembaca.</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 9 -->

    <!-- modal news 10-->
    <div class="modal fade" id="exampleModal10" tabindex="-1" aria-labelledby="exampleModalLabel10" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel10">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news10.jpeg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Tantangan Dan Potensi Bisnis Media Lokal Kekinian</h2>
                    <p class="isi-popup">Bisnis media digital masa kini di Indonesia belum menemukan role model yang
                        tepat dan benar-benar cocok. Hal ini dikatakan oleh pemimpin redaksi Suara.com, Suwarjono
                        dalam bincang Local Media Outlook bertema Menggali Potensi Bisnis Media Lokal di Tahun 2023,
                        Kamis (26/1/2023). Dalam acara yang menghadirkan beberapa narasumber ahli ini, Suwarjono
                        menjabarkan kondisi media massa mulai era kovensional sampai kekinian. Dikatakannya sampai
                        saat ini media digital masih terus mencari model bisnis yang sesuai dan benar-benar bisa
                        dijadikan contoh untuk tetap bertahan. “Hingga saat ini semua media digital masih mencari
                        model yang paling cocok, belum ada yang benar-benar menemukannya,” ujarnya. Media saat ini
                        juga dihadapkan dengan tantangan yaitu karena banyaknya konten kreator sehingga semuanya
                        berlomba untuk mencari viewers. Ia menilai, hal ini tidak bisa dihindari karena semuanya
                        harus adaptif dan tidak bisa mengembalikan keadaan seperti halnya di era konvensional.
                        Sedangkan di media lokal ada beberapa peluang yang bisa dijajal diantaranya seperti short
                        dan vertical video dan tahun ini dipandang sebagai tahun monetisasi. “Sebelumnya, search
                        engine dan video yang bisa dimonetisasi namun kini seperti Tiktok dan Snack Video contohnya,
                        mulai bisa dimonetisasi,” jelasnya.</p>
                    <p class="isi-popup">Selain itu ada beberapa peluang untuk media lokal yang bisa dikembangkan.
                        Salah satunya yang menjadi tren saat ini adalah content marketing.
                        Cara seperti ini juga diprediksi akan berkembang pada momen pilkada dan pemilu 2024 dimana
                        trennya dibagi dua yaitu melalui publisher dan sosial media.
                        “Berkaca di tahun 2019, yang dicari di masing-masing daerah saat pilkada adalah media-media
                        lokal. Bukan nasional,” jelas Suwarjono.
                        Ia pun mengingatkan untuk juga memanfaatkan platform global untuk konten segmented seperti
                        Youtube dan lain sebagainya.
                        Adapun acara yang digelar lewat platform video Zoom ini juga ditayangkan secara live di
                        website serta kanal Youtube Suaradotcom.

                        Para pembicara yang hadir selain Suwarjono adalah Alandra Dio dari platform Snack Video,
                        Annisa Rahmatillah dari media SukabumiUpdate, serta Country Manager IMS Indonesia, Eva
                        Danayanti.

                        Dalam sesi awal yang berisi paparan program IMS di Indonesia, terutama yang dikelola bersama
                        Suara.com, Eva Danayanti selaku Country Manager IMS menyampaikan bahwa LMS 2023 siap digelar
                        dengan kemasan yang lebih baik dan lebih bermanfaat bagi para peserta.
                    </p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 10 -->

    <!-- modal news 11-->
    <div class="modal fade" id="exampleModal11" tabindex="-1" aria-labelledby="exampleModalLabel11" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel11">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news11.jpeg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Tantangan Dan Potensi Bisnis Media Lokal Kekinian</h2>
                    <p class="isi-popup">Batamnews Peduli dan food startup SESAMA meluncurkan program berbagi ratusan
                        hingga ribuan nasi kotak gratis di setiap hari Jumat. Kegiatan ini rencananya akan digelar
                        secara rutin dan sudah berlangsung selama 6 bulan. Kegiatan itu untuk membantu memecahkan
                        masalah kemiskinan dan kelaparan di Kota Batam, Kepulauan Riau. Data terbaru Badan Pusat
                        Statistik (BPS) menunjukkan, bahwa hampir 70.000 penduduk Kota Batam berada di garis kemiskinan,
                        sehingga tindakan nyata dibutuhkan untuk membantu masyarakat. Kedua perusahaan ini memiliki
                        komitmen yang sama untuk melakukan gerakan berbagi makan setiap hari Jumat. Selama 6 bulan
                        terakhir, gerakan ini sudah berlangsung dengan baik dan membuat Eko Syaiful Arifin, pendiri
                        SESAMA, sangat bangga. "Makanan yang akan dibagikan akan langsung diterima oleh penerima tanpa
                        harus mengantri. Ini dilakukan dengan memperhatikan data dan referensi dari masyarakat sehingga
                        tepat sasaran dan membantu mereka yang paling membutuhkan, seperti panti asuhan, anak yatim, dan
                        anak jalanan," ujar Eko. CEO Batamnews, Zuhri Muhammad, mengungkapkan kegiatan ini juga akan
                        melibatkan puluhan content creator. "Kita akan ajak teman-teman konten kreator untuk peduli
                        sesama yang membutuhkan. Ini adalah niat kami bagaimana menumbuhkan rasa kepedulian diantara
                        kita, terutama bagi mereka yang sangat membutuhkan," ujar Zuhri</p>
                    <p class="isi-popup">Gerakan ini juga memiliki lebih dari 300 donatur tetap setiap minggunya.
                        Donatur juga menerima laporan rutin setiap minggu tentang setiap porsi makanan yang diberikan.
                        Ini membuat proses lebih transparan dan mempertanggungjawabkan amanah para donatur yang baik.
                        Kolaborasi ini sangat penting karena membantu memecahkan masalah kemiskinan dan kelaparan di
                        Kota Batam. Ini juga menunjukkan bahwa dengan bekerja sama, kita bisa membuat perbedaan dan
                        membantu masyarakat yang membutuhkan. Kedepannya, Batamnews Peduli dan SESAMA berharap bisa
                        bekerja sama dengan lebih banyak perusahaan dan individu untuk membuat perbedaan yang lebih
                        besar.
                    </p>
                    <p class="isi-popup">Bagi donatur yang berminat berdonasi silakan hubungi:</p>
                    <p class="isi-popup">1. Admin Batamnews (0811-6835-000)</p>
                    <p class="isi-popup">2. Admin SESAMA (0813-1900-9315)</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 11 -->

    <!-- modal news 12-->
    <div class="modal fade" id="exampleModal12" tabindex="-1" aria-labelledby="exampleModalLabel12" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content modal-xl">
                <div class="modal-header modal-xl">
                    <h5 class="modal-title" id="exampleModalLabel12">Local Media Summit</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body mx-3">
                    <img src="assets/images/news/news11.jpeg" alt="Artikel" width="281" height="176" class="popup" />
                    <h2 class="text-start">Fitur dan Fungsi Google Analytics 4</h2>
                    <p class="isi-popup">Google Analytics 4 adalah tool analytics terbaru dari Google. Dengan alat ini,
                        Anda dapat mengukur trafik dan engagement di website dan aplikasi Anda. Menariknya, tool ini
                        tersedia secara gratis, sehingga dapat digunakan oleh siapa saja, termasuk Anda. Berbeda dengan
                        versi sebelumnya, Google Analytics 4 (GA4) memiliki berbagai fitur untuk memudahkan Anda
                        mendapatkan informasi mendalam tentang pengunjung. Salah satunya adalah kemampuan untuk
                        melakukan pengukuran lintas platform, yaitu website dan aplikasi. Jadi, Anda bisa dengan mudah
                        mendapatkan informasi lengkap mengenai performa website atau aplikasi melalui data-data yang
                        terkumpul. Apa Saja Fitur Google Analytics 4? Berikut beberapa fitur Google Analytics 4 yang
                        dapat Anda gunakan:</p>
                    <p class="isi-popup"><b>1. Predictive Analytics</b> <br>Salah satu fitur yang tersedia di Google
                        Analytics 4
                        adalah Predictive Analytics. Dengan bantuan kecerdasan buatan, Anda dapat mengumpulkan informasi
                        mengenai target segmen Anda, termasuk kemungkinan pengguna melakukan transaksi, hingga potensi
                        pendapatan yang bisa Anda hasilkan dari bisnis Anda.
                    </p>
                    <p class="isi-popup"><b>2. Custom Reports</b> <br>Fitur Exploration ini memungkinkan Anda untuk
                        membuat tabel
                        atau visualisasi yang dapat Anda sesuaikan dengan jenis penggunaan Anda. Jadi Anda dapat dengan
                        mudah mempresentasikan data Anda kepada klien.</p>
                    <p class="isi-popup"><b>3. Events Track </b><br>Pada Google Analytics 4 ini, sebagian besar event
                        sudah
                        dilacak
                        secara otomatis dan terukur dengan sesuai. Selain itu, Anda dapat melakukan event tracking
                        hingga 300 event per setiap Google Analytics 4 property Anda.</p>
                    <p class="isi-popup"><b>4. Conversions Track</b><br> Jika Anda ingin mengetahui event mana pada
                        website Anda
                        yang dapat menghasilkan konversi, Anda dapat melakukannya dengan melacak event sebagai konversi.
                        Dengan begitu, setiap interaksi pengguna yang sesuai yang telah Anda targetkan dapat diukur
                        lebih mudah.</p>
                    <p class="isi-popup"><b>5. Automated Table </b><br>Kemudahan lain yang disediakan dari fitur Google
                        Analytics 4
                        adalah mengotomatisasi tabel. Dengan fitur ini, Anda dapat mengatur dan menyesuaikan tampilan
                        data-data yang Anda butuhkan saja ke laporan Anda.</p>
                    <p class="isi-popup"><b>6. Anomaly Detection </b><br>Pada Google Analytics 4, dengan bantuan machine
                        learning,
                        Anda bisa mendeteksi anomali yang tampak pada grafik di seluruh Google Analytics 4 Anda. Jadi,
                        Anda dapat lebih fokus pada data-data yang ditampilkan sekaligus membantu Anda untuk melihat
                        data apa yang Anda butuhkan.</p>
                    <p class="isi-popup"><b>7. Audience Segments</b><br> Di Google Analytics 4, Anda dapat melakukan
                        pengaturan
                        segmen pengguna cukup sekali saja. Dengan begitu, jika Anda membutuhkan data audiens yang
                        berbeda-beda, Anda dapat menemukannya dengan mudah tanpa harus membuat segmen setiap kali ingin
                        memfilter audiens Anda.</p>
                    <p class="isi-popup">Fungsi Google Analytics 4 Berikut fungsi Google Analytics 4 yang bisa Anda
                        dapatkan:</p>
                    <p class="isi-popup">
                    <ul>
                        <li>Menggabungkan analisis web dan aplikasi</li>
                        <li>Memberikan insight pengguna lintas-platform</li>
                        <li>Mengotomatisasi pelacakan pada setiap event</li>
                        <li>Memberikan pemahaman mendalam mengenai perilaku pengguna</li>
                        <li>Memberikan pilihan kontrol privasi pengguna</li>
                        <li>Melakukan pengukuran engagement secara akurat </li>
                        <li>Memudahkan validasi data melalui DebugView </li>
                        <li>Menyediakan jenis laporan yang beragam </li>
                        <li>Memudahkan pelacakan konversi </li>
                        <li>Mengizinkan pembuatan konversi secara kompleks </li>
                        <li>Membuat analisis jalur pengguna </li>
                        <li>Menyiapkan model analisis prediktif </li>
                        <li>Memudahkan visualisasi data </li>
                        <li>Memberikan integrasi dengan BigQuery </li>
                        <li>Menyaring penargetan audiens</li>
                    </ul>
                    </p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal news 12 -->

</div>