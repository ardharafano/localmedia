<div id="workshop">

    <div class="container">
        <ul class="nav nav-pills mb-3 justify-content-center gap-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="pills-day1-tab" data-bs-toggle="pill" data-bs-target="#pills-day1"
                    type="button" role="tab" aria-controls="pills-day1" aria-selected="true">WORKSHOP DAY 1</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="pills-day2-tab" data-bs-toggle="pill" data-bs-target="#pills-day2"
                    type="button" role="tab" aria-controls="pills-day2" aria-selected="false">WORKSHOP DAY 2</button>
            </li>
        </ul>

        <div class="tab-content justify-content-center" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-day1" role="tabpanel" aria-labelledby="pills-day1-tab"
                tabindex="0">

                <div class="table-responsive bdr">
                    <table class="table table-bordered align-middle" style="border-color:#a7a9ac">
                        <!-- thead -->
                        <tr style="background-color:#D9D9D9;color:#143E6C">
                            <th rowspan="2">No</th>
                            <th colspan="2">Waktu</th>
                            <th rowspan="2">Durasi</th>
                            <th rowspan="2">Session</th>
                            <th rowspan="2">Deskripsi</th>
                            <th rowspan="2">Speakers</th>
                        </tr>
                        <!-- thead -->

                        <!-- colspan -->
                        <tr style="background-color:#D9D9D9;color:#143E6C">
                            <th>Start</th>
                            <th>End</th>
                        </tr>
                        <!-- colspan -->

                        <!-- tbody -->
                        <tr>
                            <td>1</td>
                            <td>12:00</td>
                            <td>13:30</td>
                            <td>90"</td>
                            <td>Registrasi & Makan Siang</td>
                            <td>Peserta hadir di Venue - Registrasi dan Makan Siang</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>13:30</td>
                            <td>13:40</td>
                            <td>10"</td>
                            <td rowspan="3">SAMBUTAN</td>
                            <td>Sambutan CEO beritajatim.com</td>
                            <td>Dwi Eko Lokononto</td>
                        </tr>

                        <tr>
                            <td>3</td>
                            <td>13:40</td>
                            <td>13:50</td>
                            <td>10"</td>
                            <td>Sambutan Rektor Universitas Airlangga</td>
                            <td>Mohammad Nasih</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>13:50</td>
                            <td>14:00</td>
                            <td>10"</td>
                            <td>Sambutan Lembaga usaid*</td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td>5</td>
                            <td>14:00</td>
                            <td>14:15</td>
                            <td>15"</td>
                            <td>KEYNOTE SPEECH & PEMBUKAAN</td>
                            <td>Media yang Independen, Tumbuh dan Melayani Publik</td>
                            <td>Khofifah Indar Parawansa (Gubernur Jawa Timur)</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>14:15</td>
                            <td>17:00</td>
                            <td>165"</td>
                            <td>CONFERENCE</td>
                            <td>Peluang dan Tantangan Bisnis Media di Jawa Timur: Kini dan Masa Depan. Manajemen,
                                Bisnis, Konten, Teknologi
                            </td>
                            <td>Sapto Anggoro (Dewan Pers), Ajar Edi (Director, Corporate Affairs - Microsoft Indonesia), Yuswohady (Inventure),
                                Amir Suherlan (Managing Director - Wavemaker)</td>
                        </tr>
                        <!-- <tr>
                            <td>7</td>
                            <td>16:00</td>
                            <td>17:00</td>
                            <td>90"</td>
                            <td>TALKSHOW</td>
                            <td>Pendanaan untuk Startup Media Lokal</td>
                            <td>Tessa Piper (MDIF), Suwarjono (Suara.com)</td>
                        </tr> -->
                        <tr>
                            <td>7</td>
                            <td>19:00</td>
                            <td>21:00</td>
                            <td>90"</td>
                            <td>NETWORKING NIGHT (GALA DINNER)</td>
                            <td>Jamuan Makan Malam & Networking (topik diskusi : Pendanaan untuk Startup Media Lokal)</td>
                            <td>Suwarjono (Suara.com)</td>
                        </tr>
                        <!-- tbody -->

                    </table>
                </div>

            </div>

            <div class="tab-pane fade" id="pills-day2" role="tabpanel" aria-labelledby="pills-day2-tab" tabindex="0">

                <div class="table-responsive bdr">
                    <table class="table table-bordered align-middle" style="border-color:#a7a9ac">
                        <!-- thead -->
                        <tr style="background-color:#D9D9D9;color:#143E6C">
                            <th rowspan="2">No</th>
                            <th colspan="2">Waktu</th>
                            <th rowspan="2">Durasi</th>
                            <th rowspan="2">Session</th>
                            <th rowspan="2">Deskripsi</th>
                            <th rowspan="2">Speakers</th>
                        </tr>
                        <!-- thead -->

                        <!-- colspan -->
                        <tr style="background-color:#D9D9D9;color:#143E6C">
                            <th>Start</th>
                            <th>End</th>
                        </tr>
                        <!-- colspan -->

                        <!-- tbody -->
                        <tr>
                            <td>1</td>
                            <td>09:00</td>
                            <td>10:00</td>
                            <td>60"</td>
                            <td>WORKSHOP</td>
                            <td>Optimasi Sosial Media dan Distribusi Konten untuk Media Lokal</td>
                            <td>Dimas Sagita (Suara.com)</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>10:00</td>
                            <td>11:00</td>
                            <td>60"</td>
                            <td>TALKSHOW</td>
                            <td>Technology for Local Media </td>
                            <td>Heru Tjatur (MNC/ICT Watch)</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>11:00</td>
                            <td>11:45</td>
                            <td>45"</td>
                            <td>WORKSHOP</td>
                            <td>Menggali Digital Revenue dari Ad Native Platform</td>
                            <td>Aliefah Permata Fikri (MGID)</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>11:45</td>
                            <td>13:00</td>
                            <td>75"</td>
                            <td>ISOMA</td>
                            <td>Networking Session + Makan Siang</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>13:00</td>
                            <td>13:45</td>
                            <td>45"</td>
                            <td>WORKSHOP</td>
                            <td>Creative Video Production for Social Media</td>
                            <td>Iramdani (Arkadia - Production)</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>13:45</td>
                            <td>14:30</td>
                            <td>45"</td>
                            <td>COACHING CLINIC</td>
                            <td>Boost Traffic dengan SEO untuk Media Lokal</td>
                            <td>Subagja Hamara (Harapan Rakyat)</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>14:30</td>
                            <td>15:15</td>
                            <td>45"</td>
                            <td>WORKSHOP</td>
                            <td>Business Model Canvas for Local Media</td>
                            <td>Eva Danayanti (IMS), Asep Saefullah (Suara)</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>15:15</td>
                            <td>17:00</td>
                            <td>105"</td>
                            <td>CONFERENCE</td>
                            <td>Membangun Ekosistem Media Digital di Jawa Timur</td>
                            <td>Arief Rahman (AMSI Jatim),
                                Sherlita Agustin (Diskominfo Jatim), Dwi Setyawan (FTMM - UNAIR)
                            </td>
                        </tr>
                        <!-- <tr>
                            <td>9</td>
                            <td>17:00</td>
                            <td>17:30</td>
                            <td>30"</td>
                            <td>PENUTUPAN</td>
                            <td>Kesimpulan dan Rekomendasi</td>
                            <td></td>
                        </tr> -->
                        <!-- tbody -->

                    </table>
                </div>

            </div>
        </div>
    </div>

</div>