<!DOCTYPE html>
<html lang="en">

<head>
    <title>Local Media Summit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>  -->
    <meta name="description" content="Local Media Summit">
    <meta name="keywords" content="Suara, Local Media Summit, LMS">

    <meta property="og:title" content="locammediasummit.com" />
    <meta property="og:description"
        content="Local Media Summit - Emerging Local Media Business Viability to Represent the Unrepresented" />
    <meta property="og:url" content="https://microsite.suara.com/localmediasummit" />
    <meta property="og:image" content="assets/images/jms_thumb.jpg" />

    <link rel="Shortcut icon" href="../2022/assets/images/favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css">
    <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script>
    <link href="assets/css/bootstrap.min.css?<?= time() ?>" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

</head>

<body>

    <div id="nav">
        <nav class="navbar navbar-expand-xl navbar-dark fixed-top" aria-label="Ninth navbar example" id="nav-lokal">
            <div class="container-xl">
                <a class="navbar-brand" href="#home"><img src="../2022/assets/images/icon-home.svg" alt="image" /></a>
                <ul class="navbar-nav mx-auto mb-2 mb-xl-0">
                    <li class="nav-item d-block d-xl-none">
                        <a class="jms-2023" href="jms.php" target="_blank">JATIM MEDIA SUMMIT 2023</a>
                    </li>
                </ul>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="nav-lms">
                    <div class="collapse navbar-collapse" id="navbarsExample07XL">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="#about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#news">NEWS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="#network">NETWORK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#faq">FAQ</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    ARCHIEVE
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item-lms" href="2022/index.php" target="_blank">LMS 2022</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#kontak">CONTACT</a>
                            </li>
                            <li class="nav-item d-none d-xl-block">
                                <a class="jms-2023" href="jms.php" target="_blank">JATIM MEDIA SUMMIT 2023</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="desktop">
                        <!-- <ul class="navbar-nav nav-lms-right me-auto mb-2 mb-lg-0"> -->
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/"
                                    target="_blank"><img src="../2022/assets/images/ig.svg" alt="image" width="25"
                                        height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img
                                        src="../2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img
                                        src="../2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    REGISTER
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a>
                                    </li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a>
                                    </li>
                                </ul>
                            </li> -->

                        </ul>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarsExample07XL">
                    <div class="mobile">
                        <ul class="nav-lms-right me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.instagram.com/localmediasummit/"
                                    target="_blank"><img src="../2022/assets/images/ig.svg" alt="image" width="25"
                                        height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/MediaSummit2022" target="_blank"><img
                                        src="../2022/assets/images/twt.svg" alt="image" width="25" height="25" /></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.tiktok.com/@mediasummit2022" target="_blank"><img
                                        src="../2022/assets/images/tiktok.svg" alt="image" width="22" height="26" /></a>
                            </li>
                            <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle button-register text-light" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    REGISTER
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="participant.php" target="_blank">Participant</a>
                                    </li>
                                    <li><a class="dropdown-item" href="collaborate.php" target="_blank">Collaborator</a>
                                    </li>
                                </ul>
                            </li> -->

                        </ul>
                    </div>
                </div>

            </div>
        </nav>
    </div>

    <!-- <div id="home" class="container-fluid">
        <div class="container">
            <div class="row">


                <div class="col-lg-12">
                    <img src="../2022/assets/images/header-text5.svg" alt="image" class="header-text" widht="973" height="331" />
                    <img src="../2022/assets/images/header-text5-mobile.svg" alt="image" class="header-text-mobile" widht="649" height="435" />
                    <h5 class="text-center text-white mt-2">Perpustakaan Nasional, Jalan Medan Merdeka Selatan, Jakarta Pusat</h5>
                    <h1 class="date mt-2">October 27-28, 2022</h1>
                    <div class="body-count">
                            <h1 class="sub-date mt-2">Event Countdown</h1>
                            <div class="count" id="count"></div>
                        </div>
                </div>

            </div>

        </div>
    </div> -->

    <div id="home">
        <div class="container">
            <div class="row">

                <section class="splide head" aria-label="Splide Basic HTML Example">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <div class="container">
                                    <div class="row">
                                        <div class="media">
                                            <img src="assets/images/lms-2023.svg" alt="image" class="hg mt-5"
                                                width="446" height="210" />
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="splide__slide">
                                <div class="container">
                                    <div class="row">
                                        <div class="media">
                                            <img src="assets/images/lms-jatim.svg" alt="image" class="hg" width="446"
                                                height="210" />
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </section>

                <div style="z-index:999999">
                    <!-- <h2>COMING SOON</h2> -->
                    <div class="dropdown">
                        <div class="daftar  dropdown-toggle" type="button" data-bs-toggle="dropdown">
                            DAFTAR
                        </div>
                        <ul class="dropdown-menu">
                            <!-- <li><a class="dropdown-item" target="_blank"
                                    href="https://form.arkadia.me/105eb1e8-d29e-4e4e-8be7-7e6f331b8b2e">JMS Personal</a>
                            </li> -->
                            <li><a class="dropdown-item" target="_blank" href="register_personal.php">JMS Personal</a>
                            </li>
                            <li><a class="dropdown-item" target="_blank"
                                    href="https://form.arkadia.me/9e442390-ac86-4ada-a771-ca4bb6bb214e">JMS Media</a>
                            </li>
                            <li><a class="dropdown-item" target="_blank" href="register_collaborators.php">JMS
                                    Collaborators</a>
                            </li>
                        </ul>
                    </div>
                    <h3>INITIATORS</h3>
                    <img src="assets/images/lms_initiator.png" alt="image" width="370" height="60"
                        class="d-block m-auto" />
                    <!-- <div class="img-initiators">
                        <img src="assets/images/suara-white.svg" alt="image" width="177" height="19" class="suara" />
                        <img src="assets/images/ims-white.svg" alt="image" width="89" height="33" class="ims" />
                    </div> -->
                </div>

                <div class="col-lg-12">
                    <video id="background-video" autoplay loop muted poster="assets/images/foto/6.jpeg">
                        <source src="assets/video/h4.mp4" type="video/mp4">
                    </video>

                    <div class="overlay">
                        <!-- <h1>MENUJU VISI INDONESIA 2045</h1>
                        <p>KOTA DUNIA UNTUK SEMUA</p>
                        <div id="myBtn" onclick="myFunction()">

                            <div class="button" onclick="this.classList.toggle('active')">
                                <div class="background" x="0" y="0" width="200" height="200"></div>
                                <div class="icon" width="200" height="200">
                                    <div class="part left" x="0" y="0" width="200" height="200" fill="#fff"></div>
                                    <div class="part right" x="0" y="0" width="200" height="200" fill="#fff"></div>
                                </div>
                                <div class="pointer"></div>
                            </div>

                        </div> -->
                    </div>

                </div>

            </div>
        </div>
    </div>


    <div id="about">
        <div class="container">
            <div class="row">
                <h1 class="header">ABOUT</h1>
                <p>Local Media Summit 2023 ini merupakan pertemuan para pengelola media lokal kedua dan terbesar di
                    Indonesia. Media lokal di sini adalah media yang memiliki lingkup di sebuah wilayah geografis
                    tertentu atau menyasar segmen masyarakat tertentu.
                </p>
                <p>Dalam acara ini, para pengelola media lokal akan berjumpa dengan perusahaan, agensi iklan, lembaga
                    donor, lembaga pemerintah, platforms Internet, dan solusi teknologi. Ini adalah kesempatan media
                    lokal memperkenalkan berbagai produk dan
                    layanannya pada berbagai kalangan yang lebih luas. Kemudian para mereka juga dapat berjumpa dengan
                    para pakar dalam memecahkan berbagai masalah dan tantangan yang mereka hadapi dalam pengembangan
                    newsroom dan bisnisnya.</p>
                <p>Sementara investor media dan Lembaga donor bisa menemukan partner yang tepat di forum ini. Pengiklan,
                    lembaga pemerintah, platform, atau penyedia solusi teknologi bisa berbicara dengan media lokal yang
                    mereka ingin temui agar bisa lebih
                    memahami fokus isu, layanan, atau siapa audience mereka.</p>
                <p>Kegiatan ini juga akan menghasilkan catatan dan rekomendasi yang dapat digunakan oleh media lokal
                    maupun stakeholder nya untuk mendorong pengembangan bisnis media lokal di Indonesia.</p>
            </div>
        </div>
    </div>

    <div id="video">
        <h1>VIDEO</h1>
        <h3>Local Media Summit 2022</h3>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Tqvpc02Wi8Y" title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen></iframe>
    </div>

    <div id="foto">
        <div class="container">
            <h1>FOTO</h1>
            <h3>Local Media Summit 2022</h3>
            <div class="row">

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto1">
                        <img src="assets/images/foto/1.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto2">
                        <img src="assets/images/foto/2.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto3">
                        <img src="assets/images/foto/3.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto4">
                        <img src="assets/images/foto/4.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto5">
                        <img src="assets/images/foto/5.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto6">
                        <img src="assets/images/foto/6.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto7">
                        <img src="assets/images/foto/7.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto8">
                        <img src="assets/images/foto/8.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto9">
                        <img src="assets/images/foto/9.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto10">
                        <img src="assets/images/foto/10.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto11">
                        <img src="assets/images/foto/11.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <div class="col-lg-3 col-sm-6 col-6 content-foto">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalfoto12">
                        <img src="assets/images/foto/12.jpeg" alt="image" width="268" height="268" class="home" />
                    </a>
                </div>

                <a href="#" id="loadMorefoto">Lebih Banyak</a>

            </div>

            <!-- modal foto 1 -->
            <div class="modal fade" id="exampleModalfoto1" tabindex="-1" aria-labelledby="exampleModalLabelfoto1"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/1.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 1 -->

            <!-- modal foto 2 -->
            <div class="modal fade" id="exampleModalfoto2" tabindex="-1" aria-labelledby="exampleModalLabelfoto2"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/2.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 2 -->

            <!-- modal foto 3 -->
            <div class="modal fade" id="exampleModalfoto3" tabindex="-1" aria-labelledby="exampleModalLabelfoto3"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/3.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 3 -->

            <!-- modal foto 4 -->
            <div class="modal fade" id="exampleModalfoto4" tabindex="-1" aria-labelledby="exampleModalLabelfoto4"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/4.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 4 -->

            <!-- modal foto 5 -->
            <div class="modal fade" id="exampleModalfoto5" tabindex="-1" aria-labelledby="exampleModalLabelfoto5"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/5.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 5 -->

            <!-- modal foto 6 -->
            <div class="modal fade" id="exampleModalfoto6" tabindex="-1" aria-labelledby="exampleModalLabelfoto6"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/6.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 6 -->

            <!-- modal foto 7 -->
            <div class="modal fade" id="exampleModalfoto7" tabindex="-1" aria-labelledby="exampleModalLabelfoto7"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/7.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 7 -->

            <!-- modal foto 1 -->
            <div class="modal fade" id="exampleModalfoto8" tabindex="-1" aria-labelledby="exampleModalLabelfoto8"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/8.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 8 -->

            <!-- modal foto 9 -->
            <div class="modal fade" id="exampleModalfoto9" tabindex="-1" aria-labelledby="exampleModalLabelfoto9"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/9.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 9 -->

            <!-- modal foto 10 -->
            <div class="modal fade" id="exampleModalfoto10" tabindex="-1" aria-labelledby="exampleModalLabelfoto10"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/10.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 10 -->

            <!-- modal foto 11 -->
            <div class="modal fade" id="exampleModalfoto11" tabindex="-1" aria-labelledby="exampleModalLabelfoto11"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/11.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 11 -->

            <!-- modal foto 12 -->
            <div class="modal fade" id="exampleModalfoto12" tabindex="-1" aria-labelledby="exampleModalLabelfoto12"
                aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <img src="assets/images/foto/12.jpeg" alt="image" class="popup" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end foto 12 -->

        </div>
    </div>

    <!-- news -->
    <?php include('news.php'); ?>
    <!-- end news -->

    <div id="speaker">
        <div class="container">
            <div class="row">
                <!-- <a href="speaker.php"> -->
                <h1 class="header mb-3">SPEAKERS</h1>
                <h3>Local Media Summit 2022</h3>
                <!-- </a> -->
                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/teten.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/../2022/speaker.php#teten" target="_blank">
                            <div class="nama mt-3 mb-1 mb-1">Teten Masduki</div>
                            <div class="jabatan">Menteri Koperasi dan lms</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/sapto.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#sapto" target="_blank">
                            <div class="nama mt-3 mb-1">Sapto Anggoro</div>
                            <div class="jabatan">Anggota Dewan Pers </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/suwarjono.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#suwarjono" target="_blank">
                            <div class="nama mt-3 mb-1">Suwarjono</div>
                            <div class="jabatan">CEO Suara.com</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/lars.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#lars" target="_blank">
                            <div class="nama mt-3 mb-1">Lars H. Bestle</div>
                            <div class="jabatan">IMS Head of Department Asia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/yos.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#yos" target="_blank">
                            <div class="nama mt-3 mb-1">Yos Kusuma</div>
                            <div class="jabatan">News Partner Manager at Google</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/gunawan1.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#gunawan" target="_blank">
                            <div class="nama mt-3 mb-1">Gunawan Susanto</div>
                            <div class="jabatan">Country Manager AWS (Indonesia)</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/eva.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#eva" target="_blank">
                            <div class="nama mt-3 mb-1">Eva Danayanti</div>
                            <div class="jabatan">IMS Programme Manager Indonesia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/hendri.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#hendri" target="_blank">
                            <div class="nama mt-3 mb-1">Hendri Salim</div>
                            <div class="jabatan">CEO TechInAsia Indonesia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/janoe.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#janoe" target="_blank">
                            <div class="nama mt-3 mb-1">Janoe Arijanto</div>
                            <div class="jabatan">Ketua Persatuan Perusahaan Periklanan Indonesia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/devi.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#devi" target="_blank">
                            <div class="nama mt-3 mb-1">Devi Asmarani</div>
                            <div class="jabatan">CO-Founder and Chief Editor Magdelene.co</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/yuswohady.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#yuswohady" target="_blank">
                            <div class="nama mt-3 mb-1">Yuswohady</div>
                            <div class="jabatan">Managing Partner Inventure</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/derian_laks.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#derian" target="_blank">
                            <div class="nama mt-3 mb-1">Derian Laks</div>
                            <div class="jabatan">Country Director Dailymotion</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/arief.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#arif" target="_blank">
                            <div class="nama mt-3 mb-1">Arif Mujahidin</div>
                            <div class="jabatan">Corporate Communication Director Danone Indonesia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/henrik.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#henrik_grunnet" target="_blank">
                            <div class="nama mt-3 mb-1">Henrik Grunnet</div>
                            <div class="jabatan"> IMS Media Advisor</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/sunarti.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#sunarti" target="_blank">
                            <div class="nama mt-3 mb-1">Sunarti Sain</div>
                            <div class="jabatan">Editor in-Chief Radar Selatan, Sulawesi Selatan</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/ahmadnasir.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#ahmad" target="_blank">
                            <div class="nama mt-3 mb-1">Ahmad Nasir</div>
                            <div class="jabatan">CEO Indotnesia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/arsito.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#arsito" target="_blank">
                            <div class="nama mt-3 mb-1">Arsito Hidayatullah</div>
                            <div class="jabatan">Redaktur Pelaksana Suara.com</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/catur-ratna.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#ratna" target="_blank">
                            <div class="nama mt-3 mb-1">Catur Ratna Wulandari</div>
                            <div class="jabatan">Editor in Chief DigitalMama</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/inayah-wahid.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#inayah" target="_blank">
                            <div class="nama mt-3 mb-1">Inayah Wahid</div>
                            <div class="jabatan">Founder HAMburger Podcast</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/dimas.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#dimas" target="_blank">
                            <div class="nama mt-3 mb-1">Dimas Sagita</div>
                            <div class="jabatan">Head of Growth Suara.com</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/subagja.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#subagja" target="_blank">
                            <div class="nama mt-3 mb-1">Subagja Hamara</div>
                            <div class="jabatan">CEO Harapan Rakyat</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/lasma.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#lasma" target="_blank">
                            <div class="nama mt-3 mb-1">Lasma Natalia</div>
                            <div class="jabatan">Direktur LBH Bandung</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/rendy.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#rendy" target="_blank">
                            <div class="nama mt-3 mb-1">Rendy Sadikin </div>
                            <div class="jabatan">Community Development Manager Yoursay.id</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/suwarmin.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#suwarmin" target="_blank">
                            <div class="nama mt-3 mb-1">Suwarmin</div>
                            <div class="jabatan">Direktur Bisnis dan Konten Solopos Group</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/bram2.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#bram" target="_blank">
                            <div class="nama mt-3 mb-1">Bram Bravo</div>
                            <div class="jabatan">Head of Digital Ad Ops & Programmatic Advertising Suara.com</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/trijoko.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#trijoko" target="_blank">
                            <div class="nama mt-3 mb-1">Tri Joko Her Riadi</div>
                            <div class="jabatan">Founder BandungBergerak</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/septini1.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#septini" target="_blank">
                            <div class="nama mt-3 mb-1">Saptini Darmaningrum</div>
                            <div class="jabatan">Direktur Usaha BeritaJatim</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/prawira.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#prawira" target="_blank">
                            <div class="nama mt-3 mb-1">Prawira Maulana</div>
                            <div class="jabatan">Content Manager Tribun Bengkulu</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/lerane.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#lerane" target="_blank">
                            <div class="nama mt-3 mb-1">La Rane Hafied</div>
                            <div class="jabatan">Pendiri Paberik Soeara Rakjat</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/aliefah.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#aliefah" target="_blank">
                            <div class="nama mt-3 mb-1">Aliefah Permata Fikri</div>
                            <div class="jabatan">Sr. Account Manager MGID</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/martha1.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#martha" target="_blank">
                            <div class="nama mt-3 mb-1">Martha Rinna Dewi</div>
                            <div class="jabatan">Territory Manager AWS (Indonesia)</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/jesse.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#jesse" target="_blank">
                            <div class="nama mt-3 mb-1">Jesse Adam Halim</div>
                            <div class="jabatan">Produser HAMburger Podcast</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/septiaji.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#septiaji" target="_blank">
                            <div class="nama mt-3 mb-1">Septiaji Eko Nugroho</div>
                            <div class="jabatan">Ketua Presidium Mafindo</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/carl.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#carl_javier" target="_blank">
                            <div class="nama mt-3 mb-1">Carl Javier</div>
                            <div class="jabatan">CEO Puma Podcast Philippines</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/eddy.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#eddy" target="_blank">
                            <div class="nama mt-3 mb-1">Eddy Prastyo</div>
                            <div class="jabatan">Manager Produksi Suara Surabaya Media</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/zuhri.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#zuhri" target="_blank">
                            <div class="nama mt-3 mb-1">Zuhri Muhammad</div>
                            <div class="jabatan">Founder dan CEO BatamNews</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/upi.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#upi" target="_blank">
                            <div class="nama mt-3 mb-1">Upi Asmaradhana</div>
                            <div class="jabatan">Founder-CEO Kabar Group Indonesia</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/dwieko.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#dwieko" target="_blank">
                            <div class="nama mt-3 mb-1">Dwi Eko Lokononto</div>
                            <div class="jabatan">Pemimpin Umum & Pemimpin Redaksi BeritaJatim</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-13 col-6">
                    <div class="border">
                        <div class="speaker-img">
                            <img src="../2022/assets/images/heru.png" alt="image" width="125" height="125" />
                        </div>
                        <a href="../2022/speaker.php#heru" target="_blank">
                            <div class="nama mt-3 mb-1">Heru Tjatur</div>
                            <div class="jabatan">Co-Founder ICT Watch</div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div id="network" class="container">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="header mt-3">INITIATORS</h1>
                    <div class="media">
                        <a href="https://suara.com/" target="_blank">
                            <img src="../2022/assets/images/suara.png" alt="image" width="269" height="38" />
                        </a>
                        <a href="https://www.mediasupport.org/" target="_blank">
                            <img src="../2022/assets/images/lms.png" alt="image" width="175" height="74" />
                        </a>
                    </div>

                    <!-- opsi kolaborator baru, minta dibawah IMS dan suara -->
                    <!-- <div class="media">
                    <a href="https://www.mediasupport.org/" target="_blank">
                        <img src="assets/images/ims.svg" alt="image" width="175" height="74" />
                    </a>
                    <a href="https://suara.com/" target="_blank">
                        <img src="assets/images/suara.svg" alt="image" width="269" height="38" />
                    </a>
          </div> -->
                </div>

            </div>
        </div>
    </div>

    <hr>

    <div id="network" class="container">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="header">NETWORK</h1>
                    <div class="media">
                        <a href="https://www.youtube.com/channel/UCIPBADJECZeHH_eYCWu-jnQ?app=desktop" target="_blank">
                            <img src="../2022/assets/images/hamburger.png" alt="image" width="72" height="72" />
                        </a>
                        <a href="https://www.babad.id/" target="_blank">
                            <img src="../2022/assets/images/babad.png" alt="image" width="105" height="35" />
                        </a>
                        <a href="https://bincangperempuan.com/" target="_blank">
                            <img src="../2022/assets/images/bincang-perempuan.png" alt="image" width="70" height="70" />
                        </a>
                        <a href="https://digitalmama.id/" target="_blank">
                            <img src="../2022/assets/images/digitalmama.png" alt="image" width="84" height="40" />
                        </a>
                        <a href="https://indotnesia.suara.com/" target="_blank">
                            <img src="../2022/assets/images/indotnesia.png" alt="image" width="100" height="66" />
                        </a>
                        <a href="https://www.inidata.id/" target="_blank">
                            <img src="../2022/assets/images/inidata.png" alt="image" width="110" height="20" />
                        </a>
                        <a href="https://kaltimkece.id/" target="_blank">
                            <img src="../2022/assets/images/kaltimkece.png" alt="image" width="40" height="40" />
                        </a>
                        <a href="https://katongntt.com/" target="_blank">
                            <img src="../2022/assets/images/katong-ntt.png" alt="image" width="121" height="32" />
                        </a>
                        <a href="https://kutub.id/kemana-seharusnya-pemuda-bergerak/" target="_blank">
                            <img src="../2022/assets/images/kutub.png" alt="image" width="50" height="52" />
                        </a>
                        <a href="https://www.magdalene.co/" target="_blank">
                            <img src="../2022/assets/images/magdalene.png" alt="image" width="115" height="21" />
                        </a>
                        <a href="https://purwokertokita.com/" target="_blank">
                            <img src="../2022/assets/images/purwokertokita.png" alt="image" width="40" height="40" />
                        </a>
                        <a href="https://beritajatim.com/" target="_blank">
                            <img src="../2022/assets/images/beritajatim.png" alt="image" width="170" height="23" />
                        </a>
                        <a href="https://harapanrakyat.com/" target="_blank">
                            <img src="../2022/assets/images/harapanrakyat.png" alt="image" width="170" height="56" />
                        </a>
                        <a href="https://www.kabarmakassar.com/" target="_blank">
                            <img src="../2022/assets/images/kabarmakassar.png" alt="image" width="141" height="35" />
                        </a>
                        <a href="https://www.sukabumiupdate.com/" target="_blank">
                            <img src="../2022/assets/images/sukabumi.png" alt="image" width="100" height="27" />
                        </a>
                        <a href="https://www.telisik.id/" target="_blank">
                            <img src="../2022/assets/images/telisik.png" alt="image" width="100" height="40" />
                        </a>
                        <a href="https://www.beritabali.com/" target="_blank">
                            <img src="../2022/assets/images/beritabali.png" alt="image" width="150" height="30" />
                        </a>
                        <a href="https://www.jabarnews.com/" target="_blank">
                            <img src="../2022/assets/images/jabarnews.png" alt="image" width="150" height="23" />
                        </a>
                        <a href="https://www.riauonline.co.id/" target="_blank">
                            <img src="../2022/assets/images/riauonline.png" alt="image" width="150" height="35" />
                        </a>
                        <a href="https://zonautara.com/" target="_blank">
                            <img src="../2022/assets/images/zonautara.png" alt="image" width="50" height="66" />
                        </a>
                        <a href="https://terakota.id/" target="_blank">
                            <img src="../2022/assets/images/terakota.png" alt="image" width="150" height="42" />
                        </a>
                        <a href="https://kieraha.com/" target="_blank">
                            <img src="../2022/assets/images/kieraha.png" alt="image" width="100" height="23" />
                        </a>
                        <a href="https://kabarpapua.co/" target="_blank">
                            <img src="../2022/assets/images/kabarpapua.png" alt="image" width="150" height="21" />
                        </a>
                        <a href="https://garak.id/" target="_blank">
                            <img src="../2022/assets/images/garak.png" alt="image" width="100" height="23" />
                        </a>
                        <a href="https://ciayumajakuning.id/" target="_blank">
                            <img src="../2022/assets/images/ciayumajakuning.png" alt="image" width="60" height="61" />
                        </a>
                        <a href="https://kitapunya.id/" target="_blank">
                            <img src="../2022/assets/images/kitapunya.png" alt="image" width="120" height="30" />
                        </a>
                        <a href="https://wongkito.co/" target="_blank">
                            <img src="../2022/assets/images/wongkito.png" alt="image" width="140" height="24" />
                        </a>
                        <a href="https://ekuatorial.com/" target="_blank">
                            <img src="../2022/assets/images/ekuatorial.png" alt="image" width="140" height="52" />
                        </a>
                        <a href="https://kaidah.id/" target="_blank">
                            <img src="../2022/assets/images/kaidah.png" alt="image" width="110" height="33" />
                        </a>
                        <a href="https://lensaku.id/" target="_blank">
                            <img src="../2022/assets/images/lensaku.png" alt="image" width="150" height="38" />
                        </a>
                        <a href="https://www.sudutpayakumbuh.com/" target="_blank">
                            <img src="../2022/assets/images/sudut.png" alt="image" width="70" height="70" />
                        </a>
                        <a href="https://www.terasmanado.com/" target="_blank">
                            <img src="../2022/assets/images/terasmanado.png" alt="image" width="200" height="51" />
                        </a>
                        <a href="https://www.kanalmetro.com/" target="_blank">
                            <img src="../2022/assets/images/kanalmetro.png" alt="image" width="130" height="27" />
                        </a>
                        <a href="https://www.pojoksatu.id/" target="_blank">
                            <img src="../2022/assets/images/pojoksatu.png" alt="image" width="140" height="22" />
                        </a>
                        <a href="https://www.referensia.id/" target="_blank">
                            <img src="../2022/assets/images/referensia.png" alt="image" width="140" height="31" />
                        </a>
                        <a href="https://www.acehinfo.id/" target="_blank">
                            <img src="../2022/assets/images/acehinfo.png" alt="image" width="120" height="38" />
                        </a>
                        <a href="https://radarselatan.fajar.co.id/" target="_blank">
                            <img src="../2022/assets/images/radarselatan.png" alt="image" width="70" height="52" />
                        </a>
                        <a href="https://dikita.id/" target="_blank">
                            <img src="../2022/assets/images/dikita.png" alt="image" width="80" height="25" />
                        </a>
                        <a href="https://www.goriau.com/home.html" target="_blank">
                            <img src="../2022/assets/images/goriau.png" alt="image" width="100" height="33" />
                        </a>
                        <a href="https://www.kapol.id" target="_blank">
                            <img src="../2022/assets/images/kapol.png" alt="image" width="110" height="21" />
                        </a>
                        <a href="https://madurapost.net/" target="_blank">
                            <img src="../2022/assets/images/madurapost.png" alt="image" width="120" height="37" />
                        </a>
                        <a href="https://batamnews.co.id/" target="_blank">
                            <img src="../2022/assets/images/batamnews.png" alt="image" width="120" height="37" />
                        </a>
                        <a href="https://matamaros.com/" target="_blank">
                            <img src="../2022/assets/images/matamaros.png" alt="image" width="100" height="37" />
                        </a>
                        <a href="https://maduraindepth.com/" target="_blank">
                            <img src="../2022/assets/images/maduraindepth.png" alt="image" width="120" height="23" />
                        </a>
                        <a href="https://katinting.com/" target="_blank">
                            <img src="../2022/assets/images/katinting.png" alt="image" width="120" height="38" />
                        </a>
                        <a href="https://kabarmedan.com/" target="_blank">
                            <img src="../2022/assets/images/kabarmedan.png" alt="image" width="120" height="25" />
                        </a>
                        <a href="https://kabargupas.com/" target="_blank">
                            <img src="../2022/assets/images/kabargupas.png" alt="image" width="140" height="31" />
                        </a>
                        <a href="https://konde.co/" target="_blank">
                            <img src="../2022/assets/images/konde.png" alt="image" width="120" height="36" />
                        </a>
                        <a href="https://beritaminang.com/" target="_blank">
                            <img src="../2022/assets/images/beritaminang.png" alt="image" width="110" height="37" />
                        </a>
                        <a href="https://kediripedia.com/" target="_blank">
                            <img src="../2022/assets/images/kediripedia.png" alt="image" width="85" height="50" />
                        </a>
                        <a href="https://bantennews.co.id/" target="_blank">
                            <img src="../2022/assets/images/banten-news.jpeg" alt="image" width="150" height="47" />
                        </a>
                        <a href="https://jubi.id/" target="_blank">
                            <img src="../2022/assets/images/jubi.jpeg" alt="image" width="87" height="25" />
                        </a>
                        <a href="https://interes.id/" target="_blank">
                            <img src="../2022/assets/images/interes.png" alt="image" width="42" height="45" />
                        </a>
                        <a href="https://kitamediamuda.com/" target="_blank">
                            <img src="../2022/assets/images/kmm.png" alt="image" width="53" height="50" />
                        </a>
                        <a href="https://trusttv.id/" target="_blank">
                            <img src="../2022/assets/images/trusttv.png" alt="image" width="131" height="50" />
                        </a>
                        <a href="https://intuisi.co/" target="_blank">
                            <img src="../2022/assets/images/intuisi.jpeg" alt="image" width="118" height="40" />
                        </a>
                        <a href="https://detikmanado.com/" target="_blank">
                            <img src="../2022/assets/images/detikmanado.png" alt="image" width="150" height="22" />
                        </a>
                        <a href="https://kilasjambi.com/" target="_blank">
                            <img src="../2022/assets/images/kilasjambi.png" alt="image" width="150" height="26" />
                        </a>
                        <a href="https://ninna.id/" target="_blank">
                            <img src="../2022/assets/images/ninna.jpeg" alt="image" width="100" height="34" />
                        </a>
                        <a href="https://www.roemahkata.com/" target="_blank">
                            <img src="../2022/assets/images/roemahkata.png" alt="image" width="90" height="52" />
                        </a>
                        <a href="https://www.britabrita.com/" target="_blank">
                            <img src="../2022/assets/images/brita.png" alt="image" width="150" height="25" />
                        </a>
                        <a href="https://www.indodaily.co/" target="_blank">
                            <img src="../2022/assets/images/indodaily.png" alt="image" width="150" height="34" />
                        </a>
                        <a href="https://www.acehkita.com/" target="_blank">
                            <img src="../2022/assets/images/acehkita.jpeg" alt="image" width="130" height="30" />
                        </a>
                        <a href="https://www.wowbabel.com/" target="_blank">
                            <img src="../2022/assets/images/wowbabel.png" alt="image" width="100" height="45" />
                        </a>
                        <a href="https://www.lingkarkita.com/" target="_blank">
                            <img src="../2022/assets/images/lingkarkita.png" alt="image" width="120" height="26" />
                        </a>
                        <a href="https://www.jurnalif.com/" target="_blank">
                            <img src="../2022/assets/images/jurnalif.png" alt="image" width="50" height="62" />
                        </a>
                        <a href="https://www.langgam.id/" target="_blank">
                            <img src="../2022/assets/images/langgam.png" alt="image" width="80" height="80" />
                        </a>
                        <a href="https://www.layarkita.com/" target="_blank">
                            <img src="../2022/assets/images/layarkita.jpeg" alt="image" width="100" height="45" />
                        </a>
                        <a href="https://www.kaltimtoday.co/" target="_blank">
                            <img src="../2022/assets/images/kaltimtoday.jpeg" alt="image" width="150" height="33" />
                        </a>
                        <a href="https://www.alagraph.com/" target="_blank">
                            <img src="../2022/assets/images/alagraph.png" alt="image" width="110" height="24" />
                        </a>
                        <a href="https://www.alagraphbontangpost.id/" target="_blank">
                            <img src="../2022/assets/images/bontangpost.png" alt="image" width="120" height="24" />
                        </a>
                        <a href="https://www.sultrakini.com/" target="_blank">
                            <img src="../2022/assets/images/sultrakini.png" alt="image" width="130" height="40" />
                        </a>
                        <a href="https://www.idenera.com/" target="_blank">
                            <img src="../2022/assets/images/idenera.png" alt="image" width="120" height="29" />
                        </a>
                        <a href="https://www.projectmultatuli.org/" target="_blank">
                            <img src="../2022/assets/images/multatuli.png" alt="image" width="120" height="30" />
                        </a>
                        <a href="https://www.starbanjar.com/" target="_blank">
                            <img src="../2022/assets/images/starbanjar.jpeg" alt="image" width="180" height="26" />
                        </a>
                        <a href="https://www.suarakendari.com/" target="_blank">
                            <img src="../2022/assets/images/suarakendari.png" alt="image" width="90" height="43" />
                        </a>
                        <a href="https://www.amirariau.com/" target="_blank">
                            <img src="../2022/assets/images/amira.png" alt="image" width="120" height="32" />
                        </a>
                        <a href="https://bandungbergerak.id/" target="_blank">
                            <img src="../2022/assets/images/bandungbergerak.jpeg" alt="image" width="160" height="42" />
                        </a>
                        <a href="https://www.kolase.id/" target="_blank">
                            <img src="../2022/assets/images/kolase.png" alt="image" width="110" height="47" />
                        </a>
                        <a href="https://www.insidepontianak.com/" target="_blank">
                            <img src="../2022/assets/images/insidepontianak.png" alt="image" width="130" height="31" />
                        </a>
                        <a href="https://www.bentaratimur.id/" target="_blank">
                            <img src="../2022/assets/images/bentaratimur.jpeg" alt="image" width="130" height="35" />
                        </a>
                        <a href="https://www.seputarpapua.com/" target="_blank">
                            <img src="../2022/assets/images/seputarpapua.png" alt="image" width="120" height="25" />
                        </a>
                        <a href="https://www.barta1.com/" target="_blank">
                            <img src="../2022/assets/images/barta1.png" alt="image" width="110" height="47" />
                        </a>
                        <a href="https://www.independen.id/" target="_blank">
                            <img src="../2022/assets/images/independen.png" alt="image" width="120" height="39" />
                        </a>
                        <a href="https://www.suaragayo.com/" target="_blank">
                            <img src="../2022/assets/images/suaragayo.png" alt="image" width="120" height="30" />
                        </a>
                        <a href="https://www.radarsulbar.fajar.co.id/" target="_blank">
                            <img src="../2022/assets/images/radarsulbar.png" alt="image" width="120" height="31" />
                        </a>
                        <a href="https://www.kosakata.org/" target="_blank">
                            <img src="../2022/assets/images/kosakata.png" alt="image" width="100" height="66" />
                        </a>
                        <a href="https://www.bataknature.com/" target="_blank">
                            <img src="../2022/assets/images/bataknature.png" alt="image" width="140" height="25" />
                        </a>
                        <a href="https://www.wartabromo.com/" target="_blank">
                            <img src="../2022/assets/images/wartabromo.png" alt="image" width="150" height="43" />
                        </a>
                        <a href="https://www.volkpop.co/" target="_blank">
                            <img src="../2022/assets/images/volkpop.png" alt="image" width="90" height="61" />
                        </a>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <hr>

    <div id="collaborators" class="container">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <h1 class="header mt-3">COLLABORATORS</h1>
                    <div class="media">
                        <a href="https://www.norway.no/en/indonesia/" target="_blank">
                            <img src="../2022/assets/images/norway.png" alt="image" width="280" height="62" />
                        </a>
                        <a href="https://www.netherlandsandyou.nl" target="_blank">
                            <img src="../2022/assets/images/netherlands.png" alt="image" width="220" height="92" />
                        </a>
                        <a href="https://www.kurawalfoundation.org/" target="_blank">
                            <img src="../2022/assets/images/kurawal.png" alt="image" width="180" height="60" />
                        </a>
                        <a href="https://bri.co.id/" target="_blank">
                            <img src="../2022/assets/images/bri.png" alt="image" width="91" height="35" />
                        </a>
                        <a href="https://bni.co.id/" target="_blank">
                            <img src="../2022/assets/images/bni.png" alt="image" width="107" height="35" />
                        </a>
                        <a href="https://www.btn.co.id/" target="_blank">
                            <img src="../2022/assets/images/btn.png" alt="image" width="150" height="34" />
                        </a>
                        <a href="https://www.btpn.com/" target="_blank">
                            <img src="../2022/assets/images/btpn1.png" alt="image" width="100" height="96" />
                        </a>
                        <a href="https://aws.amazon.com/" target="_blank">
                            <img src="../2022/assets/images/aws.png" alt="image" width="88" height="52" />
                        </a>
                        <a href="https://vibicloud.com/" target="_blank">
                            <img src="../2022/assets/images/vibicloud.png" alt="image" width="120" height="34" />
                        </a>
                        <a href="https://www.dailymotion.com/id" target="_blank">
                            <img src="../2022/assets/images/dailymotion.png" alt="image" width="150" height="26" />
                        </a>
                        <a href="https://www.notix.co/" target="_blank">
                            <img src="../2022/assets/images/notix.png" alt="image" width="95" height="31" />
                        </a>
                        <!-- <a href="https://pinhome.id/" target="_blank">
                    <img src="../2022/assets/images/pinhome.png" alt="image" width="162" height="45" />
                </a> -->
                        <a href="https://www.mgid.com" target="_blank">
                            <img src="../2022/assets/images/mgid.png" alt="image" width="80" height="39" />
                        </a>
                        <a href="https://www.danone.co.id/" target="_blank">
                            <img src="../2022/assets/images/danone.png" alt="image" width="80" height="107" />
                        </a>
                        <a href="https://www.telkom.co.id/sites" target="_blank">
                            <img src="../2022/assets/images/telkom1.png" alt="image" width="130" height="71"
                                class="mb-md-3" />
                        </a>
                        <a href="https://www.pertamina.com/" target="_blank">
                            <img src="../2022/assets/images/pertamina.png" alt="image" width="180" height="44" />
                        </a>
                        <a href="https://www.chandra-asri.com/" target="_blank">
                            <img src="../2022/assets/images/chandraasri.png" alt="image" width="177" height="50" />
                        </a>
                        <a href="https://www.adaro.com/" target="_blank">
                            <img src="../2022/assets/images/adaro.png" alt="image" width="120" height="43"
                                class="mb-md-3" />
                        </a>
                        <a href="https://www.djarumfoundation.org/" target="_blank">
                            <img src="../2022/assets/images/djarum.png" alt="image" width="200" height="35" />
                        </a>
                        <a href="https://www.sinarmas.com/" target="_blank">
                            <img src="../2022/assets/images/sinarmas2.png" alt="image" width="150" height="33" />
                        </a>
                        <a href="https://merdekacoppergold.com/" target="_blank">
                            <img src="../2022/assets/images/mcg.png" alt="image" width="130" height="56"
                                class="mb-md-3" />
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div id="newslatter">
        <div class="container">

            <div class="d-flex justify-content-center row g-3">
                <div class="col-md-10">
                    <!-- <h4 class="text-center">NEWSLATTER</h4> -->
                    <h4 class="mb-3">Download materi Local Media Summit 2022</h4>
                </div>
            </div>

            <form action="register_act.php" class="d-flex justify-content-center row g-3 needs-validation" novalidate>

                <div class="col-md-10">
                    <input type="email" class="form-control" id="validationCustom01" placeholder="Masukkan Email Anda"
                        autocomplete="off" required>
                    <div class="valid-feedback">
                        Berhasil
                    </div>
                    <div class="invalid-feedback">
                        Belum Diisi
                    </div>
                </div>



                <div class="col-10 text-center daftar">
                    <button class="mb-5 button-kirim" type="submit" onclick="setTimeout(check, 1000);">KIRIM</button>
                </div>
            </form>
        </div>
    </div>

    <div id="faq">
        <div class="container">
            <div class="row g-4">
                <h1 class="text-center">FAQ</h1>
                <div class="col-sm-12 col-12">

                    <div>
                        <details open>
                            <summary>
                                Siapakah yang boleh mengikuti kegiatan Local Media Summit?
                            </summary>
                            <div>
                                Kegiatan ini terbuka tidak hanya bagi para pengiat media lokal tapi juga bagi siapa saja
                                yang tertarik dengan isu dan perkembangan media lokal di Indonesia.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Bagaimana cara mengikuti local media summit?
                            </summary>
                            <div>
                                Silahkan mendaftar melalui menu registrasi sesuai dengan latar belakang Anda. Kami akan
                                mengirimkan pemberitahuan lebih lanjut atas detail yang Anda pilih.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apakah ada dukungan pembiayaan yang disediakan bagi peserta?
                            </summary>
                            <div>
                                Kami menyediakan bantuan pembiayaan untuk admission fee dan logistik bagi kalangan media
                                lokal dan admission fee untuk kalangan non media dalam jumlah terbatas. Bagi Anda yang
                                membutuhkan silahkan pilih pada bagian registrasi. Kami akan menginfokan lebih
                                lanjut apakah permintaan Anda bisa kami akomodir.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apakah bisa mengikuti seluruh rangkaian kegiatan local media summit?
                            </summary>
                            <div>
                                Semua peserta berhak mengikuti keseluruhan rangkaian acara. Namun, dikarenakan ada
                                beberapa workshop yang berjalan bersamaan sehingga Anda diminta untuk memilih sesuai
                                dengan prioritas masing-masing.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apa saja yang akan disediakan untuk peserta Local Media Summit?
                            </summary>
                            <div>
                                Kesempatan mengikuti seluruh rangkaian kegiatan, seminar kit, makanan, dan minuman
                                selama kegiatan berlangsung.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Bagaimana menentukan pilihan dari dua kategori yang ada dalam menu registrasi?
                            </summary>
                            <div>
                                Kami membedakan keikutsertaan dalam dua kategori yang bisa Anda pilih, yaitu
                                participants dan collaborators. Participants adalah Anda yang mewakili media atau
                                individu. Sedangkan collaborators adalah Anda yang mewakili lembaga atau institusi yang
                                ingin
                                memberikan dukungan atas kegiatan ini selain menjadi peserta, seperti dukungan
                                pendanaan, peralatan, dll.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Apakah perbedaan participants media dan individu?
                            </summary>
                            <div>
                                Participants media adalah Anda para pengelola media lokal atau Anda yang ditunjuk untuk
                                mewakili media lokal Anda. Participants individu adalah Anda yang berasal dari media
                                namun tidak ditunjuk sebagai perwakilan media sehingga hadir mewakili pribadi,
                                atau Anda yang berasal dari kalangan NGO, akademisi, mahasiswa, serta kalangan umum
                                lainnya yang tertarik dengan isu perkembangan media lokal di Indonesia.
                            </div>
                        </details>

                        <details>
                            <summary>
                                Mengapa collaborators?
                            </summary>
                            <div>
                                Kami menyebut dukungan yang diberikan adalah bagian dari kontribusi lembaga atau
                                institusi Anda dalam berkolaborasi dengan media lokal di Indonesia. Kami mengajak
                                segenap kalangan yang memiliki sumber daya untuk ikut berpartisipasi sebagai
                                collaborators
                                dalam kegiatan ini, mendukung perkembangan dan pengembangan media lokal di Indonesia.
                            </div>
                        </details>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="kontak">
        <h1 class="header">Contact Us</h1>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <h2>For further information, you can reach us via these channels.</h2>
                    <h3>
                        <a href="mailto:localmediasummit@arkadiacorp.com">
                            <img src="../2022/assets/images/mail.png" alt="image" />
                            <input type="submit" value="localmediasummit@arkadiacorp.com" class="email" width="32"
                                height="32" />
                        </a>
                        <div class="br"></div>
                        <div class="wa">
                            <img src="../2022/assets/images/wa.png" alt="image" width="32" height="32" />
                            <a href="https://wa.me/+628119156234">0811-9156-234</a>
                        </div>
                    </h3>
                </div>

            </div>
        </div>
    </div>


    <div id="wa">
        <a href="https://wa.me/+628119156234" class=" fly-wa-bottom" target="_blank">
            <img src="assets/images/wa.svg" alt="img" />
        </a>
    </div>

    <div class="copyright">
        <p>© 2023 SUARA.COM - ALL RIGHTS RESERVED.</p>
    </div>

    <script>
    var splide = new Splide('.splide.head', {
        perPage: 1,
        rewind: true,
        autoplay: true,
        breakpoints: {
            991.98: {
                perPage: 1,
                gap: "1rem",
                // destroy: true,
            },
            768: {
                perPage: 1,
                gap: "1rem",
                // destroy: true,
            },
        },
    });

    splide.mount();
    </script>

    <script>
    $(document).ready(function() {
        $('body').scrollspy({
            target: ".navbar",
            offset: 50
        });
        $("#myNavbar a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function() {
                    window.location.hash = hash;
                });
            }
        });
    });
    </script>

    <!-- <script>
        var countDownDate = new Date("Oct 27, 2022 09:00:00").getTime();

        var x = setInterval(function() {

            var now = new Date().getTime();

            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);


            document.getElementById("count").innerHTML = "<div class=\"days\"> \
  <div class=\"numbers\">" + days + "</div>Days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + hours + "</div>Hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + minutes + "</div>Minutes</div> \
</div>";

            if (distance < 0) {
                clearInterval(x);
                document.getElementById("count").innerHTML = "<div class=\"days\"> \
  <div class=\"numbers\">" + "00" + "</div>Days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + "00" + "</div>Hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + "00" + "</div>Minutes</div> \
</div>";
            }
        }, 1000);
    </script> -->

    <!-- tooltip -->
    <script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
    <!-- end tooltip -->


    <!-- pause video -->
    <!-- <script>
        var video = document.getElementById("background-video");
        var btn = document.getElementById("myBtn");

        function myFunction() {
            if (video.paused) {
                video.play();
            } else {
                video.pause();
            }
        }
    </script> -->

    <!-- end pause video -->

    <!-- <script>
    $(document).ready(function() {
        jQuery('img').each(function() {
            jQuery(this).attr('src', jQuery(this).attr('src') + '?' + (new Date()).getTime());
        });
    });
    </script> -->

    <script>
    function check() {
        swal({
            title: "Gagal!",
            text: "Data Masih Belum Lengkap",
            icon: "error",
            button: true
        });
    }
    </script>


    <script>
    (function() {
        'use strict'
        var forms = document.querySelectorAll('.needs-validation')
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()
    </script>

    <script>
    $(document).ready(function() {
        $(".content").slice(0, 8).show();
        $("#loadMore").on("click", function(e) {
            e.preventDefault();
            $(".content:hidden").slice(0, 4).slideDown();
            if ($(".content:hidden").length == 0) {
                $("#loadMore").text("Tidak ada konten lagi").addClass("noContent");
            }
        });

    })
    </script>

    <script>
    $(document).ready(function() {
        $(".content-foto").slice(0, 4).show();
        $("#loadMorefoto").on("click", function(e) {
            e.preventDefault();
            $(".content-foto:hidden").slice(0, 4).slideDown();
            if ($(".content-foto:hidden").length == 0) {
                $("#loadMorefoto").text("Tidak ada foto lagi").addClass("noContentfoto");
            }
        });

    })
    </script>

</body>

</html>